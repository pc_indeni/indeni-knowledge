# indeni Knowledge Repository #

This repository currently contains the Collector scripts (parsers) for indeni. In the future, it will contain the human-generated rules as well. We currently do not plan to include the machine learning rules.

### What is this repository for? ###

This repository allows everyone around the world to contribute knowledge to one another. All the code that makes it into the "master" branch will sent to indeni installations around the world. You can help someone on the other side of the planet!

### How do I get set up and contribute? ###

[Read the IKP document on getting set up and contributing](https://indeni.atlassian.net/wiki/pages/viewpage.action?pageId=63012870)

### License ###

You agree that all contributions are and will be given voluntarily. By contributing code to this repository you hereby irrevocably and unconditionally license the Contribution (as defined below) under the Apache license version 2 https://www.apache.org/licenses/LICENSE-2.0 . You represent that you are legally entitled to grant the above license. If your employer(s) has rights to intellectual property that you create that includes your Contributions, you represent that you have received permission to make Contributions on behalf of that employer or that your employer has waived such rights for your Contributions. You further represent that each of your Contributions is your original creation. You agree to notify this repository's managers of any facts or circumstances of which you become aware that would make these representations inaccurate in any respect. "Contribution" means any original work of authorship, including any modifications or additions to an existing work, that is submitted by You to this repository

### Who do I talk to? ###

product@indeni.com

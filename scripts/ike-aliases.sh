##############################################################################################################
#
#                                   		Helping aliases
#
#	These aliases can help you when working with indeni things
#	To use them, create a file in your home folder called .bash_aliases and paste the content there.
#	Afterwards, in your shell, run the command "source ~/.bash_aliases" to load the new aliases for your current session.
#   You only need to do this the first time -- the system (.bashrc) will load them at subsequent logins.
#
#	Please note that in order to fully use this script you must have command-runner.sh in the $PATH.
#
#	More details can be found on the wiki:
#	https://indeni.atlassian.net/wiki/spaces/IKP/pages/230195473/Helping+aliases
#
##############################################################################################################

#Credentials
USER="indeni"
PASSWORD="indenirocks123!"

#Credentials for the indeni server api
INDENIUSER="admin"
INDENIPASSWORD="admin123!"

#Default devices unless an IP is specified
DEFAULTF5="192.168.197.50"

#####################################################################################
#                                   Command-Runner full-command
#
#   Description:
#   Runs a script, or a directory of indeni scripts in full-command mode
#
#   Usage:
#   crfull <script name or directory name> <device ip> [verbose]
#
#   Examples:
#   Run a command against 192.168.197.51
#   crfull rest-mgmt-tm-sys-version.ind 192.168.197.51
#
#   Run a command against 192.168.197.51 in verbose mode
#   crfull rest-mgmt-tm-sys-version.ind 192.168.197.51 verbose
#
#####################################################################################

crfull() {

    if [ -z "$3" ]; then
        command-runner.sh full-command --basic-authentication $USER,$PASSWORD --ssh $USER,$PASSWORD $1 $2
    else
        command-runner.sh full-command --verbose --basic-authentication $USER,$PASSWORD --ssh $USER,$PASSWORD $1 $2
    fi

}

#####################################################################################
#                                   f5full
#
#   Description:
#   Runs a script, or a directory of indeni scripts in full-command mode
#
#   Usage:
#   f5full <script name or directory name> [device ip]
#
#   Examples:
#   Run a command against $DEFAULTF5
#   f5full rest-mgmt-tm-sys-version.ind
#
#   Run a command against 192.168.197.51
#   f5full rest-mgmt-tm-sys-version.ind 192.168.197.51
#
#####################################################################################

f5full() {
    if [ -z "$2" ]; then
        crfull $1 $DEFAULTF5
    else
        crfull $1 $2
    fi
}

#Same as above but in verbose mode
f5fullverbose() {
    if [ -z "$2" ]; then
        crfull $1 $DEFAULTF5 verbose
    else
        crfull $1 $2 verbose
    fi
}

#####################################################################################
#                                   finddoc
#
#   Description:
#   Finds prewritten documentation for a specific metric
#
#   Usage:
#   finddoc <Path to search (recursive)> <metric name>
#
#   Example:
#   This example would find documentation for "network-interface-rx-errors" in "./f5"
#   finddoc ./f5 network-interface-rx-errors
#
#####################################################################################

finddoc() {
    find $1 -name *.ind | xargs sed -n '/COMMENTS/,/^\#\!/p' | sed -n "/^[^ ]*$2.*:/,/^[^ ]/p"
}

#####################################################################################
#                                   crparse
#
#   Description:
#   Takes a script file and runs command-runner in parse-only mode against an input
#   file with the same name
#
#   Usage:
#   crparse <script name> [input file]
#
#   Examples:
#
#   This example would run command-runner in mode parse-only against
#   "rest-mgmt-tm-sys-version.ind" with input file "rest-mgmt-tm-sys-version.input"
#   from either the current working directory or ./test:
#
#   crparse rest-mgmt-tm-sys-version.ind
#
#   This example would run command-runner in mode parse-only against
#   "rest-mgmt-tm-sys-version.ind" with input file "rest-mgmt-tm-sys-version.input"
#   from either the current working directory or ./test:
#
#   crparse rest-mgmt-tm-sys-version.ind rest-mgmt-tm-sys-version.input
#
##################################################################################################################

crparse()
{
    if [ -z "$2" ]; then
        if [ -f "./${1%.*}.input" ]; then
            command-runner.sh parse-only $1 -f "${1%.*}.input"
        elif [ -f "./test/${1%.*}.input" ]; then
            command-runner.sh parse-only $1 -f "./test/${1%.*}.input"
        else
            echo -e "No input files.\n\nTried these locations:\n$(echo ./test/${1%.*}.input)\n$(echo ${1%.*}.input)"
        fi
    else
        command-runner.sh parse-only $1 -f $2
    fi
}

#Same as above, but verbose output
crparseverbose()
{
    if [ -z "$2" ]; then
        if [ -f "./${1%.*}.input" ]; then
            command-runner.sh parse-only --verbose $1 -f "${1%.*}.input"
        elif [ -f "./test/${1%.*}.input" ]; then
            command-runner.sh parse-only --verbose $1 -f "./test/${1%.*}.input"
        else
            echo -e "No input files.\n\nTried these locations:\n$(echo ./test/${1%.*}.input)\n$(echo ${1%.*}.input)"
        fi
    else
        command-runner.sh parse-only $1 -f $2
    fi
}

##################################################################################################################
#
#                                   gotrules / goparsers
#	Save time bouncing between directories
#
##################################################################################################################

gorules()
{
    cd /usr/share/indeni/rules
}
goparsers()
{
    cd /usr/share/indeni-collector/kb/parsers
}
gooverride(){
    cd /usr/share/indeni-knowledge/overwrite/parsers
}

##################################################################################################################
#
#                                   gotorules / gotoparsers
#	Acknowledge multiple alerts in one go.
#
#	Usage:
#	delbyheadline <alert headline>
#
##################################################################################################################

delbyheadline()
{
    psql -c "delete from alert where headline LIKE '%$1%';"
}

##################################################################################################################
#
#                                   getlivemetrics
#
#	Usage:
#	getlivemetrics <metric name> <partial device name> [time frame in minutes]
#
#	metric name: Exact name of the metric you want data for
#	partial device name: A string that your device name contains. Example: F5 would match F501.domain.local and F502.domain.local, but only one of them will be used.
#	time frame in minutes: The number of minutes you want data for. Can be any number between 1-59.
#
#	Examples:
#
#	Get the lb-snatpool-limit from a device containing F5:
#	getlivemetrics lb-snatpool-limit F5
#
#	Get the values for config-unsaved from the last 30 minutes from a device containing CP01:
#	getlivemetrics lb-snatpool-limit CP01 30
#
##################################################################################################################

getlivemetrics(){
    
    commandusage="getlivemetrics <metric name> <partial device name> [time frame in minutes, 1-59]"
    
    if [ -z "$1" ]; then
        echo "You have to specify a metric name"
        echo "$commandusage"
        return 1
    fi

    if [ -z "$2" ]; then
        echo "You have to specify a device name"
        echo "$commandusage"
        return 1
    fi
    
    if [ -z "$3" ]; then
        timeframe=59
    else
        if [ "$3" -gt 59 ]; then
            echo "The time frame can't be higher than 59"
            echo "$commandusage"
            return 1
        else
            timeframe=$3
        fi
    fi
    
    #Get the name and device id of the device
    devicenames=$(curl -sGku "$INDENIUSER:$INDENIPASSWORD" "https://localhost:9009/api/v1/devices" | jq "[.[].tags | select(.[\"device-name\"] | contains(\"$2\")) | { name: .[\"device-name\"] }]")
    deviceids=$(curl -sGku "$INDENIUSER:$INDENIPASSWORD" "https://localhost:9009/api/v1/devices" | jq "[.[].tags | select(.[\"device-name\"] | contains(\"$2\")) | { id: .[\"device-id\"] }]")
	
    if [ "$deviceids" == "[]" ]; then
        echo "No device found"
    else
        devicename=$(echo $devicenames | jq ".[0] | .[]" | tr -d "\"" )
        deviceid=$(echo $deviceids | jq ".[0] | .[]" | tr -d "\"" )

        echo "Getting data from $devicename using device id $deviceid"
        curl -sfGku "$INDENIUSER:$INDENIPASSWORD" "https://localhost:9009/api/v1/metrics" --data-urlencode "query=(im.name==$1 and device-id=='$deviceid')" --data-urlencode "start=`date -d -$(echo $timeframe)minute +"%s000"`" | sed 's/},{/\n\n/g' |sed 's/}]/\n\n/g' | sed 's/","/\n/g' |sed 's/":"/: /g' |sed 's/\[/\n/g' |sed 's/,/ /g' | sed 's/"tags":{"//g'
    fi
    
}

# Restarts indeni services
restartindeni(){
    sudo service indeni-collector restart; sudo service indeni-server restart
}

##################################################################################################################
#
#                                   loadrule
#   Description:
#   Touches a rule file to trigger the indeni server loading it and then parses the log file to
#   see if it was loaded successfully or not.
#
#   Usage:
#   loadrule rulefile
#
#   Parameters:
#   Rule file: Exact, or relative path to the rule file (ie test.rule)
#
##################################################################################################################

loadrule(){

    if [ -z $1 ]; then
        echo -e "Usage:\nloadrule <file name>"
        return 1
    fi

    if [ ! -f $1 ]; then
        echo "File not found"
        return 1
    fi

    echo "Loading rule..."

    touch $1

    sleep 4

    tail -n10000 /usr/share/indeni/logs/indeni.log | awk "

    function red(s) {
        printf \"\033[1;31m\" s \"\033[0m \"
    }

    function green(s) {
        printf \"\033[1;32m\" s \"\033[0m \"
    }

    /^INFO.*Successfully loaded rule.*$1/{
        delete text
        i = 1
        text[i] = \"Successfully loaded the rule\"
    }

    /^ERROR.*$1/{
        delete text
        i = 1
        text[i] = \$0
        next
    }

    /^\!/ && i > 0 {
        i++
        text[i] = \$0
        next
    }

    /^[^\!]/ && i > 0 {
        i = 0
    }

    END {

        arrLength = 1
        for(i in text){
            arrLength++
        }

        if(arrLength == 2 && text[1]){
            print green(text[1])
        } else {
            print red(\"Failed to load rule with error message:\")
            for(i = 1; i < arrLength; i++){
                print text[i];
            }
        }
    }"

}
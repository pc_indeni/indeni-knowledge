package com.indeni.server.rules.library

import com.indeni.ruleengine.expressions.conditions.Equals
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class HostMissingRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_host_missing_rule", "Linux-based Devices: Host missing from hosts file",
    "In some Linux systems the current host may disappear from the /etc/hosts file. This shouldn't happen, but if it does indeni will trigger an issue.", AlertSeverity.ERROR).build()

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("hostname-exists-etc-hosts").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("hostname-exists-etc-hosts"), denseOnly = false),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            Equals(
              inUseValue,
              ConstantExpression(Some(0.0)))
        ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("The hostname of this device should appear in the hosts file (/etc/hosts) but is missing."),
        ConditionalRemediationSteps("This may be due to a misconfiguration of the host itself. Review the network and host/domain configuration of this device.",
          ConditionalRemediationSteps.VENDOR_CP -> "Review sk97842: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk97842"
        )
    )
  }
}

package com.indeni.server.rules.library

import com.indeni.ruleengine.Scope.{Scope, ScopeValueHelper}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.utility.IsEmptyExpression.IsEmptyExpressionHelper
import com.indeni.ruleengine.expressions.utility.SeqDiffWithoutOrderExpression
import com.indeni.ruleengine.expressions.{Expression, conditions}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.rules.{RuleMetadata, _}
import com.indeni.server.sensor.models.automationpolicy.AutomationPolicyItemAlertSetting
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


/**
  * Created by amir on 19/02/2017.
  */
case class SyslogServersInUseComplianceCheckRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val linesParameterName = "syslog_config_lines"
  private val linesParameter = new ParameterDefinition(
    linesParameterName,
    "",
    "Syslog servers config needed (multi-line, <ip-address>, <min-severity>)",
    "List the syslog servers need to be configured, and their minimal severity.\ne.g. 1.1.1.1, info.",
    UIType.MULTILINE_TEXT,
    "")


  override def expressionTree: StatusTreeExpression = {
    val linesExpected = DynamicParameterExpression.withConstantDefault(linesParameter.getName, Seq[Seq[String]]()).withLazy

    val snapshotKey = "syslog-servers"
    val missings =
      SeqDiffWithoutOrderExpression(
        MultiSnapshotExtractVectorExpression(SnapshotExpression(snapshotKey).asMulti().mostRecent().value(), "host", "severity"),
        linesExpected
      ).missings
        .withLazy

    val missingLineHeadline = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String = scope.getVisible("syslogConfig").get.toString

      override def args: Set[Expression[_]] = Set()
    }

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The snapshot we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set(snapshotKey)).multi(),

        // The condition which, if true, we have an issue. Checked against the snapshots we've collected
        missings.nonEmpty,
        multiInformers = Set(
          MultiIssueInformer(
            missingLineHeadline,
            EMPTY_STRING,
            "Missing configuration"
          ).iterateOver(
            missings,
            "syslogConfig",
            conditions.True
          ))

        // Details of the alert itself
      ).withRootInfo(
        getHeadline(),
        ConstantExpression("Indeni has found that some syslog servers are missing or misconfigured. Check the list below."),
        ConditionalRemediationSteps("Modify the device's configuration as required.",
          ConditionalRemediationSteps.OS_NXOS ->
            """|1. Check if the IP address of the syslog server is accessible. Use theVRF management option if the Mangement VRF is configured to access the syslog servers. In this case use the "ping <dest-ip> vrf management" command to ping the server otherwise ping the server without the vrf option.
               |2. Check that the appropriate logging level is enabled to send logging messages. Use the "show logging info" NX-OS command. If the logging level is not appropriate, then set the appropriate level by using the "logging level <feature> <log-level>" NX-OS command.
               |3. It is a best practice to have at least one and no more than three, syslog servers configured.
               |4. For more information please review: <a target="_blank" href="https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/system_management/502_n1_1/b_Cisco_n5k_system_mgmt_cg_rel_502_n1_1/Cisco_n5k_system_mgmt_cg_rel_502_n1_1_chapter9.html">Cisco NX-OS configuration guide</a>.""".stripMargin,
          ConditionalRemediationSteps.VENDOR_JUNIPER ->
            """|1. On the device command line interface execute "show system syslog" command to review system log configuration.
               |2. Check if the syslog server is accessible.
               |3. Check if the severity level is set properly to ensure that the traffic log messages are captured.
               |4. Consider specifying two remote syslog servers to which system logs are sent.
               |5. Review the following article on Juniper TechLibrary for more information: <a target="_blank" href="https://kb.juniper.net/InfoCenter/index?page=content&id=KB16502&actp=METADATA">SRX Getting Started - Configure System Logging</a>.""".stripMargin
        )
      ).asCondition()
    ).withoutInfo()
  }

  /**
    * @return The rule's metadata.
    */
  override def metadata: RuleMetadata =
    RuleMetadata.builder(
      "verification_syslog_servers_in_use",
      "Compliance Check: Syslog Servers In Use",
      "Indeni will verify that certain syslog servers are configured on a monitored device.",
      AlertSeverity.INFO)
      .defaultAction(AutomationPolicyItemAlertSetting.NEVER)
      .configParameter(linesParameter)
      .build()
}

package com.indeni.server.rules.library

import com.indeni.apidata.time.TimeSpan
import com.indeni.ruleengine.expressions.OptionalExpression
import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.HighLoadAverageRule.NAME
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class HighLoadAverageRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_Load_Average"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Five Minute Load Average",
    "What is the threshold for the five-minute load average for which once it is crossed an issue will be triggerd.",
    UIType.DOUBLE,
    15.0)

  override val metadata: RuleMetadata = RuleMetadata.builder(NAME, "Linux-based Devices: High load average",
    "indeni will trigger an issue when the load average on a given device seems high.",
    AlertSeverity.ERROR).interval(TimeSpan.fromMinutes(10)).configParameter(highThresholdParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("load-average-five-minutes").last
    val threshold: OptionalExpression[Double] = getParameterDouble(highThresholdParameter)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("load-average-five-minutes"), denseOnly = false),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            GreaterThanOrEqual(
              actualValue,
              threshold)

            // The Alert Item to add for this specific item
          ).withRootInfo(
              getHeadline(),
              scopableStringFormatExpression("The five-minute load average is %.0f, above the threshold of %.0f.", actualValue, threshold),
              ConstantExpression("Review the current activity on the device to determine if there is a specific cause for this.")
          ).asCondition()
    ).withoutInfo()
  }
}

object HighLoadAverageRule {

  /* --- Constants --- */

  private[library] val NAME = "linux_high_load_average"
}

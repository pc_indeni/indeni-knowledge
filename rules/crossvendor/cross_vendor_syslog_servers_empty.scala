package com.indeni.server.rules.library.crossvendor

import com.indeni.ruleengine.expressions.conditions.Equals
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, MultiSnapshotValueCheckTemplateRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class cross_vendor_syslog_servers_empty(context: RuleContext) extends MultiSnapshotValueCheckTemplateRule(context,
  severity = AlertSeverity.WARN,
  ruleName = "cross_vendor_syslog_servers_empty",
  ruleFriendlyName = "All Devices: No syslog servers are configured",
  ruleDescription = "Ιndeni will alert if no syslog servers are configured for a specific device.",
  metricName = "syslog-servers",
  alertDescription = "No syslog servers are configured on the device. It is critical to have at least one syslog server to collect device events on an external server. The logging information can be used to detect events and troubleshoot and analyze failures.",
  baseRemediationText = "Add syslog servers.",
  complexCondition = Equals(RuleHelper.createEmptyComplexArrayConstantExpression(), SnapshotExpression("syslog-servers").asMulti().mostRecent().value().noneable)
)(ConditionalRemediationSteps.OS_NXOS ->
  """|
    |1. Configure at least one syslog server using the "logging server <ip address> <severity> [facility <facility>] [use-vrf <vrf-name>]" NX-OS command
    |2. It is recommended to set the severity value to 6 (informational).
    |NOTE: The default syslog facility used by Nexus switches is "local7" . The default VRF is "management".  If you want syslog messages to be sent over the in-band global routing, use "default".
    |3. For more information please review the next configuration guide:
    |https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/5_x/nx-os/system_management/configuration/guide/sm_nx_os_cg/sm_5syslog.html""".stripMargin)

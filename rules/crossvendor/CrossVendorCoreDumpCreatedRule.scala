package com.indeni.server.rules.library.crossvendor

import com.indeni.ruleengine.expressions.conditions.{And, Equals, Not, Or}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.utility.IsEmptyExpression.IsEmptyExpressionHelper
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.rules.library.{ConditionalRemediationSteps, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

import scala.language.reflectiveCalls

case class CrossVendorCoreDumpCreatedRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_core_dump_created", "All Devices: Core dump files found",
    "A core dump is created when a process crashes. Indeni will alert when a core dump file is created.", AlertSeverity.ERROR)
    .build()

  override def expressionTree: StatusTreeExpression = {
    val currentValue = SnapshotExpression("core-dumps").asMulti().mostRecent().value()
    val previousValue = SnapshotExpression("core-dumps").asMulti().middle().optionValue()

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set("core-dumps")).multi(),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        And(
          currentValue.nonEmpty,
          previousValue.notEquals(currentValue)
        )

      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("A core dump file has been created. This happens when a process crashes, and the core dump can contain information on why this happened. This usually means that there is an issue that should be investigated."),
      ConditionalRemediationSteps("The list of core dumps is retrieved by logging into the shell over SSH and retrieving the details of files found in /var/log/dump/usermode/, /var/tmp/*core* or /var/crash. Investigate the core dump files. If the issue is not clear, open up a case with vendor support and send them the file.",
        ConditionalRemediationSteps.VENDOR_F5 ->
          """|Login to your device with SSH and run "ls /var/core". Investigate the core dump files.
             |If the cause of the issue is not clear, open up a case with F5 and follow the instructions on this page in order to provide them with the information needed:
             |<a target="_blank" href="https://support.f5.com/csp/article/K10062">Article: K10062 on AskF5</a>.""".stripMargin,
        ConditionalRemediationSteps.VENDOR_JUNIPER ->
          """|1. Run "show system core-dumps" command to locate system core files. The core dump files usually are located in "/var/log/dump/usermode/", "/var/tmp/*core*" or "/var/crash" directories.
             |2. Check for core-dumps that were created at to the time of failover.
             |3. If there is a core-dump created at the time of failover, consider uploading the file to the Juniper Networks Technical Assistance Center (JTAC) FTP server for further analysis and opening a case with the JTAC.
             |4. Review the following articles on Juniper TechLibrary for more information: <a target="_blank" href="https://kb.juniper.net/InfoCenter/index?page=content&id=KB18867">Syslog message: Core dumped</a>
             |<a target="_blank" href="https://kb.juniper.net/InfoCenter/index?page=content&id=KB26963">How to get a core-dump off the router and to the Juniper FTP server</a>""".stripMargin)
    )
  }
}

package com.indeni.server.rules.library.crossvendor

import com.indeni.ruleengine.expressions.conditions.{And, GreaterThan, LesserThan}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, _}
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.{ConditionalRemediationSteps, RuleHelper}
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.server.common.data.conditions.True
import com.indeni.apidata.time.TimeSpan
import com.indeni.apidata.time.TimeSpan.TimePeriod

case class CrossVendorCertificateWillExpireRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "Ahead_Alerting_Threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
                                                               "",
                                                               "Expiration Threshold",
                                                               "How long before expiration should Indeni alert.",
                                                               UIType.TIMESPAN,
                                                               TimeSpan.fromDays(56))

  override val metadata: RuleMetadata = RuleMetadata
    .builder(
      "cross_vendor_certificate_will_expire",
      "All Devices: Certificate(s) expiration nearing",
      "Indeni will alert when a certificate is about to expire. " +
        "The threshold for the number of days before certificate expiration can be adjusted by the user.",
      AlertSeverity.ERROR
    )
    .configParameter(highThresholdParameter)
    .build()

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("certificate-expiration").last.toTimeSpan(TimePeriod.SECOND)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),
      // What constitutes an issue
      StatusTreeExpression(
        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("certificate-expiration")),
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("certificate-expiration"), denseOnly = false),
          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            GreaterThan(
              actualValue,
              NowExpression()
            ),
            LesserThan(actualValue,
                       PlusExpression[TimeSpan](NowExpression(),
                                                getParameterTimeSpanForTimeSeries(highThresholdParameter)))
          )
          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
            scopableStringFormatExpression("${scope(\"name\")}"),
            scopableStringFormatExpression("Will expire on %s", timeSpanToDateExpression(actualValue)),
            title = "Affected Certificates"
          )
          .asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more certificates are about to expire. See the list below."),
      ConditionalRemediationSteps(
        "Renew any certificates that need to be renewed.",
        ConditionalRemediationSteps.VENDOR_CP ->
          """Please review:
            |<a target="_blank" href="https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk104400">Solution sk104400 on Check Point Support Center</a>
            |and the articles to which it links at the bottom.""".stripMargin,
        ConditionalRemediationSteps.VENDOR_PANOS ->
          """Please review this article on Palo Alto Networks Support Site:
            |<a target="_blank" href="https://www.paloaltonetworks.com/documentation/60/pan-os/pan-os/certificate-management/revoke-and-renew-certificates\">Revoke and Renew Certificates</a>.""".stripMargin,
        ConditionalRemediationSteps.VENDOR_FORTINET ->
          """
            |1. Login via ssh to the Fortinet firewall and run the FortiOS command “get vpn certificate <X> detail”  to review the period for which the certificate is valid.
            |2. Login via ssh to the Fortinet firewall and run the FortiOS command “get vpn certificate setting” to review the settings.
            |3. Login via https to the Fortinet firewall and go to the menu System > Certificates tab to review the list of the certificates. Double click each certificate to get detailed information.
            |4. For more information review the Fortinet Certification Configuration Guide: http://help.fortinet.com/fos50hlp/54/Content/FortiOS/fortigate-authentication-54/Certificates.htm
            |5. If the problem persists, contact Fortinet Technical support at https://support.fortinet.com/ for further assistance.""".stripMargin
      )
    )
  }
}

package com.indeni.server.rules.library

import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.cluster.SelectClusterComparisonExpression
import com.indeni.ruleengine.expressions.conditions.{And, Equals, GreaterThanOrEqual, Not}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.SelectTagsExpression
import com.indeni.ruleengine.expressions.math.MinExpression
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.metrics.DeviceStoreActor
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.rules.{RuleContext, RuleMetadata, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

import scala.concurrent.duration._

/**
  * Note: An this rule will result with an issue only if the following condition is yields with a result equals to True
  * assuming the snapshot samples are being reported relatively on the same time
  *
  * {{{
  *  this(most-resent.value) != cluster(most-resent.value) &&
  *  min((eval-time - this(most-recent.first-seen)), (eval-time - cluster(most-recent.first-seen))) > th
  *
  * case 1: -------------------------------------------> T
  * this    : o, o, o, o, o, o, o, o, x, x ,x, x, x, x
  * cluster : o, o, o, o, x, x, x, x, x ,x, x, x, x, x
  *
  * case 2:
  * this    : o, o, o, o, x, x, x, x, x ,x, x, x, x, x
  * cluster : o, o, o, o, o, o, o, o, x, x ,x, x, x, x
  *
  * case 3:
  * this    : o
  * cluster : x
  *
  * case 4:
  * this    : None
  * cluster : o
  *
  * case 5: (rare) not addressed in with this rule
  * this    : o, o ,o, o, o, o, z, z, z ,z ,z, z, z, z
  * cluster : o, o ,o, o, x, y, y, z, z ,z ,z, z, z, z
  * }}}
  */
class SnapshotComparisonTemplateRule(context: RuleContext,
                                     ruleName: String,
                                     ruleFriendlyName: String,
                                     severity: AlertSeverity = AlertSeverity.ERROR,
                                     ruleDescription: String,
                                     metricName: String,
                                     isArray: Boolean,
                                     applicableMetricTag: String = null,
                                     alertDescription: String,
                                     alertItemsHeader: String = "Mismatching Cluster Members",
                                     descriptionMetricTag: String = null,
                                     descriptionStringFormat: String = "",
                                     baseRemediationText: String,
                                     metaCondition: TagsStoreCondition = True,
                                     samplesTimeDifferenceThreshold: Option[FiniteDuration] = Some(5 minutes),
                                     includeSnapshotDiff: Boolean = true)(vendorToRemediationText: (String, String)*)
  extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder(ruleName, ruleFriendlyName, ruleDescription, severity).build()

  override def expressionTree: StatusTreeExpression = {

    val diffThresholdExpression = ConstantExpression[Option[Long]](samplesTimeDifferenceThreshold.map(_.toMillis).orElse(Some(0)))

    val thisSnapshot = SelectClusterComparisonExpression.thisSnapshotExpression().mostRecent()
    val clusterSnapshot = SelectClusterComparisonExpression.clusterSnapshotExpression().mostRecent()

    val thisSnapshotValue = thisSnapshot.value().noneable.withLazy
    val clusterSnapshotValue = clusterSnapshot.value().noneable.withLazy

    val thisSnapshotFirstSeen = thisSnapshot.firstSeen().noneable.withLazy
    val clusterSnapshotFirstSeen = clusterSnapshot.firstSeen().noneable.withLazy

    val timeDifferenceExpression = MinExpression(new Expression[Seq[Option[Long]]] {

      override def eval(time: Long): Seq[Option[Long]] = {
        Seq(thisSnapshotFirstSeen, clusterSnapshotFirstSeen).map(_.eval(time).map(time - _))
      }

      override def args: Set[Expression[_]] = Set(thisSnapshotFirstSeen, clusterSnapshotFirstSeen)
    })

    val selectExpression = SelectClusterComparisonExpression(context.metaDao, DeviceStoreActor.TAG_DEVICE_ID,
      DeviceStoreActor.TAG_DEVICE_NAME, DeviceStoreActor.TAG_DEVICE_IP, "cluster-id", context.snapshotsDao, metricName)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      // What constitutes an issue
      if (applicableMetricTag != null)
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.snapshotsDao, if (descriptionMetricTag == null) Set(applicableMetricTag) else Set(applicableMetricTag, descriptionMetricTag), withTagsCondition(metricName)),

          StatusTreeExpression(
            if (isArray) selectExpression.multi() else selectExpression.single(),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            And(Not(Equals(thisSnapshotValue, clusterSnapshotValue)), GreaterThanOrEqual(timeDifferenceExpression, diffThresholdExpression))

            // The Alert Item to add for this specific item
          ).withSecondaryInfo(
            scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}" + (if (descriptionMetricTag != null) " (${scope(\"" + descriptionMetricTag + "\")})" else "") + " on %s (%s)", SelectClusterComparisonExpression.clusterDeviceNameExpression(), SelectClusterComparisonExpression.clusterDeviceIpExpression()),
            if (includeSnapshotDiff) SnapshotDiffExpression(thisSnapshotValue, clusterSnapshotValue) else EMPTY_STRING,
            title = alertItemsHeader
          ).asCondition()
        ).withoutInfo().asCondition()
      else
        StatusTreeExpression(
          // The time-series we check the test condition against:
          if (isArray) selectExpression.multi() else selectExpression.single(),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(Not(Equals(thisSnapshotValue, clusterSnapshotValue)), GreaterThanOrEqual(timeDifferenceExpression, diffThresholdExpression))
        ).withSecondaryInfo(
          scopableStringFormatExpression("%s (%s)", SelectClusterComparisonExpression.clusterDeviceNameExpression(), SelectClusterComparisonExpression.clusterDeviceIpExpression()),
          if (includeSnapshotDiff) SnapshotDiffExpression(thisSnapshotValue, clusterSnapshotValue) else EMPTY_STRING,
          title = alertItemsHeader
        ).asCondition()

      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(alertDescription),
      ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
    )
  }
}


package com.indeni.server.rules.library

import com.indeni.ruleengine.Scope.{Scope, ScopeValueHelper}
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions.Condition
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.SnapshotExpression.SingleDimensionSnapshotExpression
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

import scala.util.matching.Regex

class SingleSnapshotValueCheckTemplateRule(context: RuleContext,
                                           ruleName: String,
                                           ruleFriendlyName: String,
                                           ruleDescription: String,
                                           severity: AlertSeverity = AlertSeverity.ERROR,
                                           metricName: String,
                                           applicableMetricTag: String = null,
                                           alertDescription: String,
                                           alertItemsHeader: String = null,
                                           baseRemediationText: String,
                                           complexCondition: Condition,
                                           metaCondition: TagsStoreCondition = True,
                                           itemSpecificDescription: Seq[(Regex, String)] = Seq(".*".r -> ""),
                                           alertItemHeadlineExpersion: Expression[String] = EMPTY_STRING,
                                           headlineFormat: String = "%s")
                                          (vendorToRemediationText: (String, String)*)
  extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder(ruleName, ruleFriendlyName, ruleDescription, severity).build()

  override def expressionTree: StatusTreeExpression = {
    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      // What constitutes an issue
      if (null != applicableMetricTag) StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.snapshotsDao, Set(applicableMetricTag), withTagsCondition(metricName)),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).single(),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          complexCondition
        ).withSecondaryInfo(
          scopableStringFormatExpression(headlineFormat, scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}"), alertItemHeadlineExpersion),
          new ScopableExpression[String] {
            override protected def evalWithScope(time: Long, scope: Scope): String = {
              val metricTagValue = scope.getVisible(applicableMetricTag).get.toString
              itemSpecificDescription.collectFirst {
                case item if (!item._1.findFirstMatchIn(metricTagValue).isEmpty) => item._2
              }.get
            }

            override def args: Set[Expression[_]] = Set()
          },
          title = alertItemsHeader
        ).asCondition()

      ).withoutInfo().asCondition()
      else StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).single(),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        complexCondition
      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(alertDescription),
      ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
    )
  }
}

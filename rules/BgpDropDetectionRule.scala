package com.indeni.server.rules.library

import com.indeni.ruleengine.expressions.OptionalExpression
import com.indeni.ruleengine.expressions.core.{ConstantExpression, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.tools.ChangeDetectionExpression
import com.indeni.ruleengine.expressions.utility.IsEmptyExpression.IsEmptyExpressionHelper
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.rules.{DeviceKey, RuleContext, RuleMetadata}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class BgpDropDetectionRule(context: RuleContext) extends PerDeviceRule with RuleHelper {


  private[library] val confidenceLevelParameterName = "Confidence_Level_Threshold"
  private val confidenceLevelParameter = new ParameterDefinition(confidenceLevelParameterName,
    "",
    "Confidence Level (%)",
    "If the number of BGP received routes drops drastically, an issue will be triggerd..",
    UIType.DOUBLE,
    95)


  override val metadata: RuleMetadata = RuleMetadata.builder(
    "cross_vendor_bgp_drop_detection",
    "All Devices: Drop in the number of BGP received routes",
    "indeni will track the number of BGP connections to detect when the number of connections drops.",
    AlertSeverity.INFO).configParameters(confidenceLevelParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val confidenceLevel: OptionalExpression[Double] = getParameterDouble(confidenceLevelParameter)
    val actualValue = TimeSeriesExpression[Double]("bgp-received-routes")
    val changeDetectionExp = ChangeDetectionExpression(actualValue, confidenceLevel).down().withLazy

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("bgp-received-routes")),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("bgp-received-routes")),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          changeDetectionExp.nonEmpty
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"name\")}"),
          scopableStringFormatExpression("Number of BGP received routes dropped by %.2f%%. From %.2f on average to %.2f on average", changeDetectionExp.changePercentage, changeDetectionExp.before, changeDetectionExp.after),
          title = "Drop in BGP routes"
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(
        "Number of BGP connections dropped drastically."),
      ConstantExpression(
        "Identify any network and server issues which may be causing this.")
    )
  }
}

package com.indeni.server.rules.library

import com.indeni.apidata.time.TimeSpan
import com.indeni.apidata.time.TimeSpan.TimePeriod
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.ThresholdDirection.ThresholdDirection
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

class TimeIntervalThresholdOnDoubleMetricTemplateRule(context: RuleContext,
                                                      ruleName: String,
                                                      ruleFriendlyName: String,
                                                      ruleDescription: String,
                                                      severity: AlertSeverity = AlertSeverity.ERROR,
                                                      metricName: String,
                                                      threshold: TimeSpan,
                                                      metricUnits: TimePeriod,
                                                      metaCondition: TagsStoreCondition = True,
                                                      alertDescriptionFormat: String,
                                                      alertDescriptionValueUnits: TimePeriod,
                                                      baseRemediationText: String,
                                                      thresholdDirection: ThresholdDirection = ThresholdDirection.ABOVE)(vendorToRemediationText: (String, String)*)
    extends PerDeviceRule
    with RuleHelper {

  private[library] val thresholdParameterName = "threshold"
  private val thresholdParameter =
    new ParameterDefinition(
      thresholdParameterName,
      "",
      "Alerting Threshold",
      "indeni will trigger an issue if the value is " + (if (thresholdDirection == ThresholdDirection.ABOVE) "above" else "below") + " this value.",
      UIType.TIMESPAN,
      threshold
    )

  override val metadata: RuleMetadata = RuleMetadata
    .builder(ruleName, ruleFriendlyName, ruleDescription, AlertSeverity.ERROR)
    .configParameter(thresholdParameter)
    .build()

  override def expressionTree: StatusTreeExpression = {

    // This is the value used to evaluate this rule
    val value = TimeSeriesExpression[Double](metricName).last.toTimeSpan(metricUnits)

    // This is the threshold value for the rule condition
    val thresholdValue = getParameterTimeSpanForRule(thresholdParameter).noneable

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), denseOnly = false),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        generateCompareCondition(thresholdDirection, value, thresholdValue)

        // The Alert Item to add for this specific item
      ).withRootInfo(
          getHeadline(),
          scopableStringFormatExpression(alertDescriptionFormat, value.toDouble(alertDescriptionValueUnits)),
          ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
        )
        .asCondition()
    ).withoutInfo()
  }
}

package com.indeni.server.rules.library.templatebased.f5

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._
//import com.indeni.server.rules.library.f5._
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression

/**
  * Created by yoni on 4/5/17.
  * Updated by Indeni_PJ 2017-07-07
  */

case class F5DefaultPortLockDown(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "f5_default_port_lockdown",
  ruleFriendlyName = "F5 Devices: Self IP port lockdown is set to default",
  ruleDescription = """In earlier versions of TMOS the default port lockdown setting was "default". Leaving this setting in place could allow an attacker access to the management services of the device.
                      |Indeni will alert if port lockdown is set to "Default".""".stripMargin,
  metricName = "f5-default-port-lockdown",
  applicableMetricTag = "name",
  alertItemsHeader = "Self IP's Affected",
  alertDescription = """There are self IPs configured on this device which are configured with port lockdown setting "default".
                       |This alert was added per the request of <a target="_blank" href="https://se.linkedin.com/in/patrik-jonsson-6527932">Patrik Jonsson</a>.""".stripMargin,
  baseRemediationText = """Unless this is intentionally configured, such as a dedicated cable or VLAN for HA, it is always recommended to have the Self IP configuration set to "Allow None". Make sure to schedule a service window before configuring this option.
                          |
                          |Note:
                          |ICMP traffic to the self-IP address is not affected by the port lockdown list and is implicitly allowed in all cases.
                          |
                          |More information about port lockdown:
                          |Version 11.x - https://support.f5.com/csp/article/K13250
                          |Version 12.x - https://support.f5.com/csp/article/K17333""".stripMargin,
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("f5-default-port-lockdown").asSingle().mostRecent().value().noneable))()

package com.indeni.server.rules.library.templatebased.f5.compliance

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SingleSnapshotComplianceCheckTemplateRule

case class F5ComplianceCheckDefaultNodeMonitorConfigured(context: RuleContext) extends SingleSnapshotComplianceCheckTemplateRule(context,
  ruleName = "f5_compliance_check_default_node_monitor_configured",
  ruleFriendlyName = "F5 Compliance Check: Default node monitor is not configured",
  ruleDescription = "It is good practice to have a basic check for node monitors as it's easier to fast establish correlations between multiple failing members during an outage. Indeni will alert when a device that supports node monitoring does not have it configured.",
  metricName = "f5-default-node-monitor-configured",
  parameterName = "Should Default Node Monitors Be Configured",
  parameterDescription = "If this is set to \"on\" or ticked, Indeni will alert when a device that supports node monitoring does not have it configured.",
  expectedValue = true,
  baseRemediationText = """Log in to the web interface. Click on "Local Traffic" -> "Nodes" -> "Default Monitor" and then assign a default monitor.
											 |This particular monitor should determine status on a node level and thus ICMP is recommended.
											 |
											 |Make sure to do this during a scheduled service window as a failed node monitor brings down all members that the node is associated to.""".stripMargin)()

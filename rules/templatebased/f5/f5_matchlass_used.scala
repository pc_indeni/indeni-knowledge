package com.indeni.server.rules.library.templatebased.f5

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class f5_matchlass_used(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "f5_matchlass_used",
  ruleFriendlyName = "F5 Devices: iRule(s) uses the deprecated matchclass command",
  ruleDescription = "The matchclass command in iRules has been deprecated. indeni will alert if any iRules still use it.",
  metricName = "f5-matchclass-used",
  applicableMetricTag = "name",
  alertItemsHeader = "iRules Affected",
  alertDescription = "The command \"matchclass\" is used to check if a value is contained within a data group list. While still supported the command has been deprecated in favor of the more powerful and efficient \"class\" command.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  baseRemediationText = "Information about the class command can be found at https://devcentral.f5.com/wiki/iRules.class.ashx",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("f5-matchclass-used").asSingle().mostRecent().value().noneable))()

package com.indeni.server.rules.library.templatebased.f5

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class f5_fallback_host_used(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "f5_fallback_host_used",
  ruleFriendlyName = "F5 Devices: Fallback host used in HTTP profile",
  ruleDescription = "A fallback host redirect a user to a different page/URI. It is in most cases better to use an iRule to rewrite the request. indeni will alert if fallback is used instead of an iRule.",
  metricName = "f5-fallbackhost-used",
  applicableMetricTag = "name",
  alertItemsHeader = "Profiles Affected",
  alertDescription = "A fallback host redirect a user to a different page/URI. It is in most cases better to use an iRule to rewrite the request. That way the user maintains the same URI and can hit refresh until the page is available again.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  baseRemediationText = "It is in most cases better to use an iRule to rewrite the request. That way the user maintains the same URI and can hit refresh until the page is available again.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("f5-fallbackhost-used").asSingle().mostRecent().value().noneable))()

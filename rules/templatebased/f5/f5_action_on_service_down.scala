package com.indeni.server.rules.library.templatebased.f5

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class f5_action_on_service_down(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "f5_action_on_service_down",
  ruleFriendlyName = "F5 Devices: Default Action On Service Down used",
  ruleDescription = "The default option for \"Action On Service Down\" is \"None\", which maintains connections to pool member even when the monitor fails, but does not create new connections.\nIf using a good monitor that is able to determine the status of the member however, the better option in most cases is \"Reject\", which instead resets the existing connection and forces the client to establish a new one. This ensures that the client has an optimal chance of connecting to a functioning pool member.\nindeni will alert when the device configuration does not follow this best practice.",
  metricName = "f5-default-action-on-service-down",
  applicableMetricTag = "name",
  alertItemsHeader = "Pools Affected",
  alertDescription = "The default option for \"Action On Service Down\" is \"None\", which maintains connections to pool member even when the monitor fails, but does not create new connections.\nIf using a good monitor that is able to determine the status of the member however, the better option in most cases is \"Reject\", which instead resets the existing connection and forces the client to establish a new one. This ensures that the client has an optimal chance of connecting to a functioning pool member.\nAs with many things, there are exceptions to this, see the link below for more information.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  baseRemediationText = "Read more about \"Action On Service Down\" at https://support.f5.com/csp/article/K15095",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("f5-default-action-on-service-down").asSingle().mostRecent().value().noneable))()

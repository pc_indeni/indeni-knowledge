package com.indeni.server.rules.library.templatebased.f5

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class f5_port_not_locked_down(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "f5_port_not_locked_down",
  ruleFriendlyName = "F5 Devices: Self IP not locked down",
  ruleDescription = "Best practices dictate that the self IP should be locked down to admin services. indeni will alert if this is not the case.",
  metricName = "f5-port-lockdown-not-none",
  applicableMetricTag = "name",
  alertItemsHeader = "Self IP's affected",
  alertDescription = "There are self IPs configured on this device which are listening to open ports.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  baseRemediationText = "Unless this is intentionally configured, such as a dedicated cable or VLAN for HA, it is always recommended to have the Self IP configuration set to \"Allow None\". Make sure to schedule a service window before configuring this option.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("f5-port-lockdown-not-none").asSingle().mostRecent().value().noneable))()

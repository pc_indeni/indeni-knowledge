package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class F5ManagementAllowAnyRule(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "F5ManagementAllowAnyRule",
  ruleFriendlyName = "F5 Devices: Configuration utility is not locked down",
  ruleDescription = "Allowing access to the F5 configuration utility from any IP address could allow an attacker to access the utility via another system on the same network. This rule retrieves a list of IP addresses that is allowed to access the configuration utility. Indeni will alert if \"All\" is present in the list.",
  metricName = "management-allow-any",
  alertDescription = "The configuration utility is accessible from any IP address.",
  baseRemediationText = """Follow the steps in this knowledge base article to lock down the configuration utility:
                          |https://support.f5.com/csp/article/K13309""".stripMargin,
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("management-allow-any").asSingle().mostRecent().value().noneable))()

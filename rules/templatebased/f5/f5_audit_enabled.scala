package com.indeni.server.rules.library.templatebased.f5

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class f5_audit_enabled(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "f5_audit_enabled",
  ruleFriendlyName = "F5 Devices: Audit logging is disabled",
  ruleDescription = "Audit logging is important for traceability reasons in case of an outage, or a successful intrusion attempt. indeni will alert if audit is not enabled.",
  metricName = "f5-audit-enabled",
  alertDescription = "Audit logging is important for traceability reasons in case of an outage, or a successful intrusion attempt.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  baseRemediationText = "An administrator could verify that auditing is enabled by logging into the web interface and clicking on \"System\" -> \"Logs\" -> \"Configuration\" -> \"Options\". On that page, make sure that audit logging for \"MCP\" and \"tmsh\" is set to either \"Enable\", \"Verbose\" or \"Debug\".\nMore information about TMM logging can be found here at https://support.f5.com/csp/article/K5532",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("disable"), SnapshotExpression("f5-audit-enabled").asSingle().mostRecent().value().noneable))()

package com.indeni.server.rules.library.templatebased.f5

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class f5_automap_enabled(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "f5_automap_enabled",
  ruleFriendlyName = "F5 Devices: Automap enabled",
  ruleDescription = "Automap works great for assymetric routing, but can result in port exhaustion. indeni will alert if automap is used.",
  metricName = "f5-automap-used",
  applicableMetricTag = "name",
  alertItemsHeader = "Virtual Servers Affected",
  alertDescription = "Automap works great when avoiding asymmetric routing, but in scenarios with a lot of traffic there could be port exhaustion unless more than one floating IP is defined for the member VLAN. For this reason it's better to use a SNAT Pool containing the appropriate amount of IP addresses used for address translation.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  baseRemediationText = "Information about automap, SNAT Pools and port exhaustion is available at https://support.f5.com/csp/article/K7820#exhaustion",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("f5-automap-used").asSingle().mostRecent().value().noneable))()

package com.indeni.server.rules.library.templatebased.f5

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class f5_precompressed_content_in_compression_profile(context: RuleContext) extends MultiSnapshotValueCheckTemplateRule(context,
  ruleName = "f5_precompressed_content_in_compression_profile",
  ruleFriendlyName = "F5 Devices: Precompressed content-type found in HTTP Compression profile",
  ruleDescription = "Using the F5 device to compress content is a way of accelerating application content delivery. However, compressing content that is already pre-compressed results in longer response times and is using up system resources in vain. indeni will alert when already-compressed content types are set for compression.",
  metricName = "f5-compression-profile-precompressed-content-types",
  applicableMetricTag = "name",
  alertItemsHeader = "Profiles Affected",
  alertDescription = "Using the F5 device to compress content is a way of accelerating application content delivery. However, compressing content that is already pre-compressed results in longer response times and is using up system resources in vain.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  baseRemediationText = "Remove any pre-compressed content type from the HTTP Compression profile. For each profile mentioned below, the specific content types that should be removed are listed.",
  complexCondition = RuleNot(RuleEquals(RuleHelper.createEmptyComplexArrayConstantExpression(), SnapshotExpression("f5-compression-profile-precompressed-content-types").asMulti().mostRecent().value().noneable)))()

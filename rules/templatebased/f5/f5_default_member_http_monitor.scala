package com.indeni.server.rules.library.templatebased.f5

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class f5_default_member_http_monitor(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "f5_default_member_http_monitor",
  ruleFriendlyName = "F5 Devices: Default HTTP monitor used for pool member",
  ruleDescription = "Using the default monitor is not advisable as it does not have a HTTP version, host header or even user agent. indeni will alert if the default HTTP monitor is used.",
  metricName = "f5-default-member-monitor-used",
  applicableMetricTag = "member-name",
  alertItemsHeader = "Pool Members Affected",
  alertDescription = "Using the default monitor is not advisable as it does not have a HTTP version, host header or even user agent. Depending on server na log settings this might generate white noise in application logs as it could be unsupported. Also you would not get reliable test results as it does not have a receive string configured. This means that status code 500, 404, 401, etc. are all considered as OK, while a connection reset is not.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  baseRemediationText = "Create an HTTP monitor specifically for the application and configure a receive string. An example of a receive string that would only accept status code 200 would be \"HTTP/1\\.1 200\".\nnote:\nWhen changing the monitor configuration there is always a risk of application down-time in case the configuration is wrong. Thus it is recommended to only do this during a service window.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("f5-default-member-monitor-used").asSingle().mostRecent().value().noneable))()

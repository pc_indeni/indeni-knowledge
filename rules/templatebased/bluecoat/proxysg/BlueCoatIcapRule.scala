package com.indeni.server.rules.library.templatebased.bluecoat.proxysg

import com.indeni.server.common.data.conditions.Equals
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

case class BlueCoatIcapRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "BlueCoatIcapRule",
  ruleFriendlyName = "Blue Coat Devices: ICAP connectivity issue",
  ruleDescription = "Indeni will generate a notification if an ICAP connectivity issue is discovered",
  metricName = "bluecoat-process-state",
  applicableMetricTag = "process-name",
  descriptionMetricTag = "description",
  alertItemsHeader = "Processes Affected",
  descriptionStringFormat = "${scope(\"description\")}",
  alertDescription = "ICAP connectivity issue has been discovered",
  baseRemediationText = """|1. Login via https to the ProxySG and go to Statistics > Content Analysis . Review the requests graph.
                           |2. Check if the unit is dealing with high traffic volume.
                           |3. Make sure that the ICAP service is up and running on the same port as the ProxySG. Configuration > Threat Protection > Malware Scanning > Edit.
                           |4. For more information review the following Bluecoat guides:
                           |- https://origin-symwisedownload.symantec.com/resources/webguides/proxysg/certification/sg_firststeps_webguide/Content/Troubleshooting/Malware%20Prevention/troubleshoot_failed_health_check.htm
                           |- https://origin-symwisedownload.symantec.com/resources/webguides/proxysg/certification/sg_firststeps_webguide/Content/Solutions/MalwarePrevention/add_proxyav.htm
                           |- https://origin-symwisedownload.symantec.com/resources/webguides/swg_ca/policy/Default.htm
                           |5. If the problem persists, contact Symantec Technical support at https://support.symantec.com for further assistance.""".stripMargin)()


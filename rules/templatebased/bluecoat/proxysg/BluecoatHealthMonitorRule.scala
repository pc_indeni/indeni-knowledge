package com.indeni.server.rules.library.templatebased.bluecoat.proxysg

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}

/**
  *
  */
case class BluecoatHealthMonitorRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "BluecoatHealthMonitorRule",
  ruleFriendlyName = "Blue Coat Devices: Critical conditions detected by health monitors",
  ruleDescription = "Blue Coat devices use health monitors to detect critical conditions. Indeni will alert if health monitors detect a critical condition.",
  metricName = "bluecoat-health-monitor",
  applicableMetricTag = "monitor-name",
  descriptionMetricTag = "monitor-state",
  alertItemsHeader = "Critical Condition Detected by Health Monitors",
  alertDescription = "A critical condition is detected by Blue Coat health monitor.",
  baseRemediationText = """Indeni checks the current health-check state of the different components of the ProxySG device (CPU , Memmory , Disk, License ,Database communication and more)

                          |1. Login via https to the ProxySG and go to Statistics > System > Resources . Review the related component.
                          |2. Check if it is peak time or if anything changed recently in the enivornment.
                          |3. If the problem persists, contact Symantec Technical support at https://support.symantec.com for further assistance.""".stripMargin) ()

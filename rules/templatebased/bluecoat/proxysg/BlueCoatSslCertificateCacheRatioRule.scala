package com.indeni.server.rules.library.templatebased.bluecoat.proxysg

import com.indeni.server.rules.library.{ConditionalRemediationSteps, NumericThresholdOnDoubleMetricTemplateRule, ThresholdDirection}
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class BlueCoatSslCertificateCacheRatioRule(context: RuleContext) extends NumericThresholdOnDoubleMetricTemplateRule(context,
  ruleName = "BlueCoatSslCertificateCacheRatioRule",
  ruleFriendlyName = "Blue Coat Devices: SSL certificate cache ratio is too high",
  ruleDescription = "Indeni will alert when SSL certificate emulation usage is too high",
  severity = AlertSeverity.ERROR,
  metricName = "bluecoat-certificate-cache-ratio",
  threshold = 20.0,
  thresholdDirection = ThresholdDirection.ABOVE,
  alertDescriptionFormat = "The SSL certificate cache ratio has reached %.0f%%. Please take action to avoid client connection problems.",
  baseRemediationText = """Indeni monitors the total emulated certificates percentage , lower values means a higher SSL CPU usage.
                           |1. Login to the device's web interface and click on "Statistics" -> "Advanced" -> "SSL" -> "Show SSL Statistics". Review the "Certificate Emulation" section.
                           |2. Try to increase the certificate cache timeout:
                           |Login to the ProxySG via SSH:
                           |proxy>enable
                           |proxy#conf t
                           |proxy#(config)ssl
                           |proxy#(config ssl)proxy set-cert-cache-timeout 72
                           |3. Check if there is a high number of requests:
                           |Login via https to the ProxySG and go to Statistics > Sessions > Active Sessions , see if a single user is trying to make a large number of connections to the same destination.
                           |4. For more information review the following Bluecoat guides: https://support.symantec.com/en_US/article.TECH245157.html
                           |5. If the problem persists, contact Symantec Technical support at https://support.symantec.com for further assistance."""".stripMargin)()




package com.indeni.server.rules.library.templatebased.bluecoat.proxysg

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class BlueCoatCPUMonitorRule(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "BlueCoatCPUMonitorRule",
  ruleFriendlyName = "Blue Coat Devices: CPU monitoring enabled",
  ruleDescription = "CPU monitor provides very useful data when troubleshooting a ProxySG that is experiencing a high CPU related issue. The CPU monitor should only be enabled when troubleshooting a high CPU issue as it can increase CPU utilization by 2-4% depending on the platform. Indeni will alert if CPU monitoring is enabled.",
  severity = AlertSeverity.WARN,
  metricName = "cpu-monitor-enabled",
  alertDescription = "CPU monitoring is enabled",
  baseRemediationText = """The CPU monitor should be used for tubleshooting a ProxySG that is experiencing a high CPU related issue.
                          |(Can be done using the live config or using the "show cpu-monitor" command)
                          |Otherwise it should be disabled because it increases the CPU utilization:
                          |1. Log into the ProxySG via SSH.
                          |2. Enter the following commands:
                          |# en
                          |# configure terminal
                          |# (config) diagnostics
                          |# (config diagnostics) cpu-monitor disable
                          |3. Verify that CPU monitor is disabled by entering the "show cpu-monitor" command.""".stripMargin,
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("cpu-monitor-enabled").asSingle().mostRecent().value().noneable))()

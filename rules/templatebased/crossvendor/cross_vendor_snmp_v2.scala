package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class cross_vendor_snmp_v2(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "cross_vendor_snmp_v2",
  ruleFriendlyName = "All Devices: SNMPv2c/v1 used",
  ruleDescription = "As SNMPv2 is not very secure, Indeni will alert if it is used.",
  severity = AlertSeverity.WARN,
  metricName = "unencrypted-snmp-configured",
  alertDescription = "Older versions of SNMP do not use encryption. This could potentially allow an attacker to obtain valuable information about the infrastructure.",
  baseRemediationText = "Configure SNMPv3 instead.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("unencrypted-snmp-configured").asSingle().mostRecent().value().noneable)
)(ConditionalRemediationSteps.VENDOR_F5 -> "Review https://support.f5.com/csp/article/K13625")

package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}

/**
  *
  */
case class CrossVendorCompareModel(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_compare_model",
  ruleFriendlyName = "Clustered Devices: Model mismatch across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if the model of device in use is different.",
  metricName = "model",
  isArray = false,
  alertDescription = "The members of a cluster of devices must have the same device models in use.",
  baseRemediationText = "Replace one of the devices to match the other.")(
  ConditionalRemediationSteps.VENDOR_JUNIPER ->
    """|1. Run "show version" command to review device model, operating system and software version.
       |2. Each node of SRX must have the same hardware.
       |3. Each node of SRX chassis cluster must be running the same version of Junos.
       |4. Review the following article on Juniper tech support site: <a target="_blank" href=" https://kb.juniper.net/InfoCenter/index?page=content&id=KB15911&actp=METADATA">SRX Getting Started - Troubleshoot High Availability (HA)</a>""".stripMargin
)

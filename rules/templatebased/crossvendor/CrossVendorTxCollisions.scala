package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityWithItemsTemplateRule}

/**
  *
  */
case class CrossVendorTxCollisions(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "CrossVendorTxCollisions",
  ruleFriendlyName = "All Devices: TX packets experienced collisions",
  ruleDescription = "Indeni tracks the number of packets that had issues and alerts if the ratio is too high.",
  usageMetricName = "network-interface-tx-collisions",
  limitMetricName = "network-interface-tx-packets",
  applicableMetricTag = "name",
  threshold = 0.5,
  minimumValueToAlert = 100.0, // We don't want to alert if the number of packets is really low
  alertDescription = "Some network interfaces and ports are experiencing a high collision rate. Review the ports below.",
  alertItemDescriptionFormat = "%.0f collisions identified out of a total of %.0f transmitted.",
  baseRemediationText = "Packet collisions usually occur when there is a mismatch in duplex settings on two sides of a cable.",
  alertItemsHeader = "Affected Ports")(
  ConditionalRemediationSteps.VENDOR_FORTINET ->
    """
      |1. Run "diag hardware deviceinfo nic <interface>" command to display a list of hardware related error names and values. Review  the next link for more details: http://help.fortinet.com/fos50hlp/54/Content/FortiOS/fortigate-toubleshooting-54/troubleshooting_tools.htm
      |2. Run command "fnsysctl cat /proc/net/dev" to get a summary of the interface statistics.
      |3. Check for speed and duplex mismatch in the interface settings on both sides of a cable, and check for a damaged cable. Review the next link for more info: http://kb.fortinet.com/kb/documentLink.do?externalID=10653""".stripMargin 
)

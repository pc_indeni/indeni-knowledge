package com.indeni.server.rules.library.templatebased.crossvendor.compliance

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.MultiSnapshotComplianceCheckTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class crossvendor_compliance_check_users_defined(context: RuleContext) extends MultiSnapshotComplianceCheckTemplateRule(context,
  ruleName = "crossvendor_compliance_check_users_defined",
  ruleFriendlyName = "Compliance Check: Users defined do not match requirement",
  ruleDescription = "Indeni can verify that only certain users are configured on a specific device and that others shouldn't be.",
  severity = AlertSeverity.WARN,
  metricName = "users",
  itemKey = "username",
  alertDescription = "The list of users defined on this device does not match the requirement. Please review the list below.",
  baseRemediationText = "Update the configuration of the device to match the requirement.",
  requiredItemsParameterName = "Users (Whitelist)",
  requiredItemsParameterDescription = "Enter the list of users that should be defined, each one on its own line. indeni will alert if there are any users defined which are not in this list."
)()

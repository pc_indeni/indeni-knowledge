package com.indeni.server.rules.library.templatebased.crossvendor.compliance

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SingleSnapshotComplianceCheckTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class crossvendor_compliance_check_hotfix_jumbo_take(context: RuleContext) extends SingleSnapshotComplianceCheckTemplateRule(context,
  ruleName = "crossvendor_compliance_check_hotfix_jumbo_take",
  ruleFriendlyName = "Compliance Check: Jumbo hotfix take does not match requirement",
  ruleDescription = "Indeni can verify that the take of the jumbo hotfix installed is a specific one.",
  severity = AlertSeverity.WARN,
  metricName = "hotfix-jumbo-take",
  baseRemediationText = "Install the correct jumbo hotfix take.",
  parameterName = "Jumbo hotfix take",
  parameterDescription = "The jumbo hotfix take to look for.",
  expectedValue = "1"
)()

package com.indeni.server.rules.library.templatebased.crossvendor.compliance

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.MultiSnapshotComplianceCheckTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class crossvendor_compliance_check_hotfixes_installed(context: RuleContext) extends MultiSnapshotComplianceCheckTemplateRule(context,
  ruleName = "crossvendor_compliance_check_hotfixes_installed",
  ruleFriendlyName = "Compliance Check: Hotfixes installed do not match requirement",
  ruleDescription = "Indeni can verify that only certain hotfixes are installed on a specific device and that others shouldn't be.",
  severity = AlertSeverity.WARN,
  metricName = "hotfixes",
  itemKey = "name",
  alertDescription = "The list of hotfixes installed on this device does not match the requirement. Please review the list below.",
  baseRemediationText = "Install the required hotfixes and remove the redundant ones.",
  requiredItemsParameterName = "Hotfixes",
  requiredItemsParameterDescription = "Enter the list of hotfixes that should be installed, each one on its own line. indeni will alert if there are any hotfixes installed which are not in this list."
)()

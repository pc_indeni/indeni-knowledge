package com.indeni.server.rules.library.templatebased.crossvendor.compliance

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.MultiSnapshotComplianceCheckTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class crossvendor_compliance_check_ntp_servers(context: RuleContext) extends MultiSnapshotComplianceCheckTemplateRule(context,
  ruleName = "crossvendor_compliance_check_ntp_servers",
  ruleFriendlyName = "Compliance Check: NTP servers configured do not match requirement",
  ruleDescription = "Indeni can verify that certain NTP servers are configured on a specific device.",
  severity = AlertSeverity.WARN,
  metricName = "ntp-servers",
  itemKey = "ipaddress",
  alertDescription = "The list of NTP servers configured on this device does not match the requirement. Please review the list below.",
  baseRemediationText = "Update the configuration of the device to match the requirement.",
  requiredItemsParameterName = "NTP Servers",
  requiredItemsParameterDescription = "Enter the NTP servers required, each one on its own line."
)()

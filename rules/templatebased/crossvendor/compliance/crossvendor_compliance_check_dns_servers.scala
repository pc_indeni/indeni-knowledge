package com.indeni.server.rules.library.templatebased.crossvendor.compliance

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.MultiSnapshotComplianceCheckTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class crossvendor_compliance_check_dns_servers(context: RuleContext) extends MultiSnapshotComplianceCheckTemplateRule(context,
  ruleName = "crossvendor_compliance_check_dns_servers",
  ruleFriendlyName = "Compliance Check: DNS servers configured do not match requirement",
  ruleDescription = "Indeni can verify that certain DNS servers are configured on a specific device.",
  severity = AlertSeverity.WARN,
  metricName = "dns-servers",
  itemKey = "ipaddress",
  alertDescription = "The list of DNS servers configured on this device does not match the requirement. Please review the list below.",
  baseRemediationText = "Update the configuration of the device to match the requirement.",
  requiredItemsParameterName = "DNS Servers",
  requiredItemsParameterDescription = "Enter the DNS servers required, each one on its own line."
)()

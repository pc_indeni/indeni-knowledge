package com.indeni.server.rules.library.templatebased.crossvendor.compliance

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SingleSnapshotComplianceCheckTemplateRule}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class crossvendor_compliance_check_core_dump_enabled(context: RuleContext) extends SingleSnapshotComplianceCheckTemplateRule(context,
  ruleName = "crossvendor_compliance_check_core_dump_enabled",
  ruleFriendlyName = "Compliance Check: Coredumping enablement does not match requirement",
  ruleDescription = "Indeni can verify that coredumping is enabled, or not, on a specific device.",
  severity = AlertSeverity.WARN,
  metricName = "coredumping-enabled",
  baseRemediationText = "Refer to the vendor's documentation on how to enable/disable core dumping.",
  parameterName = "Should Core Dumping Be Enabled",
  parameterDescription = "If this is set to on or ticked, indeni will alert when a device that supports core dumping does not have core dumping enabled.",
  expectedValue = true
)(ConditionalRemediationSteps.VENDOR_CP -> "Follow SK53363.",
  ConditionalRemediationSteps.VENDOR_CISCO -> "Use the \"coredump enable\" command.")

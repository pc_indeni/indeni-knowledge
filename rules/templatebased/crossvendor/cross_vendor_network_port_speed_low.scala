package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class cross_vendor_network_port_speed_low(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "cross_vendor_network_port_speed_low",
  ruleFriendlyName = "All Devices: Network port(s) running in low speed",
  ruleDescription = "Indeni will alert one or more network ports is running in a speed lower than 1000Mbps.",
  metricName = "network-interface-speed",
  applicableMetricTag = "alert-item-port-speed",
  alertItemsHeader = "Ports Affected",
  alertDescription = "One or more ports are set to a speed lower than 1000Mbps. This is usually an error. Review the list of ports below.",
  baseRemediationText = "Many times ports are in a low speed due to an autonegotation error or a misconfiguration.",
  complexCondition = RuleOr(
    RuleEquals(SnapshotExpression("network-interface-speed").asSingle().mostRecent().value().noneable, RuleHelper.createComplexStringConstantExpression("10M")),
    RuleEquals(SnapshotExpression("network-interface-speed").asSingle().mostRecent().value().noneable, RuleHelper.createComplexStringConstantExpression("100M"))
  ),
  alertItemHeadlineExpersion =
    new Expression[String] {
      val networkInterfaceSpeedExpersion = SnapshotExpression("network-interface-speed").asSingle().mostRecent()
      override def eval(time: Long): String = networkInterfaceSpeedExpersion.eval(time).value.getOrElse("value", "")
      override def args: Set[Expression[_]] = Set(networkInterfaceSpeedExpersion)
    },
  headlineFormat = "%s set has a %s"
  )(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Use the "show interface" NX-OS command to display speed and duplex settings of an interface.
      |2. Use the "show interface status" and "show interface capabilities" NX-OS commands to gather more information about ports.
      |3. You can disable link negotiation using the "no negotiate auto" command. Use the "negotiate auto" command to enable auto negotiation on 1-Gigabit ports when the connected peer does not support auto negotiation. By default, auto-negotiation is enabled on 1-Gigabit ports and disabled on 10-Gigabit ports.
      |4. Cisco does not recommend to enable auto negotiation on 10-Gigabit ports. Enabling auto-negotiation on 10-Gigabit ports brings the link down. By default, link negotiation is disabled on 10-Gigabit ports.
      |NOTE: A shut and no shut to the interface may be required after the aforementioned configuration change.""".stripMargin
)

package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityWithItemsTemplateRule}

/**
  *
  */
case class CrossVendorRxError(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "cross_vendor_rx_error",
  ruleFriendlyName = "All Devices: RX packets experienced errors",
  ruleDescription = "Indeni tracks the number of packets that had issues and alerts if the ratio is too high.",
  usageMetricName = "network-interface-rx-errors",
  limitMetricName = "network-interface-rx-packets",
  applicableMetricTag = "name",
  threshold = 0.5,
  minimumValueToAlert = 100.0, // We don't want to alert if the number of error packets is really low
  alertDescription = "Some network interfaces and ports are experiencing a high error rate. Review the ports below.",
  alertItemDescriptionFormat = "%.0f error packets identified out of a total of %.0f received.",
  baseRemediationText = "Packet errors usually occur when there is a mismatch in the speed and duplex settings on two sides of a cable, or a damaged cable.",
  alertItemsHeader = "Affected Ports")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
       |1. Run the "show interface" command to review the interface error counters and the bitrate. Consider to configure the "load-interval 30" interface sub command to improve the accuracy of the interface measurements.
       |2. Check for a mismatch in the speed and duplex interface settings on two sides of a cable, or for a damaged cable.
       |3. Use the "show interface counters errors" NX-OS command to display detailed interface error counters. If you do not specify an interface, this command displays information about all Layer 2 interfaces.""".stripMargin,
  ConditionalRemediationSteps.VENDOR_JUNIPER ->
    """|1. Run the "show interface extensive" command to review the interface error counters. 
       |2. Check for a mismatch in the speed and duplex interface settings on the both sides.
       |3. Check the physical cable. It might be damaged or incorrect type is used.
       |4. Review the following article on Juniper tech support site: <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/reference/command-summary/show-interfaces-security.html#jd0e1772">Operational Commands</a>
       |5. If the problem persists, contact the Juniper Networks Technical Assistance Center (JTAC)""",
  ConditionalRemediationSteps.VENDOR_FORTINET ->
    """
       |1. Run "diag hardware deviceinfo nic <interface>" command to display a list of hardware related error names and values. Review  the next link for more details: http://help.fortinet.com/fos50hlp/54/Content/FortiOS/fortigate-toubleshooting-54/troubleshooting_tools.htm
       |2. Run command "fnsysctl cat /proc/net/dev" to get a summary of the interface statistics.
       |3. Check for speed and duplex mismatch in the interface settings on both sides of a cable, and check for a damaged cable. Review the next link for more info: http://kb.fortinet.com/kb/documentLink.do?externalID=10653""".stripMargin
)

package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}
import com.indeni.server.common.data.conditions.{Equals => DataEquals}

/**
  *
  */
case class CrossVendorClusterInterfaceMtuVsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "CrossVendorClusterInterfaceMtuVsx",
  ruleFriendlyName = "Clustered Devices: Network interface mtu does not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if their network interface mtu are different.",
  metricName = "network-interface-mtu",
  applicableMetricTag = "name",
  descriptionMetricTag = "vs.name",
  metaCondition = DataEquals("vsx", "true"),
  isArray = false,
  alertDescription = "Devices that are part of a cluster must have the same network interface mtu setting. Review the differences below.",
  baseRemediationText = "Ensure the network interface mtu setting matches across devices in a cluster.")()

case class CrossVendorClusterInterfaceMtuNonVsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "CrossVendorClusterInterfaceMtuNonVsx",
  ruleFriendlyName = "Clustered Devices: Network interface mtu does not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if their network interface mtu are different.",
  metricName = "network-interface-mtu",
  applicableMetricTag = "name",
  metaCondition = !DataEquals("vsx", "true"),
  isArray = false,
  alertDescription = "Devices that are part of a cluster must have the same network interface mtu setting. Review the differences below.",
  baseRemediationText = "Ensure the network interface mtu setting matches across devices in a cluster.")()

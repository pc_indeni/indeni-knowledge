package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class CrossVendorPowerSupply(context: RuleContext) extends StateDownTemplateRule(context,
  severity = AlertSeverity.WARN,
  ruleName = "CrossVendorPowerSupply",
  ruleFriendlyName = "Devices with multiple PSU: Some Power Supply Unit (PSU) slot is empty or PSU is down",
  ruleDescription = "Indeni will alert if some PSU slot is empty.",
  metricName = "power-supply-inserted",
  applicableMetricTag = "name",
  alertItemsHeader = "The following PSU(s) not installed or off line",
  alertDescription = "It is best practice to install multiple PSU with separate power sources whenever possible. Indeni will alert if a device has multiple PSU slots but some slot has no PSU installed or the PSU is off line.",
  baseRemediationText = "No redundant power supply has been detected. It is best practice to run dual power supplies on two separate Power Distribution Units, separate UPS backup, and circuit breakers. This provides the greatest level of redundancy for power access.")(
  ConditionalRemediationSteps.VENDOR_PANOS -> "Run the CLI command \"show system environmentals power-supply\" for more details."
)

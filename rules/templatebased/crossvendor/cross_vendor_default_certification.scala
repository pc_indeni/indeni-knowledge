package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class cross_vendor_default_certification(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "cross_vendor_default_certification",
  ruleFriendlyName = "All Devices: Default certificate used",
  ruleDescription = "Many devices are pre-installed with a default SSL certificate. Generally, it's good practice to replace these to ensure security when accessing these devices. indeni will alert of a default certificate it used.",
  metricName = "default-management-certificate-used",
  alertDescription = "Using the default management certificate could enable a potential attacker to perform a man-in-the-middle attack without administrators knowing it. Therefore it is always recommended to use a certificate signed by a Certificate Authority that you trust. This indeni alert checks if the default management certificate is used and alerts if it is.",
  baseRemediationText = "Install a non-default certificate.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("default-management-certificate-used").asSingle().mostRecent().value().noneable)
)(ConditionalRemediationSteps.VENDOR_F5 -> "Review https://support.f5.com/csp/article/K15664")

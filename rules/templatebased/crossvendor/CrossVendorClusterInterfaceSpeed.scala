package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}
import com.indeni.server.common.data.conditions.{Equals => DataEquals}

/**
  *
  */
case class CrossVendorClusterInterfaceSpeedVsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "CrossVendorClusterInterfaceSpeedVsx",
  ruleFriendlyName = "Clustered Devices: Network interface speed does not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if their network interface speed are different.",
  metricName = "network-interface-speed",
  applicableMetricTag = "name",
  descriptionMetricTag = "vs.name",
  metaCondition = DataEquals("vsx", "true"),
  isArray = false,
  alertDescription = "Devices that are part of a cluster must have the same network interface speed setting. Review the differences below.",
  baseRemediationText = "Ensure the network interface speed setting matches across devices in a cluster.")()

case class CrossVendorClusterInterfaceSpeedNonVsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "CrossVendorClusterInterfaceSpeedNonVsx",
  ruleFriendlyName = "Clustered Devices: Network interface speed does not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if their network interface speed are different.",
  metricName = "network-interface-speed",
  applicableMetricTag = "name",
  metaCondition = !DataEquals("vsx", "true"),
  isArray = false,
  alertDescription = "Devices that are part of a cluster must have the same network interface speed setting. Review the differences below.",
  baseRemediationText = "Ensure the network interface speed setting matches across devices in a cluster.")()

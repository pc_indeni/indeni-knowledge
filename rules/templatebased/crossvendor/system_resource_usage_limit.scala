package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityWithItemsTemplateRule}

/**
  *
  */
case class system_resource_usage_limit(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "system_resource_usage_limit",
  ruleFriendlyName = "All Devices: System resource usage high",
  ruleDescription = "Some devices have a number of different resource limitations (such as maximum number of interfaces, routes, etc.). Indeni will track the actual usage of these resources and alert if the limit is nearing or reached.",
  usageMetricName = "system-resource-usage",
  limitMetricName = "system-resource-limit",
  applicableMetricTag = "name",
  threshold = 80.0,
  alertDescription = "Some system resources are nearing their limit. Review the list below.",
  alertItemDescriptionFormat = "The number of items in use is %.0f where the limit is %.0f.",
  baseRemediationText = "Depending on the system resource nearing capacity, review any configuration changes required.",
  alertItemsHeader = "Affected Resources")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Execute the "show resource" command to review the resource utilization of the switch.
      |2. Consider of upgrade in case that the resources are close to the upper limits.
    """.stripMargin
)

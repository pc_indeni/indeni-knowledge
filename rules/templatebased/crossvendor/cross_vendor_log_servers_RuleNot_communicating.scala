package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.conditions.EndsWithRepetition
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}
import com.indeni.apidata.time.TimeSpan

/**
  *
  */
case class cross_vendor_log_servers_not_communicating(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_log_servers_not_communicating",
  historyLength = 2,
  ruleFriendlyName = "All Devices: Communication issues with certain log servers",
  ruleDescription = "indeni will alert if any of the log servers a device is set to send logs to is not communicating.",
  metricName = "log-server-communicating",
  applicableMetricTag = "name",
  alertItemsHeader = "Log Servers Affected",
  alertDescription = "One or more logging servers are not communicating.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://www.linkedin.com/pub/roop-sukhavasi/3/96/b8b\">Roop Sukhavasi</a> (NYSE).",
  baseRemediationText = "Review the possible cause for this.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Read https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk40090"
)

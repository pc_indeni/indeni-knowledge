package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.conditions.EndsWithRepetition
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}
import com.indeni.apidata.time.TimeSpan

/**

case class cross_vendor_config_unsaved(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_config_unsaved",
  ruleFriendlyName = "All Devices: Configuration changed but not saved",
  ruleDescription = "Indeni will alert if the configuration was changed on a device, but not saved.",
  metricName = "config-unsaved",
  alertIfDown = false,
  alertDescription = "The configuration has been changed on this device, but has not yet been saved. This may result in the loss of the new configuration during a power cycle or device reboot.",
  historyLength = 2,
  baseRemediationText = "Log into the device and save the configuration.")(
  ConditionalRemediationSteps.VENDOR_CP -> "In clish, run \"save configuration\".",
  ConditionalRemediationSteps.VENDOR_CISCO -> "For IOS, use \"write\", for NX-OS use \"copy running-config startup-config\".",
  ConditionalRemediationSteps.VENDOR_PANOS -> "Log into the device's web interface and click on the Commit button.",
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Check that there are not unsaved configuration changes by running the "show running-config diff" NX-OS command
      |2. Log into the device and save the configuration with the "copy running-config startup-config" NX-OS command
      |3. Consider creating snapshots of the configuration by utilizing the Checkpoint and Rollback NX-OS features. The NX-OS checkpoint and rollback feature are extremely useful, and a life saver in some cases, when a new configuration change to a production system has caused unwanted effects or was incorrectly made/planned and we need to immediately return to an original/stable configuration.
      |4.  For more information about checkpoint and rollback NX-OS features please review  the following article:
      |http://www.firewall.cx/cisco-technical-knowledgebase/cisco-data-center/1202-cisco-nexus-checkpoint-rollback-feature.html""".stripMargin
)
  
  */

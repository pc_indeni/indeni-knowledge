package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.conditions._
import com.indeni.ruleengine.expressions.data._
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{MultiSnapshotValueCheckTemplateRule, RuleHelper, ConditionalRemediationSteps}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  * Created by tomas on 20170721.
  */

case class CrossVendorSnmpCommunitiesPublic(context: RuleContext) extends MultiSnapshotValueCheckTemplateRule(context,
  ruleName = "cross_vendor_snmp_communities_public",
  ruleFriendlyName = "All Devices: SNMP configured with community public",
  severity = AlertSeverity.INFO,
  ruleDescription = "Indeni will alert if any of SNMP communities is set to \"public\".",
  metricName = "snmp-communities",
  alertDescription = "Using a well known SNMP community means that it is easy for others to guess, and to poll the device. An attacker could use this to get information from the device.",
  baseRemediationText = "If SNMPv2 has to be used, use a random community that is hard to guess. If possible switch to SNMPv3 instead, which uses username and password instead of a single community string.",
  complexCondition = Contains(MultiSnapshotExtractScalarExpression(SnapshotExpression("snmp-communities").asMulti().mostRecent().value(), "community"), ConstantExpression("public"))
)(ConditionalRemediationSteps.VENDOR_JUNIPER ->
  """|1. On the device command line interface execute “show configuration snmp” and “show snmp statistics” commands to review  SNMP configuration and statistics.
     |2. For security reasons it is highly recommended to use SNMP version 3.
     |3. Using the community string  "public" is discouraged, as this is  a common default setting and presents a security vulnerability.
     |4. Ensure that all of the SNMP settings are configured correctly on all cluster members.
     |5. Review the following article on Juniper tech support site: <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/task/configuration/snmp-best-practices-basic-config.html">Configuring SNMP on Devices Running Junos OS</a>.""".stripMargin
)

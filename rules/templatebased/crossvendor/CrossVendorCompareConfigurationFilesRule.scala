package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class CrossVendorCompareConfigurationFilesRule(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "CrossVendorCompareConfigurationFilesRule",
  ruleFriendlyName = "Clustered Devices: Critical configuration files mismatch across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if critical configuration files are different.",
  metricName = "lines-config-files",
  isArray = true,
  alertDescription = "Devices that are part of a cluster must have the same settings in their critical configuration files. Review the differences below.",
  baseRemediationText = "Correct any differences found to ensure a complete match between device members.",
  alertItemsHeader = "Mismatching File Paths and Values")()
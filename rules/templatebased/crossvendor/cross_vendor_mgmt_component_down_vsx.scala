package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.conditions.Contains
import com.indeni.ruleengine.utility.LastNNonEmptyValues
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}

/**
  *
  */
case class cross_vendor_mgmt_component_down_vsx(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_mgmt_component_down_vsx",
  ruleFriendlyName = "Management Devices: Management service down (Virtual)",
  ruleDescription = "Alert if a management component is down on a device.",
  metricName = "mgmt-status",
  historyLength = 3,
  generateStateDownCondition = (historyLength, tsToTestAgainst, stateToLookFor) =>
    Contains(LastNNonEmptyValues(tsToTestAgainst, historyLength), stateToLookFor),
  applicableMetricTag = "vs.name",
  alertItemsHeader = "Management Systems Affected",
  alertDescription = "One or more management components on this device are down.",
  baseRemediationText = "This may be due to someone stopping the management component itself, a licensing or a performance issue.")(
  ConditionalRemediationSteps.VENDOR_CP -> "The management service is handled by the \"fwm\" process (for each domain/CMA). Run \"mdsstat\" for more details. Review the licenses installed on the device, as well as whether or not anyone has run cpstop recently."
)


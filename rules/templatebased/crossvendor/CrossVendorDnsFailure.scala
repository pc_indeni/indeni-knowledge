package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.conditions.EndsWithRepetition
import com.indeni.ruleengine.expressions.core._
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CrossVendorDnsFailure(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "CrossVendorDnsFailure",
  ruleFriendlyName = "All Devices: DNS lookup failure(s)",
  ruleDescription = "Indeni will alert if the DNS resolution is not working on the device.",
  metricName = "dns-server-state",
  applicableMetricTag = "name",
  alertItemsHeader = "DNS Servers Affected",
  alertDescription = "One or more DNS servers configured on this device are not responding or are failing to resolve www.indeni.com.",
  baseRemediationText = "Review the cause for the DNS resolution not working.",
  historyLength = 2
)(ConditionalRemediationSteps.VENDOR_JUNIPER ->
   """|1. On the device command line interface execute the "show system name-server"  command to review the DNS configuration.
      |2. Run the "show host host-name [host-ip-address]" command to check if DNS is working properly and is reachable.
      |3. Ensure that the UDP port 53 is allowed in the firewall rules.
      |4. Check the routes to DNS server address.
      |5. Review the following article on Juniper tech support site: <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/task/configuration/dns-name-server-configuring.html">Reaching a Domain Name System Server</a>.""".stripMargin,
  ConditionalRemediationSteps.VENDOR_FORTINET ->
    """
      |1. Login via https to the Fortinet firewall and go to the menu Network> DNS to review the DNS configuration.
      |2. Login via ssh to the Fortinet firewall and review the system dns configuration.
      |3. Verify your DNS server IPs and routing. Ensure that your firewalls or routers do not block or proxy UDP port 53. 
      |4. To verify your DNS service enter the following commands in the CLI: "execute traceroute <server_fqdn>"  where <server_fqdn> is a domain name such as www.example.com. If the DNS query fails,  an error message is received such as: traceroute: unknown host www.example.com
      |5. Login via ssh to the Fortinet firewall and troubleshoot the problem by using the "diag test application dnsproxy <X>" where <X> can be 1. Clear dns cache 2. Show stats 3. Dump DNS setting 4. Reload FQDN 5. Requery FQDN 6. Dump FQDN.
      |6. For more information review the next link: https://docs.fortinet.com/uploaded/files/2924/troubleshooting-54.pdf""".stripMargin,
  ConditionalRemediationSteps.VENDOR_BLUECOAT ->
    """
      |1. Login via HTTPS to the Bluecoat ProxySG and go to the menu Configuration > Network > DNS to review the DNS configuration.
      |2. Login via SSH to the Bluecoat ProxySG and review the system dns configuration by typing the "show dns" command.
      |3. Verify your DNS server IPs and routing. Ensure that your firewalls or routers do not block UDP port 53.
      |4. To verify your DNS service enter the following commands in the CLI: "test dns <server_fqdn>" where <server_fqdn> is a domain name such as www.example.com. If the DNS query fails, an error message is received such as: "Error encountered. Response error code: Name error".
      |5. Login via HTTPS to the Bluecoat ProxySG and go to the menu Statistics tab > Advanced > DNS > Show list of DNS URLs. Here is where you can see DNS entries or delete them.
      |6. Login via ssh to the Bluecoat ProxySG via SSH and clear the DNS cache by typing the "clear-cache dns-cache" command.
      |7. For more information review the next link: <a target="_blank" https://support.symantec.com/en_US/article.TECH240878.html</a>""".stripMargin
)

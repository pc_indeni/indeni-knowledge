package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class cross_vendor_dns_servers_comparison(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_dns_servers_comparison",
  ruleFriendlyName = "Clustered Devices: DNS servers used do not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if the DNS servers they are using are different.",
  severity = AlertSeverity.WARN,
  metricName = "dns-servers",
  isArray = true,
  alertDescription = "Devices that are part of a cluster must have the same DNS servers used. Review the differences below.",
  baseRemediationText = "Review the DNS configuration on each device to ensure they match.")()

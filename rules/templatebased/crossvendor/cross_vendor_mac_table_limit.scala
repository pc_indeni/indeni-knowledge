package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityTemplateRule

/**
  *
  */
case class cross_vendor_mac_table_limit(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "cross_vendor_mac_table_limit",
  ruleFriendlyName = "All Devices: MAC cache usage high",
  ruleDescription = "indeni will alert when the number of MAC entries stored by a device is nearing the allowed limit.",
  usageMetricName = "mac-total-entries",
  limitMetricName = "mac-limit",
  threshold = 80.0,
  alertDescriptionFormat = "The MAC table has %.0f entries where the limit is %.0f.",
  baseRemediationText = "Identify the cause of the large MAC table. If it is due to a legitimate cause, such as a high number of hosts visible on the available networks, please contact your technical support provider.")()

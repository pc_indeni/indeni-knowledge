package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class cross_vendor_bond_slave_down(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_bond_slave_down",
  ruleFriendlyName = "All Devices: Bond/LACP slave interface down",
  ruleDescription = "indeni will alert if a bond interface's slave interface is down.",
  metricName = "bond-slave-state",
  applicableMetricTag = "name",
  descriptionMetricTag = "bond-name",
  alertItemsHeader = "Interfaces Affected",
  descriptionStringFormat = "Part of ${scope(\"bond-name\")}",
  alertDescription = "One or more bond slave interfaces are down.",
  baseRemediationText = "Review the cause for the interfaces being down.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Use the \"cphaconf show_bond\" command to get additional information.",
  ConditionalRemediationSteps.VENDOR_PANOS -> "Use the \"show lacp\" command to get additional information."
)

package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}
import com.indeni.server.common.data.conditions.{Equals => DataEquals}

/**
  *
  */
case class cross_vendor_features_enabled_comparison_vsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_features_enabled_comparison_vsx",
  ruleFriendlyName = "Clustered Devices: Features enabled do not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if the features they have enabled are different.",
  metricName = "features-enabled",
  applicableMetricTag = "vs.name",
  metaCondition = DataEquals("vsx", "true"),
  isArray = true,
  alertDescription = "Devices that are part of a cluster must have the same features enabled. Review the differences below.",
  baseRemediationText = "Review the licensing and enabled features or modules on each device to ensure they match.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Execute the "show feature" and "show license-usage" NX-OS commands to review the enabled features and licenses per vPC peer switch.
      |2. Both vPC peer switches should have the same licenses installed and features activated.
      |3. For more information please review  the following CISCO  NX-OS guides:
      |https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/best_practices/cli_mgmt_guide/cli_mgmt_bp/features.html
      |https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/nx-os/licensing/guide/b_Cisco_NX-OS_Licensing_Guide/b_Cisco_NX-OS_Licensing_Guide_chapter_01.html""".stripMargin
)


case class cross_vendor_features_enabled_comparison_non_vsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_features_enabled_comparison_non_vsx",
  ruleFriendlyName = "Clustered Devices: Features enabled do not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if the features they have enabled are different.",
  metricName = "features-enabled",
  metaCondition = !DataEquals("vsx", "true"),
  isArray = true,
  alertDescription = "Devices that are part of a cluster must have the same features enabled. Review the differences below.",
  baseRemediationText = "Review the licensing and enabled features or modules on each device to ensure they match.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
       |1. Execute the "show feature" and "show license-usage" NX-OS commands to review the enabled features and licenses per vPC peer switch.
       |2. Both vPC peer switches should have the same licenses installed and features activated.
       |3. For more information please review  the following CISCO  NX-OS guides:
       |https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/best_practices/cli_mgmt_guide/cli_mgmt_bp/features.html
       |https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/nx-os/licensing/guide/b_Cisco_NX-OS_Licensing_Guide/b_Cisco_NX-OS_Licensing_Guide_chapter_01.html""".stripMargin
)

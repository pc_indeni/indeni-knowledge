package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.conditions.EndsWithRepetition
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}
import com.indeni.apidata.time.TimeSpan

/**
  *
  */
case class CrossVendorOspfNeighborIsDownRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_ospf_neighbor_down",
  ruleFriendlyName = "All Devices: OSPF neighbor(s) down",
  ruleDescription = "Indeni will alert if one or more OSPF neighbors isn't communicating well.",
  metricName = "ospf-state",
  applicableMetricTag = "name",
  alertItemsHeader = "OSPF Neighbors Affected",
  alertDescription = "One or more OSPF neighbors are down.",
  baseRemediationText = "Review the cause for the neighbors being down.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Review sk84520: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk84520",
  ConditionalRemediationSteps.OS_NXOS ->
          """This issue might be caused by:
            | a. L2/L3 connectivity issue
            | b. OSPF not being enabled on the interface
            | c. an interface is defined as passive
            | d. a mismatched subnet mask
            | e. a mismatched hello/dead interval
            | f. a mismatched authentication key
            | g. a mismatched area ID
            | i. a mismatched transit/stub/Not-So-Stubby Area (NSSA) option
            |Check OSPF configuration:
            |Use these commands in order to check the OSPF configuration (subnet, hello/dead interval, area ID, area type, authentication key (if any), and not-passive), and ensure that it matches on both sides.
            | a. "show run ospf"
            | b. "show ip ospf PID interface"
            | c. "show ip ospf PID"
            |
            |Troubleshooting OSPF States:
            |1. OSPF Neighbor Stuck in the Initialization (INIT) State. This issue might be caused by:
            | a. one side is blocking the hello packet with ACL
            | b. one side is translating, with Network Address Translation (NAT), the OSPF hello
            | c. the multicast capability of one side is broken (L2)
            |
            |2. OSPF Neighbor Stuck in a Two-Way State. This issue might be caused by OSPF priority set equal to zero.
            |NOTE: It is normal in broadcast network types.
            |
            |3. OSPF Neighbor Stuck in Exstart/Exchange. This issue might be caused by:
            | a. MTU mismatch
            | b. Neighbor Router ID (RID) is the same as its neighbor's
            | c. ACL blocking unicast - after two-way OSPF sends unicast packet
            |
            |4. OSPF Neighbor Stuck in a Loading State. This issue might be caused by bad Link State Advertisement (LSA). Check the OSPF-4-BADLSATYPE message with the "show ip ospf bad" and the "show log" commands.
            |
            |Further details can be found in: <a target="_blank" href="https://www.cisco.com/c/en/us/support/docs/switches/nexus-7000-series-switches/116422-technote-ospf-00.html"> CISCO Nexus OSPF troubleshooting guide|</a>.""".stripMargin,
  ConditionalRemediationSteps.VENDOR_JUNIPER ->
          """|1. Run the "show ospf neighbor" command to view the current state of OSPF neighbors.
             |2. Run the "show configuration protocol ospf" command to view OSPF configuration.
             |3. If no OSPF neighbor is shown, please check the following items:
             |  a. physical and data link connectivity
             |  b. mismatched IP subnet
             |  c. subnet mask
             |  d. area number
             |  e. area type
             |  f. authentication
             |  g. hello and dead interval
             |  i. network type
             |4. It's normal for DR-other neighbors to be stuck in two-way state.
             |5. If the OSPF neighbor is stuck in Exstart/Exchange State, check mismatched IP MTU.
             |6. Consider configuring traceoptions to track the OSPF operations.
             |7. Review the following articles on Juniper TechLibrary for more information:
             | a. <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/topic-map/ospf-traceoptions.html">Example: Configuring OSPF Trace Options</a>
             | b. <a target="_blank" href="https://kb.juniper.net/InfoCenter/index?page=content&id=KB14881">OSPF is in '2 Way' state with some neighbors and "Full" with others</a>
             |8. If the problem persists, contact the Juniper Networks Technical Assistance Center (JTAC).""".stripMargin
)

package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityWithItemsTemplateRule}


case class CrossVendorPortTxBwUtilHigh(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "cross_vendor_port_tx_bandwidth_utilization_high",
  ruleFriendlyName = "All Devices: TX traffic load close to port capacity",
  ruleDescription = "Indeni will alert if the transmit load is close to the port capacity.",
  usageMetricName = "network-interface-tx-rate-mbps",
  limitMetricName = "network-interface-bandwidth-mbps",
  applicableMetricTag = "name",
  threshold = 90,
  alertDescription = "The following ports are transmitting high traffic load.",
  alertItemsHeader = "Affected Ports",
  alertItemDescriptionFormat = "%.0f Mbps transmit load detected out of a total capacity of %.0f Mbps.",
  baseRemediationText = "High interface utilization usually occurs when the network is experiencing heavy traffic load. Check if the heavy traffic load is expected.")() 

package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}
import com.indeni.server.common.data.conditions.{Equals => DataEquals, Not => DataNot}


/**
  *
  */
case class cross_vendor_compare_policy_fingerprint(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_compare_policy_fingerprint",
  ruleFriendlyName = "Clustered Devices: Policy mismatch across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the policy installed is different.",
  metricName = "policy-installed-fingerprint",
  isArray = false,
  alertDescription = "The members of a cluster of devices must have the same policy installed.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/itzik-assaraf/2/870/1b5\">Itzik Assaraf</a> (Leumi Card).",
  baseRemediationText = """Review the policy installed on each device in the cluster and ensure they are the same.""",
  metaCondition = !DataEquals("vsx", "true"))(
  ConditionalRemediationSteps.VENDOR_CP -> "Normally the management server ensures the same policy was installed on all cluster members. It's possible the checkbox for ensuring this was unchecked in the most recent policy installation. Please re-install the policy.")

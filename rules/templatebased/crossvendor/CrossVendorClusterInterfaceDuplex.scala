package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}
import com.indeni.server.common.data.conditions.{Equals => DataEquals}

/**
  *
  */
case class CrossVendorClusterInterfaceDuplexVsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "CrossVendorClusterInterfaceDuplexVsx",
  ruleFriendlyName = "Clustered Devices: Network interface duplex does not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if their network interface duplex are different.",
  metricName = "network-interface-duplex",
  applicableMetricTag = "name",
  descriptionMetricTag = "vs.name",
  metaCondition = DataEquals("vsx", "true"),
  isArray = false,
  alertDescription = "Devices that are part of a cluster must have the same network interface duplex setting. Review the differences below.",
  baseRemediationText = "Ensure the network interface duplex setting matches across devices in a cluster.")()


case class CrossVendorClusterInterfaceDuplexNonVsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "CrossVendorClusterInterfaceDuplexNonVsx",
  ruleFriendlyName = "Clustered Devices: Network interface duplex does not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if their network interface duplex are different.",
  metricName = "network-interface-duplex",
  applicableMetricTag = "name",
  metaCondition = !DataEquals("vsx", "true"),
  isArray = false,
  alertDescription = "Devices that are part of a cluster must have the same network interface duplex setting. Review the differences below.",
  baseRemediationText = "Ensure the network interface duplex setting matches across devices in a cluster.")()

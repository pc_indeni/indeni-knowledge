package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class cross_vendor_no_ntp_servers(context: RuleContext) extends MultiSnapshotValueCheckTemplateRule(context,
  ruleName = "cross_vendor_no_ntp_servers",
  ruleFriendlyName = "All Devices: No NTP servers configured",
  ruleDescription = "Many odd and complicated outages occur due to lack of clock synchronization between devices. In addition, logs may have the wrong time stamps. Indeni will alert when a device has no NTP servers configured.",
  severity = AlertSeverity.WARN,
  metricName = "ntp-servers",
  alertDescription = "This system does not have an NTP server configured. Many odd and complicated outages occur due to lack of clock synchronization between devices. In addition, logs may have the wrong time stamps.",
  baseRemediationText = "Configure one or more NTP servers to be used by this device for clock synchronization.",
  complexCondition = RuleEquals(RuleHelper.createEmptyComplexArrayConstantExpression(), SnapshotExpression("ntp-servers").asMulti().mostRecent().value().noneable))(
  ConditionalRemediationSteps.VENDOR_F5 -> "Log into the Web interface and navigate to System -> Configuration -> Device -> NTP. Add the desired NTP servers and click \"update\".",
  ConditionalRemediationSteps.VENDOR_FORTINET ->
    """
      |1. Login via ssh to the Fortinet firewall and execute the FortiOS “execute time” and “execute date” commands to check the current date/time and the last date of NTP sync.
      |2. Login via ssh to the Fortinet firewall and execute the FortiOS “diagnose sys ntp status” to review the status of the NTP servers and configuration.
      |3. NTP uses UDP protocol (17) and port 123 to communicate between the client and the servers.  Make sure that the firewall rules allow these UDP ports and can route toward the NTP servers.
      |4. Login via ssh to the Fortinet firewall and execute the FortiOS debug commands “diag debug application ntpd -1” and “diag debug enable” and review the debug messages.
      |5. Make sure NTP authentication keys match on both ends. Review the next link for more information: http://kb.fortinet.com/kb/viewContent.do?externalId=FD33783.
      |6. More NTP configuration information can be found at http://help.fortinet.com/cli/fos50hlp/56/Content/FortiOS/fortiOS-cli-ref-56/config/system/ntp.htm.""".stripMargin
)

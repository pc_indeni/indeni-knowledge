package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityWithItemsTemplateRule}

/**
  *
  */
case class license_usage_limit(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "license_usage_limit",
  ruleFriendlyName = "All Devices: License usage limit approaching",
  ruleDescription = "Some licenses are limited to a certain number of elements (such as maximum users). If any of the licenses is nearing its limit, an alert will be issued.",
  usageMetricName = "license-elements-used",
  limitMetricName = "license-elements-limit",
  applicableMetricTag = "name",
  threshold = 80.0,
  minimumValueToAlert = 2.0, // We don't want to alert if the license capacity is 1 and we're using one item, this is a common occurence and isn't an issue
  alertDescription = "Some licenses are nearing their limit. Review the list below.",
  alertItemDescriptionFormat = "The number of elements in use is %.0f where the limit is %.0f.",
  baseRemediationText = "Consider purchasing additional licenses.",
  alertItemsHeader = "Affected Licenses")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Run the "show license usage" NX-OS command to display information about the current license usage and the expire date.
      |2. Run the "show license" NX-OS command to view the installed licenses.
      |3. Run the "show license usage XXX" NX-OS command e.g." sh license usage ENHANCED_LAYER2_PKG" to display information about the activated features which utilize this license.
      |4. Consider activate the grace-period for the license.
      |5. Order new license from the CISCO.
      |6. For more information please review the next Cisco guide:
      |https://www.cisco.com/c/m/en_us/techdoc/dc/reference/cli/nxos/commands/fund/show-license-usage.html
    """.stripMargin
)

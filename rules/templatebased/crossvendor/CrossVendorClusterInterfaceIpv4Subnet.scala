package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}
import com.indeni.server.common.data.conditions.{Equals => DataEquals}

/**
  *
  */
case class CrossVendorClusterInterfaceIpv4SubnetVsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "CrossVendorClusterInterfaceIpv4SubnetVsx",
  ruleFriendlyName = "Clustered Devices: Network interface ipv4 subnet does not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if their network interface ipv4 subnet are different.",
  metricName = "network-interface-ipv4-subnet",
  applicableMetricTag = "name",
  descriptionMetricTag = "vs.name",
  metaCondition = DataEquals("vsx", "true"),
  isArray = false,
  alertDescription = "Devices that are part of a cluster must have the same network interface ipv4 subnet setting. Review the differences below.",
  baseRemediationText = "Ensure the network interface ipv4 subnet setting matches across devices in a cluster.")()


case class CrossVendorClusterInterfaceIpv4SubnetNonVsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "CrossVendorClusterInterfaceIpv4SubnetNonVsx",
  ruleFriendlyName = "Clustered Devices: Network interface ipv4 subnet does not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if their network interface ipv4 subnet are different.",
  metricName = "network-interface-ipv4-subnet",
  applicableMetricTag = "name",
  metaCondition = !DataEquals("vsx", "true"),
  isArray = false,
  alertDescription = "Devices that are part of a cluster must have the same network interface ipv4 subnet setting. Review the differences below.",
  baseRemediationText = "Ensure the network interface ipv4 subnet setting matches across devices in a cluster.")()

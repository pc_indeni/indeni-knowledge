package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class CrossVendorRaidStatusRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "CrossVendorRaidStatusRule",
  ruleFriendlyName = "All Devices: Disk RAID in error state",
  ruleDescription = "Indeni will alert if a disk RAID is in an error state.",
  metricName = "raid-status",
  applicableMetricTag = "raid",
  descriptionMetricTag = "diskid-states",
  alertItemsHeader = "Disk RAID Affected",
  alertDescription = "Indeni will alert if it detects unhealthy disks in a RAID.",
  baseRemediationText = "It is best practice to keep all disks in a RAID set in healthy condition.")(
  ConditionalRemediationSteps.VENDOR_PANOS -> "Please run \"show system raid\" and \"show system raid detail\" for more information. Troubleshooting is unique to each model of device. Please search your specific result along with model number on https://live.paloaltonetworks.com. Open a support ticket with Palo Alto Networks if there is a failed drive."
)

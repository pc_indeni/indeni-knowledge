package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.conditions.{EndsWithRepetition, Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class cross_vendor_identity_integration_down(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_identity_integration_down",
  ruleFriendlyName = "All Devices: Integration with identity/AAA server down",
  ruleDescription = "Some devices may integrate with identity or AAA servers to provide user identification, authentication and authorization services. If the integration is down, such services may be disrupted. indeni will alert if this occurs.",
  metricName = "identity-integration-connection-state",
  applicableMetricTag = "name",
  alertItemsHeader = "Affected Servers",
  alertDescription = "Typically an administrator would not be aware of a disconnected domain controller (or identity/AAA server) until users can no longer reach resources they were previously able to, or they are now able to reach resources that were previously blocked.",
  baseRemediationText = "Make sure that the device can communicate with the identity/AAA server, that the username and password for accessing it are correct and that it has the permissions it needs.",
  historyLength = 3 /* Avoid transient issues */)(
  ConditionalRemediationSteps.VENDOR_CP -> "A way to confirming this can be found here: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk91040"
)

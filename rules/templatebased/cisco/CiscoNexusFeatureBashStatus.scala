package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
  *
  */
case class CiscoNexusFeatureBashStatus(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "CiscoNexusFeatureBashStatus",
  ruleFriendlyName = "Cisco Nexus: Spectre and Meltdown vulnerable",
  ruleDescription = "Indeni will alert if bash-shell is enabled.",
  metricName = "nexus-feature-bash-status",
  alertIfDown = false,
  alertDescription = "The Nexus switch could be vulnerable from the Spectre (CVE-2017-5715) and the Meltdown (CVE-2017-5754 , CVE-2017-5753 ). " +
    "It has been noticed that the vulnerable condition feature bash active status is met and the attacker could exploit it. " +
    "Configuration change is recommended by the vendor.",
  baseRemediationText = "Further restrict access to the various shell environments in case that is not needed to execute " +
    "additional third-party software on the Nexus switch. In particular the next steps are recommended by Cisco: \n" +
    "1. Disable access to the host shell by configuring \"no feature bash\"\n2. Remove the guest shell by executing \"guestshell destroy\"\n" +
    "3. Change any configuration setting user's shell from bash to vsh: \"username shelltype vsh\"\n4. Remove any users with dev-ops role \n" +
    "5. Disable the NX-OS Open Agent Container (OAC) feature. This feature allows users to load their own container based application. " +
    "Run the NX-OS command “show virtual-service detail” to check the activation status of this feature.\n" +
    "6. Check the announced vulnerable NX-OS releases at the next Security Advisory Notice (CCO access level is required) :\n" +
    "https://tools.cisco.com/security/center/content/CiscoSecurityAdvisory/cisco-sa-20180104-cpusidechannel\n" +
    "Review the next Nexus-Indeni Meltdown/Spectre article for more details:\nhttps://indeni.com/cisco-nexus-switches-indeni-series/")()

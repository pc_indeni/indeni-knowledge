package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CiscoNexusVpcConsistencyCheck(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_vpc_consistency_check",
  ruleFriendlyName = "Cisco Nexus: Graceful consistency check feature not enabled",
  ruleDescription = "Since NX-OS version 5.2, graceful consistency check has been introduced to soften vPC system reaction in occurrence to type 1 consistency check. With vPC graceful type-1 check capability, only member ports on secondary vPC peer device are brought down. vPC member ports on primary vPC peer device remain up and process all traffic coming from (or going out to) the access device. Indeni will alert if the graceful consistency check feature is not enabled.",
  severity = AlertSeverity.WARN,
  metricName = "nexus-vpc-consistency-check",
  alertDescription = "The graceful consistency check feature is not enabled.",
  baseRemediationText = """1. Use the “show running-config vpc all” command to verify that the vPC graceful consistency-check feature is not enabled.
                          |2. Use the “show vpc” command to check the status of vPC graceful consistency-check feature.
                          |3. vPC graceful type-1 check is enabled by default. The associated command is: “Nexus(config-vpc-domain)# graceful consistency-check”. Check that both vPC peer devices are configured with this command.
                          |4. Cisco strong recommendation is to always enable vPC graceful type-1 check on both vPC peer devices.
                          |5. Read the next cisco document to review the graceful consistency-check feature: https://www.cisco.com/c/dam/en/us/td/docs/switches/datacenter/sw/design/vpc_design/vpc_best_practices_design_guide.pdf""".stripMargin
)()

package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

case class CiscoNexusFexDiagnosticStatusOverall(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_fex_diagnostic_status_overall",
  ruleFriendlyName = "Cisco Nexus: Nexus FEX diagnostic status failure",
  ruleDescription = "Indeni will alert if one or more of the Nexus FEX diagnostic metrics have failed",
  metricName = "fex-diagnostic-status-overall",
  applicableMetricTag = "name",
  alertItemsHeader = "Alert items",
  alertDescription = "Online diagnostics provide verification of the FEX hardware components executed either during the bootup or during the operation of the switch. Problems either to the bootup or health monitoring diagnostics have been noticed.",
  baseRemediationText = """1. To display the results from the diagnostic tests for a Fabric Extender chassis, use the “show diagnostic result fex all” command. Review the output to identify the failed hardware component and open a ticket to Cisco TAC in case that refers to a faulty component.
                            |2. Run the “show diagnostic bootup level” command to display the bootup diagnostics level. It is not recommended by Cisco to bypass the online diagnostic tests.
                            |3. A brief review of the diagnostic tests is provided below:
                            |• SPROM: Verifies the integrity of backplane and supervisor SPROMs.
                            |• Fabric engine: Tests the switch fabric ASICs.
                            |• Fabric port: Tests the ports on the switch fabric ASIC.
                            |• Forwarding engine: Tests the forwarding engine ASICs.
                            |• Forwarding engine port: Tests the ports on the forwarding engine ASICs.
                            |• Front port: Tests the components (such as PHY and MAC) on the front ports.
                            |• LED: Monitors port and system status LEDs.
                            |• Temperature Sensor: Monitors temperature sensor readings.""".stripMargin
)()

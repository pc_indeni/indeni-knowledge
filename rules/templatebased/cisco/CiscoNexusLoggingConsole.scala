package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CiscoNexusLoggingConsole(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_logging_console",
  ruleFriendlyName = "Cisco Nexus: Log messages are sent to console",
  ruleDescription = "Indeni will alert if Log messages are sent to console",
  severity = AlertSeverity.WARN,
  metricName = "logging-console-status",
  alertIfDown = false,
  alertDescription = "Logging to console can elevate the CPU load of a Cisco NX-OS device, and therefore is not recommended by Cisco",
  baseRemediationText = """1. It is recommended to send logging information to the local log buffer or the local log file, which can be viewed using the “show logging” command.
                           |2. Use the global configuration commands  “no logging  console” to disable logging to the monitor sessions.
                           |3. If logging output is required for troubleshooting purposes, you should enable it only temporarily, to monitor for vty sessions, and avoid using it on the console. Be sure to disable logging to monitor sessions after troubleshooting is completed.
                           |4. Refer to the Cisco NX-OS System Management Configuration Guide for more information about global configuration commands for logging: https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/5_x/nx-os/system_management/configuration/guide/sm_nx_os_cg.html
                            """.stripMargin
)()

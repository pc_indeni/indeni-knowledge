package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

case class CiscoNexusFexDiagnosticStatusInbandInterface(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_fex_diagnostic_status_inband_interface",
  ruleFriendlyName = "Cisco Nexus: Nexus FEX diagnostic inband interface failed",
  ruleDescription = "Indeni will alert if Nexus FEX inband interface metric has failed",
  metricName = "fex-diagnostic-status-inband-interface",
  applicableMetricTag = "name",
  alertItemsHeader = "Alert items",
  alertDescription = "Online diagnostic which validates the connectivity of the inband port to the FEX has failed.",
  baseRemediationText = "Display the results of the diagnostic tests for a Fabric Extender chassis by running the “show diagnostic result fex all” command. Review the output to identify the result of the inband port diagnostic test. Take into account that this diagnostic runs only during FEX bootup or reset. Review the relevant logs triggered by the FEX by running the “show logging” command. Finally, contact CISCO TAC for further assistance."
)()

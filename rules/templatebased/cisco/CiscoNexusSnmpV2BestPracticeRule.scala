package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{RuleHelper, SingleSnapshotValueCheckTemplateRule}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class CiscoNexusSnmpV2BestPracticeRule(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "CiscoNexusSnmpV2BestPracticeRule",
  ruleFriendlyName = "Cisco Nexus: SNMPv2 configuration security best practices are not applied",
  ruleDescription = "Indeni will alert if SNMPv2 configuration security best practices are not applied",
  severity = AlertSeverity.WARN,
  metricName = "snmp-v2-best-practice",
  alertDescription = "SNMPv2 must be properly secured to protect the confidentiality, integrity, and availability of the network devices. " +
    "SNMPv2 community strings are passwords that are applied to a Cisco NX-OS device to restrict access, both read-only and read-write " +
    "access, to the SNMP data on the device. An ACL should be applied that further restricts SNMP2 access to a selected group of " +
    "source IP addresses in addition to the community string.",
  baseRemediationText = "1. Execute the Cisco NX-OS command “show running-config snmp” to review the SNMP configuration. Note: the SNMP communities are transmitted in clear text.\n" +
    "2. Run the command “show snmp”  to check the status (enabled/disabled) of the SNMP protocol.\n" +
    "3. In case that the SNMP protocol is not enabled run the command  “snmp-server protocol enable” in global configuration mode to enable it\n" +
    "4. Run the next command to apply an ACL to SNMPv2 communities without ACLs “snmp-server community <password> use-acl  <ACL-NAME>”. " +
    "This feature has been introduced in Cisco NX-OS Release 4.2(1).\n" +
    "5. CISCO SNMP best practices management hardening guide can be found to the next link: " +
    "https://www.cisco.com/c/en/us/about/security-center/securing-nx-os.html#_Toc303633213\n",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("false"), SnapshotExpression("snmp-v2-best-practice").asSingle().mostRecent().value().noneable)
)()




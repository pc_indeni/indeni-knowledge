package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class CiscoNexusLocalProxyArpRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "CiscoNexusLocalProxyArpRule",
  ruleFriendlyName = "Cisco Nexus: Local-proxy arp is activated.",
  ruleDescription = "Indeni will alert if Local-proxy arp is activated.",
  severity = AlertSeverity.WARN,
  metricName = "interface-local-proxy-arp-status",
  applicableMetricTag = "name",
  alertIfDown = false,
  alertItemsHeader = "Interfaces Affected",
  alertDescription = "Activation makes the router answer all ARP requests on configured subnet, even for clients that " +
    "shouldn't normally need routing. In addition, it increases the amount of ARP traffic on a network segment. " +
    "In addtion, hosts need larger ARP tables in order to handle IP-to-MAC address mappings. Finally, security can be " +
    "undermined since a machine can claim to be another in order to intercept packets, an act called \"spoofing\".",
  baseRemediationText = "It is recommended by the vendor in most cases to disable the local proxy arp.\n" +
    "Local Proxy ARP can be disabled by using the interface configuration command \"no ip local-proxy-arp\".")()



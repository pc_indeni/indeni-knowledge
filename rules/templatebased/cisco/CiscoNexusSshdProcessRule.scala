package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{NumericThresholdOnDoubleMetricTemplateRule, ThresholdDirection}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity



case class CiscoNexusSshdProcessRule(context: RuleContext) extends NumericThresholdOnDoubleMetricTemplateRule(context,
  ruleName = "CiscoNexusSshdProcessRule",
  ruleFriendlyName = "Cisco Nexus: Too many “dcos_sshd” NX-OS processes have not terminated normally.",
  ruleDescription = "Indeni will alert if too many “dcos_sshd” NX-OS processes have not terminated normally.",
  severity = AlertSeverity.WARN,
  metricName = "process-docs-sshd-counter",
  threshold = 5.0,
  thresholdDirection = ThresholdDirection.ABOVE,
  alertDescriptionFormat = "There are %.0f dcos_sshd process.\n\n"+
    "It has been noticed that for a limited number of NX-OS versions utilizing multiple ssh session can be CPU " +
    "resource intensive and can affect the overall performance of the switch. In particular, the SSH daemon hung after a short " +
    "period of time and consumes too much CPU resources. Cisco is aware of this issue and relevant bugs have been officially " +
    "published and are accessible to the CISCO bug repository portal.",
  baseRemediationText = "1. Run the “show users” command to view the active ssh sessions to the switch\n" +
    "2. Execute the “show processes cpu | i dcos_sshd” command to display the sshd processes and the CPU utilization consumed per process. " +
    "Check if the total CPU utilization is high. Check also the output of the \"show processes cpu history\" command to review " +
    "the CPU utilization history up to last 72 hours\n" +
    "3. The “show processes cpu | i dcos_sshd | count” command execution shows the total number of dcos_sshd processes\n" +
    "4. Cisco recommends as a temporary workaround to disable the SSH protocol by running the “no feature ssh” command. " +
    "This clears all the hung ssh sessions. Then the ssh feature can be enabled and closed monitor the Nexus switch. " +
    "Attention: to not lose the remote access via ssh during the implementation of this temporarily solution\n" +
    "5. It is also recommended to run the “exec-timeout 5” command to timeout any inactive ssh session after 5min period of time\n" +
    "6. Review the Cisco bug repository and schedule an upgrade to a NX-OS version recommended by Cisco\n" +
    "7. Review the “How to Use the SSH Protocol to Safely Discover and Analyze Cisco Nexus with Indeni” article for more details. " +
    "https://indeni.com/how-to-use-the-ssh-protocol-to-safely-discover-and-analysis-with-indeni-the-cisco-nexus/\n")()








package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class CiscoNexusUrpfRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "CiscoNexusUrpfRule",
  ruleFriendlyName = "Cisco Nexus: uRPF is disabled.",
  ruleDescription = "Indeni will alert if uRPF is disabled.",
  severity = AlertSeverity.WARN,
  metricName = "interface-urpf-status",
  applicableMetricTag = "name",
  alertItemsHeader = "Interfaces Affected",
  alertDescription = "Many attacks use source IP address spoofing to be effective or to conceal the true source of an attack and hinder accurate traceback. " +
    "Cisco NX-OS provides uRPF and IP source guard to deter attacks that rely on source IP address spoofing. " +
    "In particular, uRPF provides source network verification and can reduce spoofed attacks from networks that are not under direct administrative control.",
  baseRemediationText = "uRPF is configured on a per-interface basis and it can be configured in either of two modes: loose or strict. " +
    "In cases in which asymmetric routing exists, loose mode is preferred because strict mode is known to drop packets in these situations. " +
    "During configuration of the \"ip verify\" interface configuration command.\nRefer to the section the Cisco NX-OS Security Configuration Guide " +
    "for more information about the configuration and use of uRPF  " +
    "https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/4_1/nx-os/security/configuration/guide/sec_nx-os-cfg/sec_urpf.html\n")()







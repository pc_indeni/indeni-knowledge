package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class CiscoNexusCoppRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "CiscoNexusCoppRule",
  ruleFriendlyName = "Cisco Nexus: Policy-map is not configured to the control-plane",
  ruleDescription = "Indeni will alert if a Policy-map is not configured to the control-plane.",
  severity = AlertSeverity.WARN,
  metricName = "copp-status",
  alertDescription = "The CoPP (Control Plane Policy) feature is used to restrict IP packets that are destined for the " +
    "infrastructure device itself and require control-plane CPU processing.",
  baseRemediationText = "An effective CoPP policy is a complicated task and requires adequate planning and testing before being " +
    "deployed in a live production environment. In brief the next steps are needed:\n" +
    "1. Class maps are defined to match specific types of traffic\n" +
    "2. Policy maps are created to apply policing (rate-limiting) policies to class-map-matched traffic\n" +
    "3. A service policy is used to map the policy map to the control-plane interface\n" +
    "Refer to the Configuring Control-Plane Policing section of the Cisco NX-OS Security Configuration Guide for more information " +
    "about the configuration and use of the CoPP feature: " +
    "https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/4_1/nx-os/security/configuration/guide/sec_nx-os-cfg/sec_cppolicing.html")()


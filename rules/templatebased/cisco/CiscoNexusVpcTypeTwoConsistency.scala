package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

case class CiscoNexusVpcTypeTwoConsistency(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_vpc_type_two_consistency",
  ruleFriendlyName = "Cisco Nexus: vPC type 2 inconsistency",
  ruleDescription = "Indeni will alert if a VPC type 2 inconsistency has been detected on the device.",
  metricName = "nexus-vpc-type-2-consistency",
  alertDescription = "VPC type 2 inconsistency detected on the device and can cause issues including packet drop.",
  baseRemediationText = """1. Enter the “show vpc consistency-parameters global” command to display the global configuration values and vPC interfaces parameters.
                          |2. Use the “show vpc consistency-parameters interface port-channel” command to display per interface the consistency parameters and determine where the configuration mismatch occurs.
                          |3. Use the “show vpc” command to check the status of vPC and review the consistency/reason status of the command output.
                          |4. Use the “show running-config vpc” command to verify the vPC configuration and that the vPC peer ports or membership ports have identical configurations. To help ensure that all the configuration parameters are compatible, it is recommended to display the configurations for each vPC peer device once you configure the vPC.
                          |5. Run the show logging last <value> command to review the most recent and relevant logs.
                          |6. Contact CISCO TAC for further assistance.
                          |7. Review the next cisco document to find all the type 2 parameters: https://www.cisco.com/c/dam/en/us/td/docs/switches/datacenter/sw/design/vpc_design/vpc_best_practices_design_guide.pdf""".stripMargin
)()

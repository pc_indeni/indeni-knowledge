package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

case class CiscoNexusVpcKeepAlive(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_vpc_keepalive",
  ruleFriendlyName = "Cisco Nexus: vPC peer-keepalive link down",
  ruleDescription = "In the event the Peer Keepalive Link fails it will not affect the operation of the vPC. The Keepalive Link is used as a secondary test mechanism to confirm the vPC peer is live in case of the Peer-Link goes down. Indeni will alert if the vPC peer-keepalive link is down.",
  metricName = "nexus-vpc-keepalive",
  alertDescription = "vPC peer-keepalive link is down.",
  baseRemediationText = """1. Use the “show vpc”  and “show vpc peer-keepalive” command to check the status of the vPC peer-keepalive link.
                          |2. Use the “show running-config vpc” command to verify the vPC configuration.
                          |3. Ensure that both the source and destination IP addresses used for the peer-keepalive messages are unique in your network and these IP addresses are reachable from the VRF associated with the vPC peer-keepalive link.
                          |4. Consider that the switch cannot bring up the vPC peer-link unless the peer-keepalive link is already up and running.
                          |5. As best practice consider to configure the vPC Peer Keepalive link to use a separate VRF instance to ensure that the peer keepalive traffic is always carried on that link and never on the Peer-Link.
                          |6. As best practice consider to remove the keepalive vlan  from the trunk allowed list of the vPC Peer-Link or the vPC Member Ports.
                          |7. Run the show logging last <value> command to review the last logs.
                          |8. Contact CISCO TAC for further assistance.
                          |9. Consider to implement the vPC keepalive best practices described at the next cisco article: https://www.cisco.com/c/dam/en/us/td/docs/switches/datacenter/sw/design/vpc_design/vpc_best_practices_design_guide.pdf""".stripMargin
)()

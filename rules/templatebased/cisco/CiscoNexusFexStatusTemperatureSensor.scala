package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

case class CiscoNexusFexStatusTemperatureSensor(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_fex_status_temperature_sensor",
  ruleFriendlyName = "Cisco Nexus: Nexus FEX diagnostic temperature status failed",
  ruleDescription = "Indeni will alert if Nexus FEX temperature metric has failed",
  metricName = "fex-diagnostic-status-temperature-sensor",
  applicableMetricTag = "name",
  alertItemsHeader = "Alert items",
  alertDescription = "Online diagnostic which validates the temperature to the FEX has failed",
  baseRemediationText = "Display the results of the diagnostic tests for a Fabric Extender chassis by running the “show diagnostic result fex all” command. Review the output to identify the result of the Temperature Sensors test. Take into account that this diagnostic runs in the background to check the temperature status of the FEX. Run the “show environment fex all” command to review the temperature status of the FEX. Review the relevant logs triggered by the FEX by running the “show logging” command. Finally, contact CISCO TAC for further assistance."
)()

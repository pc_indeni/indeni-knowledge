package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.server.rules.library._

/**
  *
  */
case class CiscoNexusSnmpV3BestPracticeRule(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "CiscoNexusSnmpV3BestPracticeRule",
  ruleFriendlyName = "Cisco Nexus: SNMPv3 is not configured according to the best security practices",
  ruleDescription = "This rule captures whether SNMPv3 is configured according to the best security practises by validating that SNMP " +
                    "requires encryption for incoming requests. By default, the SNMP agent accepts SNMPv3 messages without encryption. " +
                    "When you enforce privacy, Cisco NX-OS responds with an authorizationError for any PDU request using securityLevel " +
                    "parameter of either noAuthNoPriv or authNoPriv.",
  severity = AlertSeverity.WARN,
  metricName = "snmp-v3-best-practice",
  alertDescription = "SNMP is not configured to require authentication or encryption for incoming requests.",
  baseRemediationText = """By default, the SNMP agent accepts SNMPv3 messages without authentication and encryption.
                          |When you enforce privacy, Cisco NX-OS responds with an authorizationError for any SNMPv3 PDU request using securityLevel parameter of either noAuthNoPriv or authNoPriv.
                          |
                          |1. Run the “show run snmp” command to review the SNMPv3 current configuration
                          |2. To enforce SNMP message encryption for a user in the global configuration mode run the next command “snmp-server user nameenforcePriv”
                          |3. To enforce SNMP message encryption for all users in the global configuration mode, run the next command “snmp-server globalEn forcePriv”
                          |4. Refer to the next SNMP configuration guide for more details: https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/configuration/guide/cli/CLIConfigurationGuide/sm_snmp.html""".stripMargin,
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("false"), SnapshotExpression("snmp-v3-best-practice").asSingle().mostRecent().value().noneable))()

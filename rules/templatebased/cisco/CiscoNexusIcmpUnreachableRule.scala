package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class CiscoNexusIcmpUnreachableRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "CiscoNexusIcmpUnreachableRule",
  ruleFriendlyName = "Cisco Nexus: ICMP unreachable messages are activated.",
  ruleDescription = "Indeni will alert if ICMP unreachable messages are activated.",
  severity = AlertSeverity.WARN,
  metricName = "interface-icmp-unreachable-status",
  applicableMetricTag = "name",
  alertIfDown = false,
  alertItemsHeader = "Interfaces Affected",
  alertDescription = "Generating these messages can increase CPU utilization on the device. ",
  baseRemediationText = "You can disable ICMP unreachable message generation using the interface configuration command \"no ip unreachables\". " +
    "It is recommended by the vendor to disable these messages.")()






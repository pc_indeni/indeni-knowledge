package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

case class CiscoNexusFexStatusPortFabric(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_fex_status_port_fabric",
  ruleFriendlyName = "Cisco Nexus: Nexus FEX diagnostic ASIC Fabric port status failed.",
  ruleDescription = "Indeni will alert if Nexus FEX ASIC Fabric Ports has failed",
  metricName = "fex-diagnostic-status-port-fabric",
  applicableMetricTag = "name",
  alertItemsHeader = "Alert items",
  alertDescription = "Online diagnostic which checks the fabric ports on the FEX ASIC (application-specific integrated circuit) has failed",
  baseRemediationText = "Display the results of the diagnostic tests for a Fabric Extender chassis by running the “show diagnostic result fex all” command. Review the output to identify the result of the fabric (uplink) ports on the ASIC (application-specific integrated circuit) test. Run the \"show interface fex-fabric\" and \"show fex detail\" commands and review the output. Investigate the relevant logs triggered by the FEX by running the “sh system internal fex log fport” command. Review the next documentation https://www.cisco.com/c/en/us/support/docs/switches/nexus-2000-series-fabric-extenders/200265-Troubleshooting-Fabric-Extender-FEX-Pe.html. Finally, contact CISCO TAC for further assistance."
)()

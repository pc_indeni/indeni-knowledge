package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

case class CiscoNexusDiagnosticStatusPowerSupply(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_diagnostic_status_power_supply",
  ruleFriendlyName = "Cisco Nexus: Nexus FEX diagnostic Power Supply  failed",
  ruleDescription = "Indeni will alert if Nexus FEX Power Supply has failed",
  metricName = "fex-diagnostic-status-fan",
  applicableMetricTag = "name",
  alertItemsHeader = "Alert items",
  alertDescription = "Online diagnostic which validates the Power Supply to the FEX has failed.",
  baseRemediationText = "Display the results of the diagnostic tests for a Fabric Extender chassis by running the “show diagnostic result fex all” command. Review the output to identify the result of the Power Supplies test. Take into account that this diagnostic runs in the background to check the power supplies status of the switch. Run the “show environment fex all” command to review the health status of the power supplies. Review the relevant logs triggered by the FEX by running the “show logging” command. Finally, contact CISCO TAC for further assistance."
)()

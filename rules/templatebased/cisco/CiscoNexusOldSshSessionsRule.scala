package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NumericThresholdOnDoubleMetricWithItemsTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CiscoNexusOldSshSessionsRule(context: RuleContext) extends NumericThresholdOnDoubleMetricWithItemsTemplateRule(context,
    ruleName = "CiscoNexusOldSshSessionsRule",
    ruleFriendlyName = "Cisco Nexus: High number of hung ssh sessions",
    ruleDescription = "Indeni will alert when the number of hung ssh sessions reaches a certain limit.",
    severity = AlertSeverity.WARN,
    metricName = "ssh-hung-sessions-counter",
    threshold = 5.0,
    applicableMetricTag = "display-name",
    alertItemDescriptionFormat = "%.0f",
    alertItemsHeader = "Hung sessions",
    alertDescription = "The stale ssh sessions can consume all the available remote sessions and the remote access (login) to the Nexus via SSH or Telnet to be denied. This may have impact also to the CPU utilization.",
    baseRemediationText = """1. Run the “show users” NX-OS command to view the active and stale remote sessions to the nexus switch. When the entries to the IDLE column of the command output are marked as "old" then this means that the session is in stale state. On N5K series switches the hung sessions can also be seen with NX-OS commands “show sockets conn” and “show processes cpu | i sshd”
                            |2. A temporary workaround is to configure  the default VTY sessions to the maximum value. Configure the “session-limit  <sessions> “ NX-OS command to increase the maximum number of virtual sessions for the Nexus device. The range is from 1 to 60. The default is 32. More details can be found to the next link: https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/fundamentals/521n11/b_5k_Fund_Config_521N11/b_5k_Fund_Config_521N11_chapter_0110.html
                            |3. A workaround is to disable temporarily the SSH protocol by running the “no feature ssh” command. This command clears all the hung ssh sessions. Then the ssh feature can be re-enabled and is needed to close monitor the Nexus switch for new stale ssh sessions. Attention: risk to lose the remote access via ssh during the implementation of this temporarily solution
                            |4. It is recommended to configure the “exec-timeout 5” command to timeout any inactive ssh session after a 5min period of time
                            |5. Finally, review the Cisco bug repository and schedule an upgrade to a NX-OS version recommended by Cisco which resolves this issue. More details for this bug and recommended NX-OS releases can be found to the next links:
                            |https://bst.cloudapps.cisco.com/bugsearch/bug/CSCux87583
                            |https://bst.cloudapps.cisco.com/bugsearch/bug/CSCux11285/?rfs=iqvred""".stripMargin
    )()

package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class CiscoNexusDirectedBroadcastsRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "CiscoNexusDirectedBroadcastsRule",
  ruleFriendlyName = "Cisco Nexus: IP directed broadcast is enabled.",
  ruleDescription = "Indeni will alert if IP directed broadcast is enabled.",
  severity = AlertSeverity.WARN,
  metricName = "interface-directed-broadcasts-status",
  applicableMetricTag = "name",
  alertIfDown = false,
  alertItemsHeader = "Interfaces Affected",
  alertDescription = "IP directed broadcasts make it possible to send an IP broadcast packet to a remote IP subnet. " +
    "After the packet reaches the remote network, the forwarding IP device sends the packet as a Layer 2 broadcast to " +
    "all stations on the subnet. This directed broadcast function has been used as an amplification and reflection aid " +
    "in several attacks, including the smurf attack.",
  baseRemediationText = "Current versions of Cisco NX-OS have this function disabled by default. It can also be disabled " +
    "with the “no ip directed-broadcast” interface configuration command. It is recommended by the vendor in most cases to disable it.\n" +
    "Refer to “Configuring IPv4” in the Cisco NX-OS Unicast Routing Configuration Guide for more information about the " +
    "ip directed-broadcast command " +
    "https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/6_x/nx-os/unicast/configuration/guide/b-7k-Cisco-Nexus-7000-Series-NX-OS-Unicast-Routing-Configuration-Guide-Release-6x/n7k_unicast_new_and_changed.html")()





package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

case class CiscoNexusFexDiagnosticStatusSprom(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_fex_diagnostic_status_sprom",
  ruleFriendlyName = "Cisco Nexus: Nexus FEX diagnostic SPROM failed",
  ruleDescription = "Indeni will alert if Nexus FEX SPROM metric has failed",
  metricName = "fex-diagnostic-status-sprom",
  applicableMetricTag = "name",
  alertItemsHeader = "Alert items",
  alertDescription = "Online diagnostics which verifies the integrity of backplane and supervisor SPROMs (Serial Programmable Read-Only Memory) of the FEX has failed.",
  baseRemediationText = "Display the results of the diagnostic tests for a Fabric Extender chassis by running the “show diagnostic result fex all” command. Review the output to identify the result of the SPROMs (Serial Programmable Read-only Memory) diagnostic test.  Run the \"show sprom fex <id> all\" command and evaluate the command output. Review the relevant logs triggered by the FEX by running the “show logging” command. Finally, contact CISCO TAC for further assistance."
)()

package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CiscoNexusVpcMemberThroughPeerStatusRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "CiscoNexusVpcMemberThroughPeerStatusRule",
  ruleFriendlyName = "Cisco Nexus: vPC vlans of vPC member ports are not defined on the vPC peer link",
  ruleDescription = "Whenever a vPC VLAN is defined on vPC member port it must be defined also on vPC peer-link. Not defining a vPC VLAN on vPC peer-link will make the VLAN not operational.",
  severity = AlertSeverity.WARN,
  metricName = "nexus-vpc-member-thru-peer-status",
  applicableMetricTag = "name",
  alertItemsHeader = "vPC members",
  alertDescription = "vPC vlans of vPC member ports are not defined on the vPC peer link.",
  baseRemediationText = """1. Check that the VLAN configuration of the  vPC member ports matches on both vPC peer devices and vPC peer link.
                          |2. Run the “show vpc” command and review the active vlans through the vPC peer link.
                          |3. Execute the “show vpc consistency-parameters interface <interface>” command and review the allowed vlans and port mode (access or trunk) per vPC member port. If there is an inconsistency (type 1 or type 2), a VLAN or the entire port channel may suspend. This depends on type-1 or type-2 consistency check for the vPC member port. 
                          |4. It is recommended to use same vPC ID as port-channel ID for ease of configuration, monitoring, and troubleshooting.
                          |5. Refer to the next link for more information: https://www.cisco.com/c/dam/en/us/td/docs/switches/datacenter/sw/design/vpc_design/vpc_best_practices_design_guide.pdf""".stripMargin
)()

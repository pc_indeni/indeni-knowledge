package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{NumericThresholdOnDoubleMetricTemplateRule, ThresholdDirection}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
//TODO switch to counter
case class CiscoNexusCheckpointRule(context: RuleContext) extends NumericThresholdOnDoubleMetricTemplateRule(context,
  ruleName = "CiscoNexusCheckpointRule",
  ruleFriendlyName = "Cisco Nexus: Checkpoint and Configuration Rollback are not activated",
  ruleDescription = "Indeni will alert if Checkpoint and Configuration Rollback are not activated.",
  severity = AlertSeverity.WARN,
  metricName = "checkpoint-status",
  threshold = 1.0,
  thresholdDirection = ThresholdDirection.BELOW,
  alertDescriptionFormat = "The recommended by CISCO feature to allow the system to maintain an archive of snapshot configurations " +
    "and fast rollback is not activated.",
  baseRemediationText = "The NX-OS checkpoint and rollback feature are extremely useful, and a life saver in some cases, " +
    "when a new configuration change to a production system has caused unwanted effects or was incorrectly made/planned " +
    "and we need to immediately return to an original/stable configuration.\n" +
    "1. A manual configuration checkpoint can be initiated with the “checkpoint” command. \n" +
    "2. Automated configuration checkpoints can be generated periodically by combining the checkpoint and scheduler features of Cisco NX-OS.\n" +
    "3. Checkpoints in the internal system checkpoint database can be viewed with the command “show checkpoint summary”, and the actual " +
    "contents of the checkpoint files can be viewed with “show checkpoint”.\n" +
    "4. A running configuration can be rolled back to a checkpoint using the “rollback” command.\n" +
    "5. Refer to the Cisco NX-OS System Management Configuration Guide for further information about this feature: " +
    "https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/5_x/nx-os/system_management/configuration/guide/sm_nx_os_cg/sm_7rollback.html")()



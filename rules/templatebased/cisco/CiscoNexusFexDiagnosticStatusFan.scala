package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

case class CiscoNexusDiagnosticStatusFan(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_diagnostic_status_fan",
  ruleFriendlyName = "Cisco Nexus: Nexus FEX diagnostic Fan failed",
  ruleDescription = "Indeni will alert if Nexus FEX fan metric has failed",
  metricName = "fex-diagnostic-status-fan",
  applicableMetricTag = "name",
  alertItemsHeader = "Alert items",
  alertDescription = "Online diagnostic which validates the fan speed and fan control to the FEX has failed.",
  baseRemediationText = "Display the results of the diagnostic tests for a Fabric Extender chassis by running the “show diagnostic result fex all” command. Review the output to identify the result of the fan diagnostic test. Take into account that this diagnostic runs in the background to check the fan status of the switch. Run the “show environment fex all” command to review the health status of the fans. Review the relevant logs triggered by the FEX by running the “show logging” command. Finally, contact CISCO TAC for further assistance."
)()

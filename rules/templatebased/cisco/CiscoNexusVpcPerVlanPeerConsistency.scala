package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

case class CiscoNexusVpcPerVlanPeerConsistency(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_vpc_per_vlan_peer_consistency",
  ruleFriendlyName = "Cisco Nexus: vPC VLAN inconsistency",
  ruleDescription = "There is  vPC per VLAN inconsistency between the vPC peers. Beginning with Ciscon NX-OS Release 5.0(2)N2(1), some Type-1 consistency checks are performed on a per-VLAN basis. VLANs that fail the consistency check are brought down on both the vPC peer switches while other VLANs are operational. Per-VLAN consistency checks are not dependent on whether graceful consistency checks are enabled.",
  metricName = "nexus-vpc-per-vlan-peer-consistency",
  alertDescription = "vPC VLAN inconsistency.",
  baseRemediationText = """1. Run the “show vpc consistency-parameters vlans” command to display the vlan consistency parameters.
                          |2. Use the “show vpc consistency-parameters interface port-channel” command to display per vPC interface the type 1 consistency parameters and determine where the configuration mismatch occurs.
                          |3. Enter the “show vpc consistency-parameters global” command to display the global configuration values and vPC interfaces parameters.
                          |4. Use the “show vpc” command to check the status of vPC and review the consistency/reason status of the command output.
                          |5. Use the “show running-config vpc” command to verify the vPC configuration and that the vPC peer ports or membership ports have identical configurations. To help ensure that all the configuration parameters are compatible, it is recommended to display the configurations for each vPC peer device once you configure the vPC.
                          |6. Use the “show vlan” command to both  vPC peers to review the cofigured VLAN and the status. Check also that the same vlans are allowed to the trunk vPC peer link between the vPC peers.
                          |7. Run the show logging last <value> command to review the most recent and relevant logs.
                          |8. Contact CISCO TAC for further assistance.""".stripMargin
)()

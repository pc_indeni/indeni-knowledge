package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.server.rules.library._

/**
  *
  */
case class CiscoNexusSnmpV3EnabledRule(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "CiscoNexusSnmpV3EnabledRule",
  ruleFriendlyName = "Cisco Nexus: SNMPv3 is not enabled",
  ruleDescription = "It is recommended to use SNMPv3 because it has authentication and encryption capabilities for username, passwords, and payload data. Indeni will alert if SNMPv3 is disabled.",
  severity = AlertSeverity.WARN,
  metricName = "snmp-v3-enabled",
  alertDescription = "SNMPv3 is not enabled",
  baseRemediationText = """SNMPv3 is the recommended SNMP version because of the additional security authentication and encryption mechanisms.
                          |By default, all user accounts in the local database are synchronized to a SNMP user that can be used by an SNMP server to authenticate SNMPv3 requests.
                          |Additional user accounts/SNMP user accounts can be created for SNMP polling and sending SNMP inform notifications.
                          |
                          |1. Run the “show run snmp” command to review the SNMPv3 status
                          |2. Apply the following NX-OS command to create a SNMPv3 user “snmp-server user snmp-user auth md5 <password> priv aes-128 <password> localizedkey”
                          |3. Refer to the next SNMP configuration guide for more details: https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/5_x/nx-os/system_management/configuration/guide/sm_nx_os_cg/sm_9snmp.html#85890""".stripMargin,
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("false"), SnapshotExpression("snmp-v3-enabled").asSingle().mostRecent().value().noneable))()

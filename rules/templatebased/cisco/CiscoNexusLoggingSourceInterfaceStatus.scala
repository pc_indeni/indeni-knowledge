package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CiscoNexusLoggingSourceInterfaceStatus(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_logging_source_interface_status",
  ruleFriendlyName = "Cisco Nexus: Logging source interface should be configured",
  ruleDescription = "Indeni will alert if logging source interface is not configured",
  severity = AlertSeverity.WARN,
  metricName = "logging-source-interface-status",
  alertIfDown = true,
  alertDescription = "To provide an increased level of consistency when collecting and reviewing log messages, it should statically be configured a logging source interface.",
  baseRemediationText = """The logging source-interface interface command, statically assigns a logging source interface helps ensure that the same IP address appears in all logging messages that are sent from an individual Cisco NX-OS device. For added stability, it is recommended to be used a loopback interface as the logging source.
                          |Use of the logging source-interface interface global configuration command to specify source IP address for the log messages
                          |Refer to the Cisco NX-OS System Management Configuration Guide for more information: https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/5_x/nx-os/system_management/configuration/guide/sm_nx_os_cg.html
                            """.stripMargin
)()

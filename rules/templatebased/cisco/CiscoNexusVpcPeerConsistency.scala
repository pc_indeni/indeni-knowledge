package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

case class CiscoNexusVpcPeerConsistency(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_vpc_peer_consistency",
  ruleFriendlyName = "Cisco Nexus: vPC configuration inconsistency",
  ruleDescription = "Indeni will alert when there is a problem to the vPC configuration consistency between vPC peers.",
  metricName = "nexus-vpc-peer-consistency",
  alertDescription = "There is a problem to the vPC configuration Consistency between the vPC peers.",
  baseRemediationText = """1. Use the “show vpc consistency-parameters interface port-channel” command to display per vPC interface the type 1 consistency parameters and determine where the configuration mismatch occurs.
                          |2. Enter the “show vpc consistency-parameters global” command to display the global configuration values and vPC interfaces parameters. The displayed output provides only those configurations that would limit the vPC from coming up. Review also the output of the other command options of the “show vpc consistency-parameters <X>”,  X = <vlans> or <vpc>.
                          |3. Use the “show vpc” command to check the status of vPC and review the consistency/reason status of the command output.
                          |4. Use the “show running-config vpc” command to verify the vPC configuration and that the vPC peer ports or membership ports have identical configurations. To help ensure that all the configuration parameters are compatible, it is recommended to display the configurations for each vPC peer device once you configure the vPC.
                          |5. Run the show logging last <value> command to review the most recent and relevant logs.
                          |6. Contact CISCO TAC for further assistance. """.stripMargin
)()

package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class CiscoNexusProxyArpRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "CiscoNexusProxyArpRule",
  ruleFriendlyName = "Cisco Nexus: Proxy arp is activated.",
  ruleDescription = "Indeni will alert if Proxy arp is activated.",
  severity = AlertSeverity.WARN,
  metricName = "interface-proxy-arp-status",
  applicableMetricTag = "name",
  alertIfDown = false,
  alertItemsHeader = "Interfaces Affected",
  alertDescription = "Proxy arp increases the amount of ARP traffic on a network segment. In addition, hosts need larger " +
    "ARP tables in order to handle IP-to-MAC address mappings. Finally, security can be undermined since a machine can claim " +
    "to be another in order to intercept packets, an act called \"spoofing\".\n",
  baseRemediationText = "It is recommended by the vendor in most cases to disable the disable the ip proxy arp.\n" +
    "Proxy ARP can be disabled by using the interface configuration command \"no ip proxy-arp\".")()




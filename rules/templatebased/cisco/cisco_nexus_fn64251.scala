package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{RuleHelper, SingleSnapshotValueCheckTemplateRule}

case class cisco_nexus_fn64251(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "cisco_nexus_fn64251",
  ruleFriendlyName = "Cisco Nexus: Clock Signal Component Issue - FN 64251",
  ruleDescription = "Indeni will alert if a Nexus device has the issue described in field notice 64251.",
  metricName = "cisco-nexus-fn64251",
  applicableMetricTag = "name",
  alertItemsHeader = "Components Affected",
  alertDescription = "A clock signal component manufactured by one supplier, and included in some Cisco products, has been seen to degrade over time in some units. The complete field notice can be found here:\nhttp://www.cisco.com/c/en/us/support/docs/field-notices/642/fn64251.html",
  baseRemediationText = """Customer with this affected product should refer to the Clock Signal Component Issue page and follow the instructions in order to request a replacement.
      |It is advised to work with Cisco's TAC to proactively replace the affected hardware.
      |More details about  the relevant CISCO Field Notice: FN – 64251 can be found to the next link :
      |https://www.cisco.com/c/en/us/support/docs/field-notices/642/fn64251.html""".stripMargin,
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("cisco-nexus-fn64251").asSingle().mostRecent().value().noneable)
)()


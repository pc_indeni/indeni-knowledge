package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class cisco_invalid_sfp(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_invalid_sfp",
  ruleFriendlyName = "Cisco Devices: SFP validation failed",
  ruleDescription = "Interface SFP verification error can happen if the interface speed is configured incorrectly or the SFP module is an incompatible non-cisco hardware. Indeni will alert if this happens.",
  severity = AlertSeverity.WARN,
  metricName = "network-interface-invalid-sfp",
  applicableMetricTag = "name",
  alertIfDown = false,
  alertItemsHeader = "Affected Interfaces",
  alertDescription = "SFP validation failed can occur due to interface speed problem, use of incompatible non-cisco SFP or of incompatible installed SFP and interface type.",
  baseRemediationText = """Check the following to troubleshoot and resolve this issue:
                          |1. Run the “show interface” NX-OS command to display the interfaces with SFP validation error message.
                          |2. Run the “show interface transceiver detailed” NX-OS command to display information about the transceivers connected to a specific interface. Besides, this NX-OS command output provides information about the Cisco SFP Product ID (PID).
                          |3. Hard code the speed to these interfaces. An SFP validation message can occur if you insert a 1G RJ45 SFP into a 10G SFP slot.
                          |4. Check if there is a mismatch between the installed SFP and the interface type e.g. FC interface installed at an Ethernet type interface.
                          |5. Review the Transceiver Modules Compatibility Matrix from cisco.com and check for incompatibilities
                          |6. You can use a 3rd party sfp by applying the hidden command “service unsupported-transceiver” at global configuration mode. However, any troubles with your 3rd party SFP+ will not be supported by TAC.""".stripMargin
)()

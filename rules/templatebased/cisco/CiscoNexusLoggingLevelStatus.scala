package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CiscoNexusLoggingLevelStatus(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_logging_level_status",
  ruleFriendlyName = "Cisco Nexus: Logging level status for Console and/or Monitor is debug",
  ruleDescription = "Indeni will alert when logging level status for Console and/or Monitor is debug",
  severity = AlertSeverity.WARN,
  metricName = "logging-con-mon-severity-level-status",
  alertIfDown = false,
  alertDescription = "The severity level chosen determines the level, granularity, and frequency of the log messages generated. It is recommended to avoid logging at level 7. Logging at level 7 produces an elevated CPU load on the device that can lead to device and network instability.",
  baseRemediationText = """The logging level can be changed with the next command “logging server <ip-address|hostname> X” where X is the logging level (X=0-7)
                          |Refer to Configuring System Message Logging in the Cisco NX-OS System Management Configuration Guide for more information about remote logging configuration: https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/5_x/nx-os/system_management/configuration/guide/sm_nx_os_cg/sm_5syslog.html
                            """.stripMargin
)()

package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CiscoNexusVpcPeerGateway(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_vpc_peer_gateway",
  ruleFriendlyName = "Cisco Nexus: vPC Peer-Gateway enhancement not activated",
  ruleDescription = "The vPC peer-gateway capability allows for a vPC switch the forwarding of traffic local to the vPC peer device and avoids use of the peer-link (by not bridging the traffic to the other vPC peer device). The vPC Peer-Gateway enhancement allows vPC interoperability with some network-attached storage (NAS) or load-balancer devices that do not perform a typical default gateway ARP request at boot up. There is no impact on traffic and existing functionality when activating the Peer-Gateway capability. Indeni will alert if the vPC Peer-Gateway enhancement is not activated.",
  severity = AlertSeverity.WARN,
  metricName = "nexus-vpc-peer-gateway",
  alertDescription = "The vPC Peer-Gateway enhancement is not activated.",
  baseRemediationText = """1. Use the “show running-config vpc” command to verify that the vPC peer gateway feature is not configured.
                          |2. Use the “show vpc” command to check the status of vPC peer gateway feature.
                          |3. Cisco recommendation is to always enable vPC peer-gateway in the vPC domain (i.e configure peer-gateway on both vPC peer devices), even if there is no end device using this feature (devices that don’t perform standard ARP request for their default IP gateway). There is no side-effects enabling it.
                          |4. May hosts with an HSRP gateway cannot access beyond their VLAN. In particular, if the host gateway mac-address is mapped to the physical MAC address of any one of the vPC peer-devices, packets may get dropped due to the loop prevention mechanism in vPC. Peer-gateway can be a workaround also for this scenari by mapping the host gateway's mac-address to the HSRP MAC address and not the physical MAC address of any one of the vPC peer-devices.
                          |5. To activate vPC peer-gateway capability, use the following command line (under vPC configuration context mode): “Nexus(config-vpc-domain)# peer-gateway”.
                          | Both vPC peer devices need to be configured with this command.
                          |6. Read the next cisco document to review the vpc peer gateway feature: https://www.cisco.com/c/dam/en/us/td/docs/switches/datacenter/sw/design/vpc_design/vpc_best_practices_design_guide.pdf""".stripMargin
)()

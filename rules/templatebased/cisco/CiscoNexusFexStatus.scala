package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CiscoNexusFexStatus(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_fex_status",
  ruleFriendlyName = "Cisco Nexus: Nexus FEX problem connectivity status",
  ruleDescription = "Indeni will alert if Nexus connectivity status is not operational (online)",
  severity = AlertSeverity.CRITICAL,
  metricName = "fex-status",
  applicableMetricTag = "name",
  alertItemsHeader = "Alert items",
  descriptionMetricTag = "state",
  alertDescription = "The FEX connectivity state is not “online”. This may cause service outage for the devices connected to this FEX",
  baseRemediationText = """1. Run the “show fex” command to get a summary output with the connectivity status per FEX.
                            |2. Execute the show fex [FEX-number [detail]] command to get detailed information about a specific fabric extender or all attached units. Review the FEX state status. A brief explanation for each description is provided below:
                            |• Online - FEX is up and interfaces are online.
                            |• Offline - FEX is down and interfaces are not available.
                            |• Online Sequence - FEX is in the process of coming online.
                            |• Offline Sequence - FEX is in the process of going offline.
                            |• Discovered - FEX has been detected. Online sequence will begin shortly.
                            |• FEX AA Upg Fail - Upgrade of Dual Homed FEX failed.
                            |• AA Version Mismatch - FEX is connected via vPC to two parent switches running different NX-OS versions. vPC can be established but dual home FEX is not possible to work.
                            |• Fex Type Mismatch - Configured FEX type does not match physical FEX inserted.
                            |3. View the status of a FEX and see some relevant to the FEX logs.
                            |4. Review the next online configuration guide for more information: https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/command/reference/rel_4_1/NX5000CommdReference/fex-cmd-ref.pdf
                            |5. Contact with Cisco TAC for further assistant and provide the “show tech-support fex <id>” command for analysis.
                            """.stripMargin
)()

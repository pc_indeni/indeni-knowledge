package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CiscoNexusVpcAutoRecovery(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_vpc_auto_recovery",
  ruleFriendlyName = "Cisco Nexus: vPC auto recovery feature not enabled",
  ruleDescription = "Auto-recovery was included starting with NX-OS 5.2(1). vPC auto-recovery feature was designed to address 2 enhancements to vPC:\nThe first one is to provide a backup mechanism in case of vPC peer-link failure followed by vPC primary peer device failure (vPC auto-recovery feature).\nThe second is to handle a specific case where both vPC peer devices reload but only one comes back to life (vPC auto-recovery reload-delay feature). Indeni will alert if the vpc auto recover feature is not enabled.",
  severity = AlertSeverity.WARN,
  metricName = "nexus-vpc-auto-recovery",
  alertDescription = "The vpc auto recovery feature is not enabled.",
  baseRemediationText = """1. Use the “show running-config vpc all” command to verify that the vPC auto-recovery feature is not enabled.
                          |2. Use the “show vpc” command to check the status of vPC graceful auto-recovery feature and vPC auto-recovery reload-delay timer.
                          |3. Activate vPC auto-recovery by using this command:” Nexus(config-vpc-domain)# auto-recovery”. Both vPC peer devices need to be configured with this command.
                          |4. vPC auto-recovery reload-delay is not enabled by default. Activate it by using this command:” N7K(config-vpc-domain)# auto-recovery reload-delay <240 – 3600 sec>”. The delay can be tuned from 240 seconds to 3600 seconds.  Both vPC peer devices need to be configured with this command. vPC auto-recovery reload-delay deprecates previous feature called vPC reload restore.
                          |5. Cisco strong recommendation is to always enable vPC auto-recovery reload-delay on both vPC peer devices.
                          |6. Cisco strong recommendation is to always enable vPC auto-recovery on both vPC peer devices.""".stripMargin
)()

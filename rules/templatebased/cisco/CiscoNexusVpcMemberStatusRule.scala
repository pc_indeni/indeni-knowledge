package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CiscoNexusVpcMemberStatusRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "CiscoNexusVpcMemberStatusRule",
  ruleFriendlyName = "Cisco Nexus: vPC Members ports down",
  ruleDescription = "A vPC member port is a port-channel member of a vPC.",
  severity = AlertSeverity.ERROR,
  metricName = "nexus-vpc-member-status",
  applicableMetricTag = "name",
  alertItemsHeader = "vPC members",
  alertDescription = "One or more vPC port-channel members are down.",
  baseRemediationText = """1. Check that the configuration of the vPC member ports matches on both vPC peer devices.
                            |2. Run the “show vpc” command and review the vPC status, consistency and reason output.
                            |3. Execute the “show vpc consistency-parameters interface <interface>” command and review the output. If there is an inconsistency (type 1 or type 2), a VLAN or the entire port channel may suspend. This depends on type-1 or type-2 consistency check for the vPC member port.
                            |4. Whenever a vPC VLAN is defined on vPC member port, it must  be defined also on vPC peer-link. Not defining a vPC VLAN on vPC peer-link will make the VLAN not operational.
                            |5. Both sides of the vPC member ports (i.e vPC member port on 7K1 and vPC member port on 7K2) must be of same port type (M1/M2/F1/F2/F2E/F3 or M3).
                            |6. Check if is exceeded the maximum number of active ports bundled in the same vPC member port.
                            |7. It is recommended to use same vPC ID as port-channel ID for ease of configuration, monitoring, and troubleshooting.
                            |8. Refer to the next link for more information: https://www.cisco.com/c/dam/en/us/td/docs/switches/datacenter/sw/design/vpc_design/vpc_best_practices_design_guide.pdf""".stripMargin
)()

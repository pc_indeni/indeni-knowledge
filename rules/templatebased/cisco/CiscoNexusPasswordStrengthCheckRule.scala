package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class CiscoNexusPasswordStrengthCheckRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "CiscoNexusPasswordStrengthCheckRule",
  ruleFriendlyName = "Cisco Nexus: Enforcing Strong Password Selection is not enabled",
  ruleDescription = "Indeni will alert if a Policy-map is not configured to the control-plane.",
  severity = AlertSeverity.WARN,
  metricName = "password-strength-status",
  alertDescription = "The Cisco NX-OS built-in capability to optionally enforce strong password checking when a password is set or entered is not activated",
  baseRemediationText = "This feature is enabled by default and will prevent the selection of a trivial or weak password by " +
    "requiring the password to match the following criteria:\n" +
    "• Is at least eight characters long\n" +
    "• Does not contain many consecutive characters (abcde, lmnopq, etc.)\n" +
    "• Does not contain dictionary words (English dictionary)\n" +
    "• Does not contain many repeating characters (aaabbb, tttttyyyy, etc.)\n" +
    "• Does not contain common proper names (John, Mary, Joe, Cisco, etc.)\n" +
    "• Contains both uppercase and lowercase letters\n" +
    "• Contains numbers\n\n" +
    "Password checking can be enabled by using the “password strength-checking” command. \n" +
    "NOTE: If strong password checking is enabled after passwords have already been set, the system will not retroactively validate any existing password.\n" +
    "For more information, refer to the section of the Cisco NX-OS Security Configuration Guide. \n" +
    "https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/4_1/nx-os/security/configuration/guide/sec_nx-os-cfg/sec_rbac.html\n")()



package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CiscoNexusVpcPeerStatus(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_vpc_peer_status",
  ruleFriendlyName = "Cisco Nexus: vPC peer link down",
  ruleDescription = "The status of vpc remote peer switch via peer-link. Indeni will alert if a vPC peer link is down.",
  severity = AlertSeverity.CRITICAL,
  metricName = "nexus-vpc-peer-status",
  alertDescription = "vPC peer link down.",
  baseRemediationText = """1. Use the “show running-config vpc” command to verify the vPC configuration.
                          |2. Use the “show vpc” command to check the status of vPC.
                          |3. Use the “show vpc peer-keepalive” command to check the status of the vPC peer-keepalive link.
                          |NOTE: When the vPC peer link goes down, the vPC secondary switch shuts down all of its vPC member ports if it can still receive keepalive messages from the vPC primary switch. The vPC primary switch keeps all of its interfaces up. The Dual-Active or Split Brain vPC failure scenario occurs when the Peer Keepalive Link fails followed by the Peer-Link. Under this condition both switches undertake the vPC primary roles. If this happens, the vPC primary switch will remain as the primary and the vPC secondary switch will become operational primary causing severe network instability and outage.
                          |4. Run the “show vpc  statistics peer-link” and check if the counters of the interfaces increase.
                          |5. Run the “show vpc  orphan-ports” to review the status of the orphan ports.
                          |6. Use the “show vpc consistency-parameters” command to verify that both the vPC peers have the identical type-1 parameters.
                          |7. Use the “show port-channel summary” command to verify the members in the port channel are mapped to the vPC.
                          |8. Run the show logging last <value> command to review the logs.
                          |9. Contact CISCO TAC for further assistance.
                          |10. Review and consider to implement the vPC best practices described at the next cisco article https://www.cisco.com/c/dam/en/us/td/docs/switches/datacenter/sw/design/vpc_design/vpc_best_practices_design_guide.pdf""".stripMargin
)()

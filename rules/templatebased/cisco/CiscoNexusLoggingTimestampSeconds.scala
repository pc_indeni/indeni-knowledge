package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CiscoNexusLoggingTimestampSeconds(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_logging_timestamp_seconds",
  ruleFriendlyName = "Cisco Nexus: Logging time stamps have been configured to include second precision",
  ruleDescription = "Indeni will alert when logging time stamps have been configured to include second precision",
  severity = AlertSeverity.WARN,
  metricName = "logging-timestamp-status",
  alertIfDown = false,
  alertDescription = "It is important to implement a correct and consistent logging time-stamp configuration to help ensure that you can accurate correlate logging data. Logging time stamps should be configured to include millisecond precision",
  baseRemediationText = """The configuration of logging time stamps with millisecond precision is provided with the next command “logging timestamp millisecond”.NOTE: Cisco NX-OS logging will automatically time stamp log entries with the date and time in the locally configured time zone of the device.
                           |Refer to the Cisco NX-OS System Management Configuration Guide for more information about logging timestamps: https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/5_x/nx-os/system_management/configuration/guide/sm_nx_os_cg.html
                            """.stripMargin
)()

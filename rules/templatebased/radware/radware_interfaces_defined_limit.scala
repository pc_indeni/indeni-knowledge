package com.indeni.server.rules.library.templatebased.radware

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityTemplateRule

/**
  *
  */
case class radware_interfaces_defined_limit(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "radware_interfaces_defined_limit",
  ruleFriendlyName = "Radware Alteon: Interfaces defined nearing limit",
  ruleDescription = "Alteon devices allow for the configuration of multiple interfaces, up to a limit. indeni will alert prior to the limit.",
  usageMetricName = "interfaces-usage",
  limitMetricName = "interfaces-limit",
  threshold = 80.0,
  alertDescriptionFormat = "There are %.0f interfaces defined where the limit is %.0f.",
  baseRemediationText = "Consider removing certain interfaces.")()

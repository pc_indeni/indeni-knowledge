package com.indeni.server.rules.library.templatebased.radware

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityTemplateRule

/**
  *
  */
case class radware_realserver_groups_defined_limit(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "radware_realserver_groups_defined_limit",
  ruleFriendlyName = "Radware Alteon: Real server groups defined limit approaching",
  ruleDescription = "Alteon devices allow for the configuration of multiple real server groups, up to a limit. indeni will alert prior to configuration reaching the limit.",
  usageMetricName = "real-server-groups-usage",
  limitMetricName = "real-server-groups-limit",
  threshold = 80.0,
  alertDescriptionFormat = "There are %.0f real server groups defined where the limit is %.0f.",
  baseRemediationText = "Consider removing certain real server groups.")()

package com.indeni.server.rules.library.templatebased

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityWithItemsTemplateRule

case class RadwareSnatPoolExhaustion(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "RadwareSnatPoolExhaustion",
  ruleFriendlyName = "Radware Devices: SNAT pool near maximum allocated",
  ruleDescription = "SNAT uses ports to do the translation, and there is a limit to the number of ports one can use concurrently for a given IP address. Indeni will alert when the SNAT pool is nearing its capacity.",
  usageMetricName = "radware-lb-snatpool-usage",
  limitMetricName = "radware-lb-snatpool-limit",
  applicableMetricTag = "name",
  threshold = 80.0,
  alertDescription = "Some SNAT pools are nearing their capacity.", 
  alertItemDescriptionFormat = "The number of ports in use is %.0f where the limit is %.0f.",
  baseRemediationText = "Monitor the pool(s) utilization. Investigate the source of traffic increase and consider adding an IP to the pool if needed.", 
  alertItemsHeader = "Affected device")()

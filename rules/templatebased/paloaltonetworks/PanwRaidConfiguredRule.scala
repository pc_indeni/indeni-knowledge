package com.indeni.server.rules.library.templatebased.paloaltonetworks

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class PanwRaidConfiguredRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "PanwRaidConfiguredRule",
  ruleFriendlyName = "Palo Alto Networks Firewalls: RAID not configured",
  ruleDescription = "Indeni will alert if RAID is not configured on Palo Alto device.",
  metricName = "panw-raid-configured",
  applicableMetricTag = "raid",
  descriptionMetricTag = "diskid-states",
  alertItemsHeader = "The following RAID array has not been configured with a second disk",
  alertDescription = "It is best practice to configure redundant disk arrays on Palo Alto device whenever possible. Indeni will alert if RAID is not configured on a Palo Alto device.",
  baseRemediationText = "It is important to know that a device may have been ordered without redundant disks in the array. It is possible especially on a pa-5000 series firewall to order a firewall with only one disk. This is certainly not recommended if you do not have high-availability configured with another firewall. It will still show that you have a RAID because it uses a RAID configuration although you may only have one disk as a member of that array. Please run \"show system raid\" and \"show system raid detail\" for more information. If you need RAID redundancy on this firewall contact your sales team. Otherwise, you may disable this rule for this device.")()

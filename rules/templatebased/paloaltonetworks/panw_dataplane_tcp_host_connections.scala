package com.indeni.server.rules.library.templatebased.paloaltonetworks

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.paloalto._

/**
  *
  */
case class panw_dataplane_tcp_host_connections(context: RuleContext) extends DataplanePoolUsageRule(context, "TCP host connections", 80.0)

package com.indeni.server.rules.library.templatebased.paloaltonetworks

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.paloalto._

/**
  *
  */
case class panw_dataplane_software_packet_buffer_1(context: RuleContext) extends DataplanePoolUsageRule(context, "software packet buffer 1", 80.0)

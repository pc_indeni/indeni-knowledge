package com.indeni.server.rules.library.templatebased.paloaltonetworks

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

case class panw_panorama_cert_expr(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "panw_panorama_cert_expr",
  ruleFriendlyName = "Palo Alto Networks: Panorama certificate about to expire",
  ruleDescription = "Indeni will alert if a Palo Alto Networks Panorama device is running a version known to have a certificate expiration issue reported in April 2017.",
  metricName = "panw-panos-panorama-cert-expr",
  alertDescription = "In April 2017, Palo Alto Networks announced that Panorama devices may require an immediate upgrade to avoid running into a communication issue with managed firewalls. More information is available at: https://live.paloaltonetworks.com/t5/General-Topics/Panorama-Certificate-Expiration-on-June-16-2017/td-p/150948",
  baseRemediationText = "Update to a software version which is not affected by this issue.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("panw-panos-panorama-cert-expr").asSingle().mostRecent().value().noneable)
)()


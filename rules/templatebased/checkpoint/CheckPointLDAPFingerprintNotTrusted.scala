package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  * Created by Patrik J 2017-11-02.
  */

case class CheckPointLDAPFingerprintNotTrusted(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "CheckPointLDAPFingerprintNotTrusted",
  ruleFriendlyName = "Check Point Firewalls: LDAP fingerprint not trusted",
  ruleDescription = "Alerts when the Active Directory SSL fingerprint does not match the one stored in Check Point. When the fingerprints do not match, the Check Point gateway will loose connection with the domain controller and cannot fetch data such as \"Identity Awareness\".",
  metricName = "ldap-integration-fingerprint-matched",
  applicableMetricTag = "name",
  alertItemsHeader = "Affected LDAP Objects",
  alertDescription = "When an LDAP SSL connection is established from a Check Point gateway to a Active Directory server the certificate fingerprint is stored. If the certificate is updated on the server the fingerprint will not match, and connection to the Active Directory server will be lost. This means that no new identities for \"Identity Awareness\" can be collected.",
  baseRemediationText = """To update the fingerprint: 
                          |1. Open SmartDashboard.
                          |2. Open the relevant LDAP account object.
                          |3. Go to the "Servers" tab.
                          |4. For each host in the list, click "Edit" and go to the "Encryption" tab. Then click "Fetch".
                          |5. Save and push policy.""".stripMargin)()
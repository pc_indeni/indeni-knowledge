package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.ruleengine.expressions.conditions.EndsWithRepetition
import com.indeni.apidata.time.TimeSpan
import com.indeni.server.rules.library.StateDownTemplateRule

case class check_point_signature_update_status_nonvsx(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "check_point_signature_update_status_nonvsx",
  ruleFriendlyName = "Check Point Firewalls: Signature update status",
  ruleDescription = "Indeni has detected that one or more signature databases for software blades are out of date. " +
    "This will happen either if the gateway reports a failure to update, or if the signature version are more than 14 days old. This means that protection against new threats is affected. ",
  metricName = "signature-update-status",
  applicableMetricTag = "blade",
  historyLength = 3,
  alertIfDown = true,
  alertItemsHeader = "Affected blades",
  alertDescription = "Software blade signatures are out of data.\n\n" +
    "This alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/johnathanbrowall\">Johnathan Browall Nordstrom</a>.",
  baseRemediationText = "Contact support to get help in troubleshooting this issue. " +
    "There are also troubleshooting guides that can help to determine why the updates has failed.\n" +
    "Anti-bot and Anti-virus: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk98665\n" +
    "URL Filter: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk35196")()


case class check_point_signature_update_status_vsx(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "check_point_signature_update_status_vsx",
  ruleFriendlyName = "Check Point Firewalls: Signature update status",
  ruleDescription = "Indeni has detected that one or more signature databases for software blades are out of date. " +
    "This will happen either if the gateway reports a failure to update, or if the signature version are more than 14 days old. This means that protection against new threats is affected. ",
  metricName = "signature-update-status",
  applicableMetricTag = "blade",
  descriptionMetricTag = "vs.name",
  historyLength = 3,
  alertIfDown = true,
  alertItemsHeader = "Affected blades",
  alertDescription = "Software blade signatures are out of data.\n\n" +
    "This alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/johnathanbrowall\">Johnathan Browall Nordstrom</a>.",
  baseRemediationText = "Contact support to get help in troubleshooting this issue. " +
    "There are also troubleshooting guides that can help to determine why the updates has failed.\n" +
    "Anti-bot and Anti-virus: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk98665\n" +
    "URL Filter: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk35196")()

package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.server.common.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class clusterxl_pnote_down_non_vsx(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "clusterxl_pnote_down_non_vsx",
  ruleFriendlyName = "Check Point ClusterXL (Non-VSX): Pnote(s) down",
  ruleDescription = "ClusterXL has multiple problem notifications (pnotes) - if any of them fail an alert will be issued.",
  metricName = "clusterxl-pnote-state",
  applicableMetricTag = "name",
  alertItemsHeader = "Problematic Elements",
  alertDescription = "This cluster member is down due to certain elements being in a \"problem state\".\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/gal-vitenberg/83/484/103\">Gal Vitenberg</a>.",
  baseRemediationText = "Review the list of problematic elements and take appropriate action.",
  metaCondition = !DataEquals("vsx", "true"),
  itemSpecificDescription = Seq (
    "(?i).*FIB.*".r -> "The FIB device is responsible for supporting dynamic routing under ClusterXL. Review the firewall logs to ensure traffic with the FIBMGR service is flowing correctly.",

    // Catch-all
    ".*".r -> "Please consult with your technical support provider about this pnote."
  ),
  // Ignore interface active check, we alert about interface count separately
  itemsToIgnore = Set ("Interface Active Check".r))()

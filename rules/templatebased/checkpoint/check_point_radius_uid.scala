package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class check_point_radius_uid(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "check_point_radius_uid",
  ruleFriendlyName = "Check Point Devices: RADIUS server uid is not 0",
  ruleDescription = "When configuring access through RADIUS, it is important to set the uid granted to the user to 0 so they have root access.",
  metricName = "radius-super-user-id",
  alertDescription = "The RADIUS Super User UID is the uid an administrator gets when entering expert mode (after authenticating via RADIUS). If this is not uid 0, then the administrator might have permission problems, preventing some commands from operating correctly.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/johnathanbrowall\">Johnathan Browall Nordstrom</a>.",
  baseRemediationText = "Set the Super User UID to 0. In clish: \"set aaa radius-servers super-user-uid 0\" or via the webUI set it under User Management -> Authentication Servers.",
  complexCondition = RuleNot(RuleEquals(RuleHelper.createComplexStringConstantExpression("0"), SnapshotExpression("radius-super-user-id").asSingle().mostRecent().value().noneable))
)()

package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule
import com.indeni.server.common.data.conditions.{Equals => DataEquals}

/**
  *
  */
case class checkpoint_compare_corexl_cores_enabled_vsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "checkpoint_compare_corexl_cores_enabled_vsx",
  ruleFriendlyName = "Check Point Cluster: CoreXL cores-enabled mismatch across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the number of CoreXL cores enabled are different.",
  metricName = "corexl-cores-enabled",
  applicableMetricTag = "vs.name",
  metaCondition = DataEquals("vsx", "true"),
  isArray = false,
  alertDescription = "The members of a cluster of Check Point firewalls must have the same number of cores enabled.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/gal-vitenberg/83/484/103\">Gal Vitenberg</a>.",
  baseRemediationText = """Compare the output of "fw ctl multik stat" across members of the cluster.""")()


case class checkpoint_compare_corexl_cores_enabled_non_vsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "checkpoint_compare_corexl_cores_enabled_non_vsx",
  ruleFriendlyName = "Check Point Cluster: CoreXL cores-enabled mismatch across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the number of CoreXL cores enabled are different.",
  metricName = "corexl-cores-enabled",
  metaCondition = !DataEquals("vsx", "true"),
  isArray = false,
  alertDescription = "The members of a cluster of Check Point firewalls must have the same number of cores enabled.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/gal-vitenberg/83/484/103\">Gal Vitenberg</a>.",
  baseRemediationText = """Compare the output of "fw ctl multik stat" across members of the cluster.""")()

package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.server.common.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class chkp_no_policy_no_vsx(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "chkp_no_policy_no_vsx",
  ruleFriendlyName = "Check Point Firewalls (Non-VSX): No firewall policy loaded",
  ruleDescription = "indeni will alert when a Check Point firewall is running without a policy.",
  metricName = "policy-installed-fingerprint",
  alertDescription = "It appears the firewall does not have a valid policy. It's possible this is due to \"fw unloadlocal\".",
  baseRemediationText = "Ensure a valid policy is installed.",
  metaCondition = !DataEquals("vsx", "true"),
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression(""), SnapshotExpression("policy-installed-fingerprint").asSingle().mostRecent().value().noneable)
)()

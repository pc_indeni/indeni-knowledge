package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.ruleengine.expressions.conditions.{Equals, Not}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.common.data.conditions.{Equals => DataEquals}
import com.indeni.server.rules.library.{MultiSnapshotValueCheckTemplateRule, RuleHelper}

case class chkp_firewall_routes_missing_novsx(context: RuleContext) extends MultiSnapshotValueCheckTemplateRule(context,
  ruleName = "chkp_firewall_routes_missing_novsx",
  ruleFriendlyName = "Check Point Firewalls (Non-VSX): Routes defined in clish/webUI are missing",
  ruleDescription = "Sometimes the routes that are defined in the Check Point Web UI or through clish may not be fully applied to the operating system layer. If this happens, Indeni will alert.",
  metricName = "routes-missing-kernel",
  applicableMetricTag = "name",
  alertItemsHeader = "Routes missing",
  alertDescription = "The configured routes have not been correctly applied to the Gaia OS. This means that some of the routes configured do not currently work.",
  baseRemediationText = "A workaround to get it to work can be to restart the routeD daemon by running \"cpstop;cpstart\" or restarting the device. However since this should not happen a case can also be opened with your technical support provider. In the case of devices in a cluster it is possible that the issue happens only for one of the nodes and a failover to the other node could lessen the impact of the issue.",
  metaCondition = !DataEquals("vsx", "true"),
  complexCondition = Not(Equals(RuleHelper.createEmptyComplexArrayConstantExpression(), SnapshotExpression("routes-missing-kernel").asMulti().mostRecent().value().noneable))
)()

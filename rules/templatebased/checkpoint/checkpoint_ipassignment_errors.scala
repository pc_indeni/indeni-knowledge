package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class checkpoint_ipassignment_errors(context: RuleContext) extends MultiSnapshotValueCheckTemplateRule(context,
  ruleName = "checkpoint_ipassignment_errors",
  ruleFriendlyName = "Check Point Firewalls: Errors found in $FWDIR/conf/ipassignment.conf",
  ruleDescription = "The ipassignment.conf file is used for remote access VPN configuration. Any errors in the file's contents will be alerted on by indeni.",
  metricName = "ipassignment-conf-errors",
  alertDescription = "Errors in $FWDIR/conf/ipassignment.conf could cause Remote Access VPN clients to not be assigned their static IP address.",
  baseRemediationText = "See https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk105162",
  complexCondition = RuleNot(RuleEquals(RuleHelper.createEmptyComplexArrayConstantExpression(), SnapshotExpression("ipassignment-conf-errors").asMulti().mostRecent().value().noneable)))()

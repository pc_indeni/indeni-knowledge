package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
  *
  */
case class check_point_aggressive_aging_vsx(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "check_point_aggressive_aging_vsx",
  ruleFriendlyName = "Check Point Firewalls (VSX): Aggressive Aging enabled",
  ruleDescription = "Aggressive Aging turning on means the firewall is under an extreme load. If this happens, indeni will alert.",
  metricName = "chkp-agressive-aging",
  applicableMetricTag = "vs.name",
  alertIfDown = false,
  alertItemsHeader = "Affected VS's",
  alertDescription = "Aggressive Aging has started operating on this device.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/johnathanbrowall\">Johnathan Browall Nordstrom</a>.",
  baseRemediationText = "Run \"fw ctl pstat\" for more information. Determine what may be causing the excessive load on the firewall.")()

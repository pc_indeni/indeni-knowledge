package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.server.common.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class check_point_aggressive_aging_novsx(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "check_point_aggressive_aging_novsx",
  ruleFriendlyName = "Check Point Firewalls (Non-VSX): Aggressive Aging enabled",
  ruleDescription = "Aggressive Aging turning on means the firewall is under an extreme load. If this happens, indeni will alert.",
  metricName = "chkp-agressive-aging",
  alertIfDown = false,
  alertDescription = "Aggressive Aging has started operating on this device.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/johnathanbrowall\">Johnathan Browall Nordstrom</a>.",
  baseRemediationText = "Run \"fw ctl pstat\" for more information. Determine what may be causing the excessive load on the firewall.",
  metaCondition = !DataEquals("vsx", "true"))()

package com.indeni.server.rules.library

import com.indeni.ruleengine.expressions.OptionalExpression
import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.{DivExpression, TimesExpression}
import com.indeni.server.common.ParameterValue
import com.indeni.server.common.data.conditions.Equals
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class ConcurrentConnectionsUsageNoVsxRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "High_Threshold_of_Connection_usage"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Concurrent Connection Usage",
    "What is the threshold for the concurrent connection usage for which once it is crossed an alert will be issued.",
    UIType.DOUBLE,
    new ParameterValue((80.0).asInstanceOf[Object])
  )

  override val metadata: RuleMetadata = RuleMetadata.builder("concurrent_connection_limit_novsx", "All Devices: Concurrent connection limit nearing",
    "Indeni will alert the number of connections for a device is too high.", AlertSeverity.ERROR).configParameter(highThresholdParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("concurrent-connections").last
    val threshold: OptionalExpression[Double] = getParameterDouble(highThresholdParameter)
    val limit = TimeSeriesExpression[Double]("concurrent-connections-limit").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), !Equals("vsx", "true")),

      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectTimeSeriesExpression[Double](context.tsDao, Set("concurrent-connections", "concurrent-connections-limit"), denseOnly = false),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        GreaterThanOrEqual(
          actualValue,
          TimesExpression(limit, DivExpression(threshold, ConstantExpression(Some(100.0)))))

      ).withRootInfo(
        getHeadline(),
        scopableStringFormatExpression("This device has a high number of concurrent connections: %.0f (vs limit of %.0f) which is above the threshold of %.0f%%.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/in/motisagey\">Moti Sagey</a>.", actualValue, limit, threshold),
        ConditionalRemediationSteps("Review why this may be happening and consider upgrading the device or redirecting traffic.",
          ConditionalRemediationSteps.VENDOR_CP -> "Consider enabling aggressive aging if it is not yet enabled: <a target=\"blank\" href=\"https://sc1.checkpoint.com/documents/R76/CP_R76_IPS_AdminGuide/12857.htm#o12861\">Aggressive Aging Configurations</a>",
          ConditionalRemediationSteps.VENDOR_PANOS -> "Compare the products and the maximum sessions allowed: <a target=\"_blank\" href=\"https://www.paloaltonetworks.com/products/product-selection\">Compare Firewalls</a>",
          ConditionalRemediationSteps.VENDOR_BLUECOAT ->
            """The number of current connected clients has reached the device limit.
              |1. Login to the device's web interface and click on "Statistics" -> "Advanced" -> "HTTP" -> "Show HTTP Statistics".
              |2. Check  the currently established client connections value and compare it to the maximum acceptable concurrent client connections.
              |3. Limit the number of users or upgrade your license or appliance after consulting with Symantec support.""".stripMargin,
          ConditionalRemediationSteps.VENDOR_JUNIPER ->
            """|1. Each device has a limit for concurrent sessions or connections based on the hardware capacity. Exceeding this limit will cause traffic drops.
               |2. Run the "show security flow session summary" to review the current number of sessions.
               |3. Consider enabling aggressive aging if it is not yet enabled.
               |4. Review the following articles on Juniper TechLibrary for more information: <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/concept/security-session-capacity-device-expanding.html">Expanding Session Capacity by Device</a>
               |<a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/example/session-termination-for-srx-series-controlling-cli.html">Example: Controlling Session Termination for SRX Series Services Gateways</a>
               |5. If the problem persists, contact the Juniper Networks Technical Assistance Center (JTAC).""".stripMargin
        )
      ).asCondition()
    ).withoutInfo()
  }
}


package com.indeni.server.rules.library

import com.indeni.apidata.time.TimeSpan
import com.indeni.ruleengine.expressions.conditions.{And, Equals, GreaterThanOrEqual}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class ConfigChangeOnStandbyMemberRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_config_change_on_standby", "Clustered Devices: Configuration changed on standby member",
    "Generally, making configuration changes to the standby member of a device is not recommended. indeni will trigger an issue if this happens.",
    AlertSeverity.WARN).interval(TimeSpan.fromMinutes(5)).build()

  override def expressionTree: StatusTreeExpression = {
    val configUnsavedValue = TimeSeriesExpression[Double]("config-unsaved").last
    val memberStateValue = TimeSeriesExpression[Double]("cluster-member-active").last
    val configSyncValue = TimeSeriesExpression[Double]("cluster-config-synced").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectTimeSeriesExpression[Double](context.tsDao, Set("config-unsaved", "cluster-member-active", "cluster-config-synced"), denseOnly = false),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        And(
          Equals(configUnsavedValue, ConstantExpression(Some(1.0))),
          Equals(memberStateValue, ConstantExpression(Some(0.0))),
          GreaterThanOrEqual(configSyncValue, ConstantExpression(Some(0.0))))
      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("The configuration has been changed on this device, but it's not the active member of the cluster. Best practices recommend making changes to the active member of a cluster and then syncing to the standby."),
      ConditionalRemediationSteps("Make the configuration changes to the active member of the cluster.",
        ConditionalRemediationSteps.OS_NXOS ->
          """1. Save the configuration by executing the "copy running startup config" command. Note: Network admin role is required to execute this command.
            |2. Check that there are not unsaved configuration changes by running the “show running-config diff” command to the switches.
            |3. Consider creating snapshots of the configuration by utilizing the Checkpoint and Rollback NX-OS features. The NX-OS checkpoint and rollback feature are extremely useful, and a life saver in some cases, when a new configuration change to a production system has caused unwanted effects or was incorrectly made/planned and we need to immediately return to an original/stable configuration.
            |4. For more information review the following article: <a target="_blank" href="http://www.firewall.cx/cisco-technical-knowledgebase/cisco-data-center/1202-cisco-nexus-checkpoint-rollback-feature.html">Guide to Nexus checkpoint & rollback feature</a>""".stripMargin,
        ConditionalRemediationSteps.VENDOR_JUNIPER ->
          """|1. The chassis cluster synchronization feature automatically synchronizes the configuration from the primary node to the secondary node when the secondary joins the primary as a cluster.
             |2. Review the following article on Juniper tech support site: <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/concept/chassis-cluster-backup-config-sync.html">Understanding Automatic Chassis Cluster Synchronization Between Primary and Secondary Nodes</a>""".stripMargin
      )
    )
  }
}

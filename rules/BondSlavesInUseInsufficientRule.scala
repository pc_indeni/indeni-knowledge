package com.indeni.server.rules.library

import com.indeni.ruleengine.expressions.conditions.GreaterThan
import com.indeni.ruleengine.expressions.core.{ConstantExpression, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules._
import com.indeni.server.rules.library.BondSlavesInUseInsufficientRule.NAME
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class BondSlavesInUseInsufficientRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder(NAME, "All Devices: Bond Operating Without Sufficient Slave Interfaces",
    "indeni will trigger an issue if a bond interface is operating without sufficient slave interfaces.", AlertSeverity.ERROR).build()

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("bond-slaves-in-use").last
    val requiredValue = TimeSeriesExpression[Double]("bond-slaves-required").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("bond-slaves-in-use", "bond-slaves-required")),

            StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("bond-slaves-in-use", "bond-slaves-required"), denseOnly = false),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              GreaterThan(
                requiredValue,
                inUseValue)

              // The Alert Item to add for this specific item
            ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"name\")}"),
                scopableStringFormatExpression("%.0f slave interfaces are required, while only %.0f are up", requiredValue, inUseValue),
                title = "Bond Interfaces Affected"
            ).asCondition()
        ).withoutInfo().asCondition()
    ).withRootInfo(
        ConstantExpression("Insufficient slave interfaces up for bond interface"),
        ConstantExpression("Bond interfaces may require a certain number of slave interfaces to be up and running. If a bond interface has insufficient slave interfaces an issue may ensue."),
        ConditionalRemediationSteps("Determine why the slave interfaces are down and resolve the issue.",
          ConditionalRemediationSteps.VENDOR_CP -> "Consider reading sk69180.")
    )
  }
}

object BondSlavesInUseInsufficientRule {

  /* --- Constants --- */

  private[library] val NAME = "cross_vendor_bond_slave_in_use_insufficient"
}

package com.indeni.server.rules.library

import com.indeni.apidata.time.TimeSpan
import com.indeni.apidata.time.TimeSpan.TimePeriod
import com.indeni.ruleengine.expressions.data.{SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.rules.library.ThresholdDirection.ThresholdDirection
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

import scala.util.matching.Regex

class TimeThresholdOnDoubleMetricWithItemsTemplateRule(context: RuleContext,
                                                       ruleName: String,
                                                       ruleFriendlyName: String,
                                                       ruleDescription: String,
                                                       severity: AlertSeverity = AlertSeverity.ERROR,
                                                       metricName: String,
                                                       threshold: TimeSpan,
                                                       metricUnits: TimePeriod,
                                                       metaCondition: TagsStoreCondition = True,
                                                       applicableMetricTag: String,
                                                       alertItemDescriptionFormat: String,
                                                       alertItemDescriptionUnits: TimePeriod,
                                                       alertDescription: String,
                                                       baseRemediationText: String,
                                                       thresholdDirection: ThresholdDirection = ThresholdDirection.ABOVE,
                                                       alertItemsHeader: String,
                                                       itemsToIgnore: Set[Regex] = Set("^$".r),
                                                       itemSpecificDescription: Seq[(Regex, String)] = Seq(".*".r -> ""))(vendorToRemediationText: (String, String)*)
  extends NumericThresholdWithItemsTemplateRule[TimeSpan](
    context,
    ruleName,
    ruleFriendlyName,
    ruleDescription,
    severity,
    TimeSeriesExpression[Double](metricName).last.toTimeSpan(metricUnits),
    context.tsDao,
    SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), denseOnly = false),
    metricName,
    threshold,
    metaCondition,
    applicableMetricTag,
    alertItemDescriptionFormat,
    value => value.toDouble(alertItemDescriptionUnits),
    alertDescription,
    baseRemediationText,
    thresholdDirection,
    alertItemsHeader,
    itemsToIgnore,
    itemSpecificDescription
  )(vendorToRemediationText: _*) {}

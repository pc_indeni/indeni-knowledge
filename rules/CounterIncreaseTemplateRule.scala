package com.indeni.server.rules.library

import com.indeni.ruleengine.Scope.{Scope, ScopeValueHelper}
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions._
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.server.common.data.conditions
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

import scala.util.matching.Regex

class CounterIncreaseTemplateRule(context: RuleContext, ruleName: String, ruleFriendlyName: String, ruleDescription: String,
                                       severity: AlertSeverity = AlertSeverity.ERROR,
                                       metricName: String, applicableMetricTag: String, alertDescription: String, alertRemediationSteps: String, alertItemsHeader: String, itemSpecificDescription: Seq[(Regex, String)] = Seq(".*".r -> ""))(vendorToRemediationText: (String, String)*) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder(ruleName, ruleFriendlyName, ruleDescription, severity).build()

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double](metricName).last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), conditions.True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set(applicableMetricTag), withTagsCondition(metricName)),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName)),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          GreaterThan(
            actualValue,
            ConstantExpression(Some(0.0)))

          // The Alert Item to add for this specific item
          ).withSecondaryInfo(
            scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}"),
            new ScopableExpression[String] {
              override protected def evalWithScope(time: Long, scope: Scope): String = {
                val metricTagValue = scope.getVisible(applicableMetricTag).get.toString
                itemSpecificDescription.collectFirst {
                  case item if (!item._1.findFirstMatchIn(metricTagValue).isEmpty) => item._2
                }.get
              }

              override def args: Set[Expression[_]] = Set()
            },
            title = alertItemsHeader
        ).asCondition()
      ).withRootInfo(
          getHeadline(),
          ConstantExpression(alertDescription),
          ConditionalRemediationSteps(alertRemediationSteps, vendorToRemediationText:_*)
      ).asCondition()
    ).withoutInfo()
  }
}

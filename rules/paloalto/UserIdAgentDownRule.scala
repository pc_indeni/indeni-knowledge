package com.indeni.server.rules.library.paloalto

import com.indeni.ruleengine.expressions.conditions.{And, Equals}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.rules.library.RuleHelper
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class UserIdAgentDownRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("panw_user-id_agent_down", "Palo Alto Networks Firewalls: User-ID agent(s) down",
    "If the active member of a cluster has one or more User-ID agents down, Indeni will alert.", AlertSeverity.ERROR).build()

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("userid_agent_state").last
    val activeMemberValue = TimeSeriesExpression[Double]("cluster-member-active").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      And(
          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("cluster-member-active"), denseOnly = false),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            Equals(activeMemberValue, ConstantExpression[Option[Double]](Some(1.0)))
          ).withoutInfo().asCondition(),
          StatusTreeExpression(

            // The additional tags we care about (we'll be including this in alert data)
            SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("userid_agent_state")),

              StatusTreeExpression(
                // The time-series we check the test condition against:
                SelectTimeSeriesExpression[Double](context.tsDao, Set("userid_agent_state"), denseOnly = false),

                // The condition which, if true, we have an issue. Checked against the time-series we've collected
                Equals(
                  inUseValue,
                  ConstantExpression[Option[Double]](Some(0.0)))

                // The Alert Item to add for this specific item
              ).withSecondaryInfo(
                  scopableStringFormatExpression("${scope(\"name\")}"),
                  scopableStringFormatExpression("The User-ID agent is not responsive.", inUseValue),
                  title = "User-ID Agents"
              ).asCondition()
          ).withoutInfo().asCondition())

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("One or more User-ID agents are down."),
        ConstantExpression("""A User-ID Agent being down may cause improper User-ID mappings to your firewall traffic and URL logs for example. Not having a proper User-ID mapping may even cause failure to access resources because they cannot be identified as a member of a group in a user/group based policy.
                             |How to Troubleshoot User-ID Agent Problems:
                             |https://www.paloaltonetworks.com/documentation/80/pan-os/pan-os/user-id/verify-the-user-id-configuration.html
                             |https://www.paloaltonetworks.com/documentation/80/pan-os/pan-os/user-id/user-id-concepts""".stripMargin
    )
  )
  }
}

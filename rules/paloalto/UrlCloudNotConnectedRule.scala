package com.indeni.server.rules.library.paloalto

import com.indeni.ruleengine.expressions.conditions.{And, Equals}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.rules.library.RuleHelper
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class UrlCloudNotConnectedRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("panw_url_cloud_not_connected", "Palo Alto Networks Firewalls: URL cloud not connected",
    "If the active member of a cluster loses connectivity to the URL cloud, indeni will alert.", AlertSeverity.ERROR).build()

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("url-filtering-cloud-connected").last
    val activeMemberValue = TimeSeriesExpression[Double]("cluster-member-active").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("url-filtering-cloud-connected", "cluster-member-active"), denseOnly = false),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            And(
              Equals(inUseValue, ConstantExpression[Option[Double]](Some(0.0))),
              Equals(activeMemberValue, ConstantExpression[Option[Double]](Some(1.0)))
            )
          ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("This device cannot connect to the URL cloud."),
        ConstantExpression("""Failure to update URL’s when the cloud connection is down could cause a failure to block newly discovered phishing or malware URL’s and pass recently recategorized URLs. If this issue persists it may indicate a cloud failure, DNS issue, or other access issue between the firewall and the cloud resources.
                              |Troubleshooting: https://www.paloaltonetworks.com/documentation/80/pan-os/pan-os/url-filtering/troubleshoot-url-filtering/pan-db-cloud-connectivity-issues""".stripMargin)
    )
  }
}

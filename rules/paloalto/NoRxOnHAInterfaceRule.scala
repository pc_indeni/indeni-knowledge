package com.indeni.server.rules.library.paloalto

import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules._
import com.indeni.server.rules.library.RuleHelper
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class NoRxOnHAInterfaceRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("panw_ha_no_rx", "Palo Alto Networks Firewalls: HA interfaces not receiving traffic",
    "A Palo Alto Networks firewall can have up to three HA interfaces. They should all be receiving traffic at all times. If one stops receiving traffic, Indeni will alert.", AlertSeverity.ERROR).build()

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("network-interface-rx-packets").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name", "high-availability-interface"), True),

          StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("network-interface-rx-packets"), denseOnly = false),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              com.indeni.ruleengine.expressions.conditions.Equals(actualValue, ConstantExpression(Some(0.0)))

              // The Alert Item to add for this specific item
          ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"name\")}"),
                EMPTY_STRING,
                title = "Affected HA Interfaces"
            ).asCondition()
        ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("All high-availability interfaces must be receiving traffic at all times. HA1 should be receiving hello/alive pings, HA2 is used for state/session synchronization and HA3 for active/active traffic forwarding. For more information see https://www.paloaltonetworks.com/documentation/80/pan-os/pan-os/high-availability/ha-concepts ."),
        ConstantExpression("""No receive traffic on an HA interface can indicate failed connectivity between devices although the interfaces may be UP.
                              |A split-brain scenario can occur if HA interfaces are unable to communicate. This makes it difficult to keep one firewall active and may cause traffic interruption.
                              |Keep-alives are recommended to test regular flow.
                              |No receive on HA1 is critical error state.
                              |No receive on HA2 is warning state.
                              |No receive on HA3 is warning state only if in active/active HA.
                              |See HA1, HA2, or HA3 purposes and configuration options under: https://www.paloaltonetworks.com/documentation/71/pan-os/web-interface-help/device/device-high-availability and search https://live.paloaltonetworks.com for your specific HA interface to troubleshoot.""".stripMargin)
    )
  }
}

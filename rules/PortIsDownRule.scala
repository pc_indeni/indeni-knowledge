package com.indeni.server.rules.library

import com.indeni.ruleengine.expressions.conditions.{And, Equals}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class PortIsDownRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_network_port_down", "All Devices: Network port(s) down",
    "Indeni will trigger an issue if one or more network ports is down.", AlertSeverity.CRITICAL).build()

  override def expressionTree: StatusTreeExpression = {

    val actualValue = TimeSeriesExpression[Double]("network-interface-state").last
    val adminValue = TimeSeriesExpression[Double]("network-interface-admin-state").last
    val deviceIsPassive = TimeSeriesExpression[Double]("device-is-passive").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("network-interface-state")),

        And(
          StatusTreeExpression(
            SelectTimeSeriesExpression[Double](context.tsDao, Set("network-interface-state"), denseOnly = false),
            Equals(ConstantExpression[Option[Double]](Some(0)), actualValue)
          ).withoutInfo().asCondition(),
          StatusTreeExpression(
            SelectTimeSeriesExpression[Double](context.tsDao, Set("network-interface-admin-state"), denseOnly = false),
            adminValue.notEquals(0.0)
          ).withoutInfo().asCondition().orElse(Some(true)),
          StatusTreeExpression(
            SelectTimeSeriesExpression[Double](context.tsDao, Set("device-is-passive"), denseOnly = false),
            deviceIsPassive.notEquals(1.0)
          ).withoutInfo().asCondition().orElse(Some(true))
        )
      ).withSecondaryInfo(
        scopableStringFormatExpression("${scope(\"name\")}"),
        EMPTY_STRING,
        title = "Ports Affected"
      ).asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more ports are down."),
      ConditionalRemediationSteps("Review the cause for the ports being down.",
        ConditionalRemediationSteps.OS_NXOS ->
          """|
            |1. Check the physical media to ensure that there are no damaged parts.
            |2. Verify that the SFP (small form-factor pluggable) devices in use are those authorized by Cisco and that they are not faulty by executing the "show interface transceiver" NX-OS command.
            |3. Verify that you have enabled the port by using the "no shutdown" NX-OS command.
            |4. Use the "show interface" command to verify the state of the interface. Besides, you can use the "show interface counters" command to check port counters.
            |5. Check if the port is configured in dedicated mode.
            |6. Execute the following NX-OS commands to gather more information about ports:
            | a. "show interface status"
            | b. "show interface capabilities"
            | c. "show udld"
            | d. "show tech-support udld"
            |
            |7. For more information review: <a target="_blank" href="https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus9000/sw/7-x/troubleshooting/guide/b_Cisco_Nexus_9000_Series_NX-OS_Troubleshooting_Guide_7x/b_Cisco_Nexus_9000_Series_NX-OS_Troubleshooting_Guide_7x_chapter_0101.pdf">Nexus Troubleshooting Guide</a> """.stripMargin,
        ConditionalRemediationSteps.VENDOR_JUNIPER ->
          """|
            |1. On the device command line interface run "show interfaces extensive" command to check the status of the interface.
            |2. Execute "show configuration interface" command to check interface configuration.
            |3. Check the encapsulation type and physical media on the port.
            |4. Check the port specification and the fiber cable.
            |5. Review the following article on Juniper TechLibrary for more information: <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/reference/command-summary/show-interfaces-security.html#jd0e1772">Operational Commands: show interfaces (SRX Series)</a>.""".stripMargin,
        ConditionalRemediationSteps.VENDOR_FORTINET ->
          """|
            |1. Monitor hardware network operations (e.g. speed, duplex settings) by using the "diag hardware deviceinfo nic <interface>" FortiOS command.
            |2. Run the command "diag hardware deviceinfo nic <interface>" command to display a list of hardware related names and values. Review the next link for more details: http://help.fortinet.com/fos50hlp/54/Content/FortiOS/fortigate-toubleshooting-54/troubleshooting_tools.htm
            |3. Run the hidden FortiOS command "fnsysctl cat /proc/net/dev" to get a summary of  the interface statistics.
            |4. Check for a mismatch in the speed and duplex interface settings on two sides of a cable, or for a damaged cable / SFP. Try to manually configure both sides to the same speed/duplex mode when you can. For more information, review "Symptoms of Ethernet speed/duplex mismatches" at http://kb.fortinet.com/kb/documentLink.do?externalID=10653
            |5. Review the log history for interfaces status changes.
            |6. Review the interface configuration. For more information, use the following interface configuration guide: http://help.fortinet.com/fos50hlp/52data/Content/FortiOS/fortigate-system-administration-52/Interfaces/interfaces.htm""".stripMargin
      )
    )
  }
}




package com.indeni.server.rules.library

import com.indeni.ruleengine.expressions.conditions.{And, EndsWithRepetition, Equals}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class ClusterConfigNotSyncedRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cluster_config_unsynced", "Clustered Devices: Cluster configuration not synced",
    "For devices that support full configuration synchronization, indeni will trigger an issue if the configuration is out of sync.", AlertSeverity.ERROR).build()

  override def expressionTree: StatusTreeExpression = {
    val tsToTestAgainst = TimeSeriesExpression[Double]("cluster-config-synced")
    val activeMemberValue = TimeSeriesExpression[Double]("cluster-member-active").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectTimeSeriesExpression[Double](context.tsDao, Set("cluster-config-synced", "cluster-member-active"), denseOnly = false),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        And(
          EndsWithRepetition(tsToTestAgainst, ConstantExpression(0.0), 3),
          Equals(activeMemberValue, ConstantExpression[Option[Double]](Some(1.0)))
        )
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("The configuration has been changed on this device, but has not yet been synced to other members of the cluster. This may result in an unexpected behavior of other cluster members should this member go down."),
      ConditionalRemediationSteps("Log into the device and synchronize the configuration across the cluster.",
        ConditionalRemediationSteps.OS_NXOS ->
          """|1. Login to the device to review the FHRP configuration across the vPC cluster if it is configured.
             |2. Execute the "show hsrp brief" command to check the HSRP state and configuration to the cluster.
             |3. Execute the “show vrrp detail” command to check the VRRP state and configuration to the cluster.
             |4. Log into the device and synchronize the configuration across the vPC peer switches by reviewing  the “show run vpc” command output from both peers.
             |5. Execute the “show vpc consistency-parameters” command and review the output.  Ensure that type 1 & 2 vPC consistency parameters match. If they do not match, then vPC is suspended. Items that are type 2 do not have to match on both Nexus 5000 switches for the vPC to be operational.
             |6. Check that there are not unsaved configuration changes by running the “show running-config diff” NX-OS command.
             |7. Log into both peers and save the configuration with the "copy running-config startup-config" NX-OS command.""".stripMargin,
        ConditionalRemediationSteps.VENDOR_JUNIPER ->
          """|1. Run "show chassis cluster information configuration-synchronization" command to review configuration synchronization status of a chassis cluster (Junos OS Release 12.1X47-D10 or later).
             |2. Check the activation and last sync status if these options are enabled.
             |3. Check the link connectivity.
             |4. Check the cluster configuration for synchronization.
             |5. Review this article on Juniper TechLibrary: <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/reference/command-summary/show-chassis-cluster-information-detail-config-sync.html">Operational Commands</a>
             |6. Contact Juniper Networks Technical Assistance Center (JTAC) if further assistance is required.""".stripMargin
      )
    )
  }
}

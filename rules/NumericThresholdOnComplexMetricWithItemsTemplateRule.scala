package com.indeni.server.rules.library

import com.indeni.ruleengine.expressions.casting.ToDoubleExpression
import com.indeni.ruleengine.expressions.data.{SelectSnapshotsExpression, SingleSnapshotExtractExpression, SnapshotExpression}
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.ThresholdDirection.ThresholdDirection
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

import scala.util.matching.Regex

class NumericThresholdOnComplexMetricWithItemsTemplateRule(context: RuleContext,
                                                           ruleName: String,
                                                           ruleFriendlyName: String,
                                                           ruleDescription: String,
                                                           severity: AlertSeverity = AlertSeverity.ERROR,
                                                           metricName: String,
                                                           threshold: Double,
                                                           metaCondition: TagsStoreCondition = True,
                                                           applicableMetricTag: String,
                                                           alertItemDescriptionFormat: String,
                                                           alertDescription: String,
                                                           baseRemediationText: String,
                                                           thresholdDirection: ThresholdDirection = ThresholdDirection.ABOVE,
                                                           alertItemsHeader: String,
                                                           itemsToIgnore: Set[Regex] = Set("^$".r),
                                                           itemSpecificDescription: Seq[(Regex, String)] = Seq(".*".r -> ""))(vendorToRemediationText: (String, String)*)
  extends NumericThresholdWithItemsTemplateRule[Double](
    context,
    ruleName,
    ruleFriendlyName,
    ruleDescription,
    severity,
    ToDoubleExpression(
      SingleSnapshotExtractExpression(SnapshotExpression(metricName).asSingle().mostRecent().value(), "value")),
    context.snapshotsDao,
    SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).single(),
    metricName,
    threshold,
    metaCondition,
    applicableMetricTag,
    alertItemDescriptionFormat,
    identity => identity,
    alertDescription,
    baseRemediationText,
    thresholdDirection,
    alertItemsHeader,
    itemsToIgnore,
    itemSpecificDescription
  )(vendorToRemediationText: _*) {}

package com.indeni.server.rules.library.core

import com.indeni.ruleengine.Status
import com.indeni.ruleengine.expressions.core.{ForExpression, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data.SelectTagsExpression
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.{AvailableDevicesFilterExpression, EnabledAlertFilterExpression, RuleConfigurationExpression, RuleSpecificDisabledDevicesExpression}
import com.indeni.server.rules.library.core.PerAllowedDeviceRule.withOptionalVendorAndOs

/**
  * A per-device rule which wraps another per-device rule and adds a filtering level on the devices, which filters
  * out devices that are under maintenance, on a black list, has a licence limitation or were explicitly configured
  * not to be monitored.
  *
  * @param rule The wrapped rule.
  */
case class PerAllowedDeviceRule(rule: PerDeviceRule, context: RuleContext) extends PerDeviceRule {

  /* --- Methods --- */

  /* --- Public Methods --- */

  override def metadata: RuleMetadata = rule.metadata

  override def expressionTree: StatusTreeExpression = {

    val originalTree = rule.expressionTree

    val selectDeviceAndVendor = withOptionalVendorAndOs(originalTree).statusesEvaluator.collection

    val onlyMonitoredDevices = AvailableDevicesFilterExpression(context.deviceService, selectDeviceAndVendor)

    val onlyAvailableDevices = RuleSpecificDisabledDevicesExpression(rule.metadata.name, context.rulesDisableDao, onlyMonitoredDevices)

    val withConfiguration = RuleConfigurationExpression(metadata.name, context.configurationSetDao, onlyAvailableDevices)

    val onlyEnabled = EnabledAlertFilterExpression(rule.metadata, withConfiguration)

    StatusTreeExpression(ForExpression[Status](onlyEnabled, originalTree.statusesEvaluator.evaluator))
  }
}

object PerAllowedDeviceRule {

  /* --- Methods --- */

  /* --- Private Methods --- */

  private[library] def withOptionalVendorAndOs(expressionTree: StatusTreeExpression): StatusTreeExpression = {

    val forDevice = expressionTree.statusesEvaluator
    val selectDevices = forDevice.collection.asInstanceOf[SelectTagsExpression]
    val selectDevicesWithVendorAndOs = selectDevices.copy(optionalKeys = selectDevices.optionalKeys ++ Set(Vendor, OsKey))

    StatusTreeExpression(ForExpression[Status](selectDevicesWithVendorAndOs, forDevice.evaluator))
  }
}

/**
  * A rule that its top/root expression is a for-expression over devices.
  */
trait PerDeviceRule extends Rule {

  /* --- Methods --- */

  /* --- Public Methods --- */

  override def expressionTree: StatusTreeExpression
}

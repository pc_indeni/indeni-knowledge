package com.indeni.server.rules.library.checkpoint

import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.AverageExpression
import com.indeni.server.common.data.conditions.Equals
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.RuleHelper
import com.indeni.server.rules.library.checkpoint.KernelMemoryHighNoVsxRule.NAME
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class KernelMemoryHighNoVsxRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_Kernel_Memory_usage"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Kernel Memory Usage",
    "What is the threshold for the kernel memory usage for which once it is crossed an alert will be issued.",
    UIType.DOUBLE,
    80.0)

  override val metadata: RuleMetadata = RuleMetadata.builder(NAME, "Check Point Firewalls (Non-VSX): Firewall kernel memory usage high",
    "Check Point firewalls have a memory segment dedicated to the kernel. If the kernel memory is nearing its limit, an alert will be issued.", AlertSeverity.ERROR).configParameter(highThresholdParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = AverageExpression(TimeSeriesExpression[Double]("kernel-memory-usage"))
    val thresholdValue = getParameterDouble(highThresholdParameter)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), !Equals("vsx", "true")),

          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("kernel-memory-usage")),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            GreaterThanOrEqual(
              inUseValue,
              thresholdValue)

            // The Alert Item to add for this specific item
          ).withRootInfo(
              getHeadline(),
              scopableStringFormatExpression("The firewall kernel memory in use is %.0f%% where the threshold is %.0f%%.", inUseValue, thresholdValue),
              ConstantExpression("Determine why the firewall kernel memory is high and resolve the issue.")
        ).asCondition()
    ).withoutInfo()
  }
}

object KernelMemoryHighNoVsxRule {

  /* --- Constants --- */

  private[checkpoint] val NAME = "chkp_firewall_kernel_memory_no_vsx"
}

package com.indeni.server.rules.library.checkpoint

import com.indeni.ruleengine.Scope.{Scope, ScopeValueHelper}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{MultiSnapshotExtractScalarExpression, SelectSnapshotsExpression, SelectTagsExpression, SnapshotExpression}
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.utility.IsEmptyExpression.IsEmptyExpressionHelper
import com.indeni.ruleengine.expressions.utility._
import com.indeni.ruleengine.expressions.{Expression, conditions}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.rules.library.RuleHelper
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.automationpolicy.AutomationPolicyItemAlertSetting
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


/**
  * Created by amir on 19/02/2017.
  * Updated for wild card resolution by tomas on 2018 01 20.
  */
case class CheckPointConfigurationComplianceCheckRule(context: RuleContext) extends
  PerDeviceRule with RuleHelper {

  private val linesParameterName = "config_lines_to_look_for"
  private val linesParameter = new ParameterDefinition(
    linesParameterName,
    "",
    "Configuration Lines Needed (multi-line)",
    "List the configuration commands that need to show in the output of \"show configuration\".",
    UIType.MULTILINE_TEXT,
    "")

  override def expressionTree: StatusTreeExpression = {
    val linesExpected = DynamicParameterExpression.withConstantDefault(linesParameter.getName, Seq[String]()).withLazy
    val snapshotKey = "configuration-content"
    val missingResolved = new SeqDiffWithoutOrderExpression[String](
      MultiSnapshotExtractScalarExpression(
        SnapshotExpression(snapshotKey).asMulti().mostRecent().value(), "line"), linesExpected) {

      // Modification for a wildcard "*" resolution.
      override def eval(time: Long): SeqDiff[String] = {
        val actualSet = actual.eval(time).toSet
        val expectedSetResolved = expected.eval(time).toSet.map { x: String => x.replace("*", "\\S+").r.anchored }
        val redundant = Set[String]()
        val missingChecked = {
          for {
            i <- expectedSetResolved
            j <- actualSet
          } yield i.findFirstIn(j) match {
            case Some(_) => Some(i)
            case _ => None
          }
        }.flatten.map(_.toString())
        val expectedSet = expectedSetResolved.map(_.toString())
        val missing = (expectedSetResolved.map(_.toString()) diff missingChecked).map(_.replace("\\S+", "*"))
        SeqDiff(expectedSet, missing, redundant)
      }
    }.missings.withLazy

    val missingLineHeadline = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String = scope.getVisible(CheckPointConfigurationComplianceCheckRule.CheckPointConfigurationTag)
        .get.toString

      override def args: Set[Expression[_]] = Set()
    }

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The snapshot we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set(snapshotKey)).multi(),

        // The condition which, if true, we have an issue. Checked against the snapshots we've collected
        missingResolved.nonEmpty,

        multiInformers =
          Set(
            MultiIssueInformer(
              missingLineHeadline,
              EMPTY_STRING,
              "Missing lines"
            ).iterateOver(collection = missingResolved,
              scopeKey = CheckPointConfigurationComplianceCheckRule.CheckPointConfigurationTag,
              condition = conditions.True)
          )

        // Details of the alert itself
      ).withRootInfo(
        getHeadline(),
        ConstantExpression("Some configuration lines that should be defined are not:\n" +
          "Indeni has found that some lines are missing. These are listed below."),
        ConstantExpression("Modify the device's configuration as required.")
      ).asCondition()
    ).withoutInfo()
  }

  /**
    * @return The rule's metadata.
    */
  override def metadata: RuleMetadata =
    RuleMetadata.builder(
      "CheckPointConfigurationComplianceCheckRule",
      "Compliance Check: Configuration Mismatch (Check Point Gaia)",
      "Indeni can verify that certain lines appear in the \"show configuration\" output for Check Point Gaia." +
        " The required configuration lines need to be entered in Indeni Rules > Configuration tab. " +
        " \"*\" can be used as a wildcard for a single word or numeric value. ",
      AlertSeverity.ERROR).defaultAction(
      AutomationPolicyItemAlertSetting.NEVER)
      .configParameter(linesParameter).build()
}



object CheckPointConfigurationComplianceCheckRule {
  val CheckPointConfigurationTag = "CPConfigurationTag"
}

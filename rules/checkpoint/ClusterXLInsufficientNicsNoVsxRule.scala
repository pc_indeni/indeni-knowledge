package com.indeni.server.rules.library.checkpoint

import com.indeni.server.common.data.conditions.Equals
import com.indeni.server.rules._
import com.indeni.server.rules.library._

case class ClusterXLInsufficientNicsNoVsxRule(context: RuleContext)
  extends NearingCapacityTemplateRule(
    context,
    ruleName = "clusterxl_insufficient_nics_novsx",
    ruleFriendlyName = "Check Point ClusterXL (Non-VSX): Required interface(s) down",
    ruleDescription =
      "iClusterXL requires a certain number of interfaces to be up for the member to be considered OK.",
    usageMetricName = "cphaprob-up-interfaces",
    limitMetricName = "cphaprob-required-interfaces",
    threshold = 100.0,
    thresholdDirection = ThresholdDirection.BELOW,
    alertDescriptionFormat = "Only %.0f interfaces are up, while %.0f interfaces are required.",
    baseRemediationText = "Determine why the interfaces are down and resolve the issue.",
    metaCondition = !Equals("vsx", "true"))()

BUILD_PATH=build

prepare:
	mkdir -p ${BUILD_PATH}
	mkdir -p ${BUILD_PATH}/kb
	
clean:
	rm -f ${BUILD_PATH}/indeni-knowledge.jar
	rm -rf ${BUILD_PATH}/kb

jar: prepare
	cp -r parsers/ ${BUILD_PATH}/kb/parsers
	jar cvf ${BUILD_PATH}/indeni-knowledge.jar -C ${BUILD_PATH} kb
#! META
name: panos-show-neighbor-all
description: fetch the neighbor data
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall

#! COMMENTS
neighbor-discovery-total-entries:
    why: |
        A network device which forwards traffic needs to know the MAC addresses of devices it is directly connected to, so it can send traffic on layer 2. With IPv6, it uses neighbor discovery (ND) requests. The ND replys are stored in a cache which allows the device to avoid doing ND requests again and again for the same destination IP. The ND cache has a finite size to avoid using up all of the available memory. If the ND cache fills up with entries, some traffic may be dropped or drastically slowed down.
    how: |
        This alert uses the Palo Alto Networks API to retrieve the current utilization of the ND cache - number of entries in it vs the total limit.
    without-indeni: |
        An administrator could write a script to leverage the Palo Alto Networks API to collect this data periodically and alert appropriately. Alternatively, wait for an issue to occur and check the ND cache status by running "show neighbor all".
    can-with-snmp: false
    can-with-syslog: false
neighbor-discovery-limit:
    skip-documentation: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><neighbor><interface>all<%2Finterface><%2Fneighbor><%2Fshow>&key=${api-key}
protocol: HTTPS

#! PARSER::XML
_vars:
    root: /response/result
_metrics:
    -
        _value.double:
            _text: ${root}/total
        _tags:
            "im.name":
                _constant: "neighbor-discovery-total-entries"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Neighbor Discovery Cache - Current Entries"
            "im.dstype.displayType":
                _constant: "number"
    -
        _value.double:
            _text: ${root}/max
        _tags:
            "im.name":
                _constant: "neighbor-discovery-limit"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Neighbor Discovery Cache - Limit"
            "im.dstype.displayType":
                _constant: "number"
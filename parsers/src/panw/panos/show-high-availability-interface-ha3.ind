#! META
name: panos-show-high-availability-interface-ha3
description: Fetch HA3 interface stats
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: "paloaltonetworks"
    os.name: "panos"
    high-availability: "true"

#! COMMENTS
network-interface-admin-state:
    why: |
        If a network interface is set to be up (what's known as "admin up") but is actually down (a cable is not connected, the device on the other side is down, etc.) it is important to know.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for interfaces that are set to be up, but are actually down.
    without-indeni: |
        The status of network interfaces can be retrieved through SNMP.
    can-with-snmp: true
    can-with-syslog: true
network-interface-speed:
    why: |
        Generally, these days network interfaces are set at 1Gbps or more. Sometimes, due to a configuration or device issue, an interface can be set below that (to 100mbps or even 10mbps). As that is usually _not_ the intended behavior, it is important to track the speed of all network interfaces.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the actual runtime speed of each interface.
    without-indeni: |
        The speed of network interfaces can be retrieved through SNMP.
    can-with-snmp: true
    can-with-syslog: true
network-interface-duplex:
    why: |
        Generally, these days network interfaces are set at full duplex. Sometimes, due to a configuration or device issue, an interface can be set to half duplex. As that is usually _not_ the intended behavior, it is important to track the duplex setting of all network interfaces.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the actual runtime duplex of each interface.
    without-indeni: |
        The duplex of network interfaces can be retrieved through SNMP.
    can-with-snmp: true
    can-with-syslog: true
network-interface-tx-packets:
    why: |
        Tracking the number of packets flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of packets transmitted through the interface.
    without-indeni: |
        The traffic statistics of network interfaces can be retrieved through SNMP in newer versions of PAN-OS (7.x).
    can-with-snmp: true
    can-with-syslog: true
network-interface-tx-bytes:
    why: |
        Tracking the amount of data flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of bytes transmitted through the interface.
    without-indeni: |
        The traffic statistics of network interfaces can be retrieved through SNMP in newer versions of PAN-OS (7.x).
    can-with-snmp: true
    can-with-syslog: true
network-interface-rx-packets:
    why: |
        Tracking the number of packets flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of packets received through the interface.
    without-indeni: |
        The traffic statistics of network interfaces can be retrieved through SNMP in newer versions of PAN-OS (7.x).
    can-with-snmp: true
    can-with-syslog: true
network-interface-rx-bytes:
    why: |
        Tracking the amount of data flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of bytes received through the interface.
    without-indeni: |
        The traffic statistics of network interfaces can be retrieved through SNMP in newer versions of PAN-OS (7.x).
    can-with-snmp: true
    can-with-syslog: true
network-interface-tx-dropped:
    why: |
        If outgoing packets are being dropped on a network interface, it is important to be aware of it. This may be due to a high load on the firewall, or another capacity issue.
    how: |
        This script logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of packets dropped on an interface.
    without-indeni: |
        The traffic statistics of network interfaces can be retrieved through SNMP in newer versions of PAN-OS (7.x).
    can-with-snmp: true
    can-with-syslog: true
network-interface-tx-errors:
    why: |
        If outgoing packets are being dropped on a network interface, it is important to be aware of it. This may be due to egress errors on an interface.
    how: |
        This script logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of egress errors on an interface.
    without-indeni: |
        The traffic statistics of network interfaces can be retrieved through SNMP in newer versions of PAN-OS (7.x).
    can-with-snmp: true
    can-with-syslog: true
network-interface-tx-carrier:
    why: |
        If outgoing packets are being dropped on a network interface, it is important to be aware of it. This may be due to a signal issue on an interface.
    how: |
        This script logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of carrier transitions on an interface.
    without-indeni: |
        The traffic statistics of network interfaces can be retrieved through SNMP in newer versions of PAN-OS (7.x).
    can-with-snmp: true
    can-with-syslog: true
network-interface-rx-dropped:
    why: |
        If incoming packets are being dropped on a network interface, it is important to be aware of it. This may be due to a high load on the firewall, or another capacity issue.
    how: |
        This script logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of packets dropped on an interface.
    without-indeni: |
        The traffic statistics of network interfaces can be retrieved through SNMP in newer versions of PAN-OS (7.x).
    can-with-snmp: true
    can-with-syslog: true
network-interface-rx-frame:
    why: |
        If incoming frames are being dropped on a network interface, it is important to be aware of it. This may be due to a framing errors on the link, or another cabling issue.
    how: |
        This script logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of frames dropped on an interface.
    without-indeni: |
        The traffic statistics of network interfaces can be retrieved through SNMP in newer versions of PAN-OS (7.x).
    can-with-snmp: true
    can-with-syslog: true
network-interface-state:
    skip-documentation: true
network-interface-mac:
    skip-documentation: true
network-interface-rx-errors:
    skip-documentation: true
network-interfaces:
    skip-documentation: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><high-availability><interface>ha3</interface></high-availability></show>&key=${api-key}
protocol: HTTPS

#! PARSER::XML
_vars:
    root: /response/result/entry
_optional_metrics:
    -
        _tags:
            "im.name":
                _constant: "network-interface-state"
            "name":
                _text: "${root}/hw/name"
            "high-availability-interface":
                _constant: "true"
        _temp:
            "state":
                _text: "${root}/hw/state"
        _transform:
            _value.double: |
                {
                    if (temp("state") == "up") {
                        print "1"
                    } else {
                        print "0"
                    }
                }
    -

        _tags:
            "im.name":
                _constant: "network-interface-admin-state"
            "name":
                _text: "${root}/hw/name"
            "high-availability-interface":
                _constant: "true"
        _temp:
            "state_c":
                _text: "${root}/hw/state_c"
        _transform:
            _value.double: |
                {
                    if (temp("state_c") == "up" || temp("state_c") == "auto") {
                        print "1"
                    } else {
                        print "0"
                    }
                }
    -
        _tags:
            "im.name":
                _constant: "network-interface-duplex"
            "name":
                _text: "${root}/hw/name"
            "high-availability-interface":
                _constant: "true"
        _value.complex:
            value:
                _text: "${root}/hw[not(duplex = 'unknown')]/duplex"
    -
        _tags:
            "im.name":
                _constant: "network-interface-speed"
            "name":
                _text: "${root}/hw/name"
            "high-availability-interface":
                _constant: "true"
        _temp:
            "speed":
                _text: ${root}/hw[not(speed = 'unknown')]/speed
        _transform:
            _tags:
                "alert-item-port-speed": |
                    {
                        print tag("name") "-" temp("speed") "M"
                    }
            _value.complex:
                value: |
                    {
                        print temp("speed") "M"
                    }
    -
        _tags:
            "im.name":
                _constant: "network-interface-mac"
            "name":
                _text: "${root}/hw/name"
            "high-availability-interface":
                _constant: "true"
        _value.complex:
            value:
                _text: "${root}/hw/mac"
    -
        _tags:
            "im.name":
                _constant: "network-interface-tx-bytes"
            "name":
                _text: "${root}/hw/name"
            "im.dsType":
                _constant: "counter"
        _value.double:
            _text: "${root}/ifnet/counters/tx-bytes"
    -
        _tags:
            "im.name":
                _constant: "network-interface-rx-bytes"
            "name":
                _text: "${root}/hw/name"
            "high-availability-interface":
                _constant: "true"
            "im.dsType":
                _constant: "counter"
        _value.double:
            _text: "${root}/ifnet/counters/rx-bytes"
    -
        _tags:
            "im.name":
                _constant: "network-interface-tx-packets"
            "name":
                _text: "${root}/hw/name"
            "high-availability-interface":
                _constant: "true"
            "im.dsType":
                _constant: "counter"
        _value.double:
            _text: "${root}/ifnet/counters/tx-packets"
    -
        _tags:
            "im.name":
                _constant: "network-interface-rx-packets"
            "name":
                _text: "${root}/hw/name"
            "high-availability-interface":
                _constant: "true"
            "im.dsType":
                _constant: "counter"
        _value.double:
            _text: "${root}/ifnet/counters/rx-packets"
    -
        _tags:
            "im.name":
                _constant: "network-interface-rx-errors"
            "name":
                _text: "${root}/hw/name"
            "high-availability-interface":
                _constant: "true"
            "im.dsType":
                _constant: "counter"
        _value.double:
            _text: "${root}/ifnet/counters/rx-errs"
    -
        _tags:
            "im.name":
                _constant: "network-interface-tx-errors"
            "name":
                _text: "${root}/hw/name"
            "high-availability-interface":
                _constant: "true"
            "im.dsType":
                _constant: "counter"
        _value.double:
            _text: "${root}/ifnet/counters/tx-errs"
    -
        _tags:
            "im.name":
                _constant: "network-interface-rx-dropped"
            "name":
                _text: "${root}/hw/name"
            "high-availability-interface":
                _constant: "true"
            "im.dsType":
                _constant: "counter"
        _value.double:
            _text: "${root}/ifnet/counters/rx-drop"
    -
        _tags:
            "im.name":
                _constant: "network-interface-tx-dropped"
            "name":
                _text: "${root}/hw/name"
            "high-availability-interface":
                _constant: "true"
            "im.dsType":
                _constant: "counter"
        _value.double:
            _text: "${root}/ifnet/counters/tx-drop"
    -
        _tags:
            "im.name":
                _constant: "network-interface-tx-carrier"
            "name":
                _text: "${root}/hw/name"
            "high-availability-interface":
                _constant: "true"
            "im.dsType":
                _constant: "counter"
        _value.double:
            _text: "${root}/ifnet/counters/tx-carrier"
    -
        _tags:
            "im.name":
                _constant: "network-interface-rx-frame"
            "name":
                _text: "${root}/hw/name"
            "high-availability-interface":
                _constant: "true"
            "im.dsType":
                _constant: "counter"
        _value.double:
            _text: "${root}/ifnet/counters/rx-frame"
    -
        _tags:
            "im.name":
                _constant: "network-interfaces"
            "high-availability-interface":
                _constant: "true"
        _value.complex:
            "name":
                _text: "${root}/hw/name"
        _value: complex-array

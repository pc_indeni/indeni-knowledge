#! META
name: panos-debug-status-management-server-show
description: Grab the debug status of management-server
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos

#! COMMENTS
debug-status:
    skip-documentation: true

#! REMOTE::SSH
debug management-server show

#! PARSER::AWK
BEGIN {
    # For each flag this command supports, we list what a "normal" value is
    normal["management-server"] = "info"
}

# Note that the value can be all kinds of things depending on what the debug element is
#management-server debug:info
/[a-zA-Z\.\'\-\s]+\:\s*(\S)+/ {

    # Sometimes the output can be in JSON format like "{'management-server': 'info'}"
    gsub(/(\{|\}|')/, "", $0)
    flag = $1
    sub(/:$/, "", flag)

    debugValue = $2

    #debug:info
    sub(/.+:/, "", debugValue)

    if (flag in normal) {
        debugtags["name"] = flag

        if ((normal[flag] == debugValue) || debugValue == "off") {
          state = 0
        } else {
          state = 1
        }

    writeDoubleMetric("debug-status", debugtags, "gauge", 3600, state)
    }
}

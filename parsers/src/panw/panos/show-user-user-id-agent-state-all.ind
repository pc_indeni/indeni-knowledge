#! META
name: panos-show-user-user-id-agent-state
description: Get the status of the user-id agents
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall

#! COMMENTS
userid_agent_state:
    why: |
        The User-ID feature can be used in conjuction with User-ID agents ( https://live.paloaltonetworks.com/t5/Configuration-Articles/How-to-Install-the-Palo-Alto-Networks-User-ID-Agent/ta-p/52863 ) to collect information pertaining to which user is logged onto what IP. If the communication with an agent is down, the Palo Alto Networks firewall won't know which users are logged into each device and may be block access to critical resources (if the User-ID agent is used to limit access to them).
    how: |
        This alert uses the Palo Alto Networks API to retrieve the current status of the User-ID agents (the equivalent of running "show user user-id-agent state all" in CLI).
    without-indeni: |
        An administrator can log into the web interface to determine the status of the agents. Normally, this would be a result of an outage reported, pertaining to certain users' access to network resources.
    can-with-snmp: true
    can-with-syslog: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><user><user-id-agent><state>all</state></user-id-agent></user></show>&key=${api-key}
protocol: HTTPS

#! PARSER::AWK

#Agent: Some USERID(vsys: vsys1) Host: 1.1.1.1:222
/^\s*Agent/ {
    line = $0
    sub(/^\s*Agent:\s*/, "", line)
    agent_name = line
}

#Status                                            : not-conn:idle(Error: Failed to connect to User-ID-Agent at 1.1.1.1(1.1.1.1):222
#OR
#Status                                            : conn:Get IPs
#! (could also be several other connected states like conn:idle )
/^\sStatus/ {
    if ($3 ~ /^conn/) {
        state = 1
    } else {
        state = 0
    }

    agent_tags["name"] = agent_name
    writeDoubleMetricWithLiveConfig("userid_agent_state", agent_tags, "gauge", 60, state, "User-ID Agents", "state", "name")
}

#! META
name: panos-show-system-state-match-debug-global-n-level-n-pcap
description: Grab the debug status of multiple debug states
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall

#! COMMENTS
debug-status:
    skip-documentation: true

#! REMOTE::SSH
show system state | match debug.level\|debug.pcap\|debug\.global\|monitor\.detail\.enable\|protect\.portal\.debug

#! PARSER::AWK
BEGIN {
    # For each flag this command supports, we list what a "normal" value is
    normal["cfg.global-protect.portal.debug"] = "False"
    normal["cfg.dp.monitor.detail.enable"] = "False"
    normal["sw.ikedaemon.debug.global"] = "normal"
    normal["sw.sslvpn.debug.global"] = "info"
    normal["sw.keymgr.debug.global"] = "normal"
    normal["sw.rasmgr.debug.global"] = "normal"
    normal["sw.satd.debug.global"] = "normal"
    normal["sw.ikedaemon.debug.pcap"] = "False"
    normal["sw.sysd.debug-level"] = "4"
    normal["ha.app.debug.level"] = "debug"
    normal["md.apps.s0.mp.cfg.debug-level"] = "info"
    normal["md.apps.s1.mp.cfg.debug-level"] = "info"
    normal["md.apps.s1.cp.cfg.debug-level"] = "info"
    normal["md.apps.s1.dp1.cfg.debug-level"] = "info"
    normal["md.apps.s1.dp0.cfg.debug-level"] = "info"
    normal["md.apps.s1.mp.cfg.debug-level"] = "info"
    normal["sw.cryptod.runtime.debug.level"] = "info"
    normal["sw.dnsproxyd.runtime.debug.level"] = "warn"
    normal["sw.l2ctrld.lacp.runtime.debug.level"] = "info"
    normal["sw.l2ctrld.lldp.runtime.debug.level"] = "warn"
    normal["sw.l2ctrld.runtime.debug.level"] = "info"
    normal["sw.pppoed.runtime.debug.level"] = "warn"
    normal["sw.routed.runtime.debug.level"] = "info"
    normal["sw.sslmgr.runtime.debug.level"] = "info"
    normal["sw.dhcpd.runtime.debug.level"] = "info"
}


{
    # Debug status lines look like this:
    # Note that the value can be all kinds of things depending on what the debug element is
    #sw.satd.debug.global: normal
    # Sometimes the output can be in JSON format like "{'sw.satd.debug.global': 'debug'}"
    gsub(/(\{|\}|')/, "", $0)
    flag = $1
    sub(/:$/, "", flag)

    debugValue = $2

    #debug:info
    sub(/.+:/, "", debugValue)


    if (flag in normal) {
        debugtags["name"] = flag

        if ((normal[flag] == debugValue) || debugValue == "off") {
            state = 0
        } else {
            state = 1
        }

        writeDoubleMetric("debug-status", debugtags, "gauge", 3600, state)
    }
}
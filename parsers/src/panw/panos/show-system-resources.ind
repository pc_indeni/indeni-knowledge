#! META
name: panos-show-system-resources
description: Fetch resource utilization
type: monitoring
monitoring_interval: 1 minute
includes_resource_data: true
requires:
    vendor: paloaltonetworks
    os.name: panos

#! COMMENTS
memory-usage:
    why: |
        The various memory components of a Palo Alto Networks firewall are important to track to ensure a smooth operation. This includes the management plane's memory element (MP) as well as the variety of data plane elements (such as IP pools, SSL termination memory, etc.).
    how: |
        This script and others use the Palo Alto Networks API to retrieve the current status of multiple different memory elements.
    without-indeni: |
        The management plane's memory is visible in the web interface. Other memory elements are only visible via the CLI. An administrator would need to manually check these periodically.
    can-with-snmp: false
    can-with-syslog: false
cpu-usage:
    skip-documentation: true
memory-free-kbytes:
    skip-documentation: true
memory-total-kbytes:
    skip-documentation: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><system><resources></resources></system></show>&key=${api-key}
protocol: HTTPS

#! PARSER::AWK
BEGIN {
}

#Cpu(s):  2.2%us,  3.5%sy,  0.5%ni, 93.6%id,  0.0%wa,  0.0%hi,  0.2%si,  0.0%st
/Cpu.*%id/ {
    idle = trim($5)
    sub(/%id,/, "", idle)

    cputags["cpu-id"] = "MP"
    cputags["cpu-is-avg"] = "false"
    cputags["resource-metric"] = "true"

    writeDoubleMetricWithLiveConfig("cpu-usage", cputags, "gauge", "60", 100 - idle, "CPU Usage", "percentage", "cpu-id")
}

#Mem:   4057012k total,  3540068k used,   516944k free,    89628k buffers
/^Mem/ {
    totalk = $2
    usedk = $4
    buffersk = $8
    sub(/k/, "", totalk)
    sub(/k/, "", usedk)
    sub(/k/, "", buffersk)
    usedk = usedk - buffersk # In linux we shouldn't count "buffers" as used

    memtags["name"] = "PA firewall management plane"
    writeDoubleMetricWithLiveConfig("memory-free-kbytes", memtags, "gauge", "60", totalk-usedk, "Memory - Free", "kilobytes", "name")
    writeDoubleMetricWithLiveConfig("memory-total-kbytes", memtags, "gauge", "60", totalk, "Memory - Total", "kilobytes", "name")

    memtags["resource-metric"] = "true"
    writeDoubleMetricWithLiveConfig("memory-usage", memtags, "gauge", "60", usedk/totalk*100, "Memory Usage", "percentage", "name")
}
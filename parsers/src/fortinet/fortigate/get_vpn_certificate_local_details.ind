#! META
name: fortios-get-vpn-certificate-local-details
description: Check device X.509 certificates to see whether or not they will expire soon
type: monitoring
monitoring_interval: 59 minutes
requires:
    vendor: fortinet
    os.name: FortiOS
    product: firewall
    vdom_enabled: false

#! COMMENTS
certificate-expiration:
    why: |
        An expired SSL certificate should cause any "request" involving that certificate to fail. This could cause a variety of problems: the failure of HTTPS requests, failure of SSL/TLS web traffic inspection, the failure of X.509 certificate-based VPN tunnels, etc. SSL certificates should be renewed ahead of time.
    how: |
        Using SSH to access the Fortinet device and retrieve the X.509 "valid to" field for all "local" device certificates. If the time in this field is within a certain threshold of days, Indeni raises an alert. Note that this script does not currently validate the Fortinet's category of "remote" certificates.  It does check all "local" and root CA certificates.
    without-indeni: |
        An administrator could manually check the certificate expiration dates by using the Fortinet web GUI or by logging in to the device via SSH and manually running the necessary commands.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
get vpn certificate local details
get vpn certificate ca detail

#! PARSER::AWK

# 1) Fortinet has three "categories" of certs (displayed in the Web UI):  local, remote, and external.
#  It also stores (trusts) many root CA certificates (just like a web browser). This script currently checks
#  all of the local, external, and trusted CA certificates.  We haven't been able to get the "remote" certificates
#  yet -- the data is available from the CLI, but we need "nested config" support from Indeni to get the data.
# 2) AWK is mangling some Unicode of a few certificate names in the live config -- see this Crowd topic:
#  https://community.indeni.com/discussions/topics/46563?page=0.  On my test device, there are only 3 such certs, and
#  they're near the bottom of a long list in the live config.
# 3) Also in the live config, the timezone for the cert expiration date is incorrect. I think this is an Indeni bug.
#  See Crowd topic: https://community.indeni.com/discussions/topics/46573?page=0

#Name:        Fortinet_CA_SSL
/Name:/ {
    certName = $2
    certTags["name"] = certName
}

#Valid to:    2019-05-24 13:15:35  GMT
/Valid to:/ {
    expiresDate = $3
    expiresTime = $4
    split(expiresDate, expiresDateArr, "-")
    split(expiresTime, expiresTimeArr, ":")
    expirationDateTime = datetime(expiresDateArr[1], expiresDateArr[2], expiresDateArr[3], expiresTimeArr[1], expiresTimeArr[2], expiresTimeArr[3])
    writeDoubleMetricWithLiveConfig("certificate-expiration", certTags, "gauge", 0, expirationDateTime, "Certificates Expiration", "date", "name")
}

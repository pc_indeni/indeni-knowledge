#! META
name: chkp-fw_tab_stats-vsx
description: Run "fw tab" on all vs's in VSX
type: monitoring
monitoring_interval: 15 minute
requires:
    vendor: "checkpoint"
    vsx: "true"
    role-firewall: "true"

#! COMMENTS
kernel-table-actual:
    skip-documentation: true

kernel-table-limit:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 fw vsx stat -l | grep VSID | awk '{print $NF}'

#! PARSER::AWK

/^[0-9]+$/ {
    writeDynamicVariable("VSID", $1)
}


#! REMOTE::SSH
vsenv ${VSID} && ${nice-path} -n 15 fw vsx stat ${VSID} && (${nice-path} -n 15 fw tab | grep -B 1 " limit " && ${nice-path} -n 15 fw tab -s) && vsenv 0

#! PARSER::AWK

# The command "vsenv" does not work with nice. Added "vsenv 0" so that the parser can find end of line, as the prompt changes otherwise.

function dumpVsTableData() {

    for (tableName in actuals)
    {
        if (limits[tableName] != "")
        {
            if (tableName in accept) {
                if  (tableName !~ /nrb_hitcount_table|cptls_server_cn_cache|fwx_alloc|fwx_cache|spii_multi_pset2kbuf_map|drop_tmpl_timer|fa_free_disk_space|mal_stat_src_week|rtm_view_[0-9]*/)
                    {
                        tableTags["name"] = tableName
                        tableTags["vs.id"] = vsId
                        tableTags["vs.name"] = vsName

                        writeDoubleMetric("kernel-table-actual", tableTags, "gauge", "60", actuals[tableName])
                        writeDoubleMetric("kernel-table-limit", tableTags, "gauge", "60", limits[tableName])
                    }
                }
            }
        }
}


BEGIN {
    vsId = ""
    vsName = ""

    accept["fwx_alloc"] = 1
    accept["connections"] = 1
    accept["fwx_cache"] = 1
    accept["cphwd_db"] = 1
    accept["cphwd_tmpl"] = 1
    accept["cphwd_dev_conn_table"] = 1
    accept["cphwd_vpndb"] = 1
    accept["cphwd_dev_identity_table"] = 1
    accept["cphwd_dev_revoked_ips_table"] = 1
    accept["cphwd_pslglue_conn_db"] = 1
    accept["f2f_addresses"] = 1
    accept["tcp_f2f_ports"] = 1
    accept["udp_f2f_ports"] = 1
    accept["tcp_f2f_conns"] = 1
    accept["udp_f2f_conns"] = 1
    accept["dos_suspected"] = 1
    accept["dos_penalty_box"] = 1
    accept["client_auth"] = 1
    accept["pdp_sessions"] = 1
    accept["pdp_super_sessions"] = 1
    accept["pdp_ip"] = 1
    accept["crypt_resolver_DB"] = 1
    accept["cluster_active_robo"] = 1
    accept["appi_connections"] = 1
    accept["cluster_connections_nat"] = 1
    accept["cryptlog_table"] = 1
    accept["DAG_ID_to_IP"] = 1
    accept["DAG_IP_to_ID"] = 1
    accept["decryption_pending"] = 1
    accept["encryption_requests"] = 1
    accept["IKE_SA_table"] = 1
    accept["ike2esp"] = 1
    accept["ike2peer"] = 1
    accept["inbound_SPI"] = 1
    accept["initial_contact_pending"] = 1
    accept["ipalloc_tab"] = 1
    accept["IPSEC_mtu_icmp"] = 1
    accept["IPSEC_mtu_icmp_wait"] = 1
    accept["L2TP_lookup"] = 1
    accept["L2TP_MSPI_cluster_update"] = 1
    accept["L2TP_sessions"] = 1
    accept["L2TP_tunnels"] = 1
    accept["MSPI_by_methods"] = 1
    accept["MSPI_cluster_map"] = 1
    accept["MSPI_cluster_update"] = 1
    accept["MSPI_cluster_reverse_map"] = 1
    accept["MSPI_req_connections"] = 1
    accept["MSPI_requests"] = 1
    accept["outbound_SPI"] = 1
    accept["peer2ike"] = 1
    accept["peers_count"] = 1
    accept["persistent_tunnels"] = 1
    accept["rdp_dont_trap"] = 1
    accept["rdp_table"] = 1
    accept["resolved_link"] = 1
    accept["Sep_my_IKE_packet"] = 1
    accept["SPI_requests"] = 1
    accept["udp_enc_cln_table"] = 1
    accept["udp_response_nat"] = 1
    accept["VIN_SA_to_delete"] = 1
    accept["vpn_active"] = 1
    accept["vpn_routing"] = 1
    accept["XPO_names"] = 1
    accept["vpn_queues"] = 1
    accept["ikev2_sas"] = 1
    accept["sam_requests"] = 1
    accept["tnlmon_life_sign"] = 1
    accept["cptls_server_cn_cache"] = 1
    accept["string_dictionary_table"] = 1
    accept["dns_cache_tbl"] = 1
    accept["nrb_hitcount_table"] = 1
}

#VSID:            0
/^VSID:/ {
    if (vsId != "") {
        # Write the previous VS's data
        dumpVsTableData()
    }

    # New VSID, zero out the arrays
    delete limits
    delete actuals

    vsId = $NF

    next
}

#Name:            VSX-CXL2-Gear
/Name:/ {
    vsName = $NF

    next
}

#-------- drop_tmpl_timer --------
/^----/ {
    if (NF == 3) {
        tableName = $2
    }

    next
}

#dynamic, id 142, attributes: expires 3600, refresh, , hashsize 512, limit 1
/dynamic.*limit \d/ {
    if (match($0, "limit \\d+")) {
        limit = substr($0, RSTART+6, RSTART + RLENGTH - 6)
        limits[tableName] = limit
    }

    next
}

#localhost             vsx_firewalled                       0     1     1       0
/localhost.*/ {
    tableName = $2
    actualValue = $4
    actuals[tableName] = actualValue

    next
}

END {
    # The last VS is handled here:
    dumpVsTableData()
}

#! META
name: cpstat_fw_vsx
description: run "cpstat fw" on all vs in VSX
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    vsx: "true"
    role-firewall: "true"

#! COMMENTS
policy-installed:
    skip-documentation: true

policy-install-last-modified:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 fw vsx stat -l | grep VSID | awk '{print $NF}' | while read id; do vsenv $id && ${nice-path} -n 15 fw vsx stat $id && ${nice-path} -n 15 cpstat fw; done

#! PARSER::AWK

function addVsTags(tags) {
	tags["vs.id"] = vsid
    tags["vs.name"] = vsname
}

function dumpPolicyData() {
    addVsTags(policytags)

    if (policyname != "") {
        policystatus = 1
    } else {
        policystatus = 0
    }
	
	# Should not write a metric if this is a virtual switch, since they do not have a policy.
	if (type != "Switch") {
		writeDoubleMetricWithLiveConfig("policy-installed", policytags, "gauge", "60", policystatus, "Firewall Policy", "state", "name")
	}
	
	# Should not write a metric if this is a virtual switch, since they do not have a policy or if there is no installTime.
	if (installTime != "" && type != "Switch") {
		writeDoubleMetricWithLiveConfig("policy-install-last-modified", policytags, "gauge", "60", datetime(year,month_num,day,hour,minute,second), "Firewall Policy - Last Modified", "date", "")
	}
	policyname=""
    installtime=""
	month_num=""
	day=""
	time=""
	hour=""
	minute=""
	second=""
	year=""
	delete installTimeArr	
}

BEGIN {
	vsid=""
	vsname=""
}

# VSID:            0
/VSID:/ {
    # Dump data of previous VS if needed
    if (vsid != "") {
		dumpPolicyData()
    }
	vsid = trim($NF)
}

# Type:            Virtual Switch
# Type:            VSX Gateway
# Type:            Virtual System
/^Type:/ {
	type=$NF
}

# Name:            VSX-CXL2-Gear
/Name:/ {
	vsname = trim($NF)
}

# Policy name: Standard
/Policy name/ {
    policyname=$0
    gsub(/Policy name\:/, "", policyname)
    policyname = trim(policyname)
}

# Install time: Mon May  9 03:05:41 2016
/Install time/ {
    installTime=$0
    # Remove the "install time:" part
	gsub(/Install time\:/, "", installTime)
	# Remove some double spaces
	gsub(/  /, " ", installTime)
    split(trim(installTime),installTimeArr," ")
	month_num=parseMonthThreeLetter(installTimeArr[2])
	day=installTimeArr[3]
	time=installTimeArr[4]
	split(time,timeArr,":")
	hour=timeArr[1]
	minute=timeArr[2]
	second=timeArr[3]
	year=installTimeArr[5]
}


END {
    # Dump data of last VS
    if (vsid != "") {
        dumpPolicyData()
    }
}

#! META
name: chkp-cphaprob-state-preempt
description: Parse out information about objects from the database regarding clusters
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: checkpoint
    high-availability: "true"
    role-firewall: "true"
    vsx:
        neq: true
    or:
        -
            os.version: "R80.10"
        -
            os.version: "R80.20"

#! COMMENTS
known-devices:
    skip-documentation: true

cluster-preemption-enabled:
    why: |
        When preemption is enabled, if the primary firewall fails, then the secondary firewall will take the active role and start to forward traffic. But, when the primary firewall comes back up, it will immediately resume the active role. If this happens repeatedly in a short period of time, it can have a negative effect on performance. Therefore, best practice is not to use preemption.
    how: |
        By parsing the "cphaprob state" command and $FWDIR/state/local/FW1/local.cluster_member file, it is possible to retrieve the preempt setting for the device.
    without-indeni: |
        An administrator can view the setting in Check Point SmartDashboard.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        An administrator can view the setting in Check Point SmartDashboard.

#! REMOTE::SSH
${nice-path} -n 15 ifconfig | grep inet | awk '/Bcast/ {gsub("addr:","", $2);print "local IP: "$2}'; ${nice-path} -n 15 cat $FWDIR/state/local/FW1/local.cluster_member | awk '/ip_address / {gsub(/^\(/,"",$2);gsub(/\)$/,"",$2); print "object IP: "$2}'; ${nice-path} -n 15 cat $FWDIR/state/HAGW01/FW1/local.cluster_member | grep "\: (" | awk '{gsub(/^\(/,"",$2);gsub(/\)$/,"",$2); print "object name: "$2}'; ${nice-path} -n 15 cphaprob state




#! PARSER::AWK

##################################################################################
#                            OUTPUT                                              #
##################################################################################
#local IP: 10.0.2.34
#local IP: 192.168.100.34
#local IP: 192.168.101.34
#local IP: 192.168.102.34
#object IP: 10.0.2.33
#object IP: 10.0.2.34
#object name: GW12
#object name: GW11
#
#Cluster Mode:   High Availability (Active Up) with IGMP Membership
#
#Number     Unique Address  Assigned Load   State
#
#1 (local)  192.168.102.34  0%              Standby
#2          192.168.102.33  100%            Active
#
#Local member is in current state since Sat Sep  8 19:21:00 2018
#
#
#
##################################################################################



BEGIN{
    ip_index = 0
    object_ip_index = 0
    object_name_index = 0
    local_ip_list[1] = ""
    cluster_memebers_ips[1] = ""
    cluster_memebers_name[1] = ""
    is_local[1] = 0
}

#local IP: 10.0.2.34
/^local IP/{
    ip_index++
    eth = $4
    local_ip_list[ip_index] = eth
}

#object IP: 10.0.2.33
/^object IP/{
    object_ip_index++
    cluster_member_ip = $3
    cluster_memebers_ips[object_ip_index] = cluster_member_ip

}
#object name: GW12
/^object name/{
    object_name_index++
    member_name = $3
    cluster_memebers_name[object_name_index] = member_name
}

#Cluster Mode:   High Availability (Active Up) with IGMP Membership
/Cluster Mode/{
    HAmode = $5 " " $6
    sub( /^\(/ , "" , HAmode)
    sub( /\)$/ , "" , HAmode)
}




######## END tasks ########

END {
    for (i = 1; i <= object_name_index; i++ )  {
        for (j = 1; j <= ip_index ; j++ ) {
            if ( cluster_memebers_ips[i] == local_ip_list[j] ) {
                is_local[i] = 1
                j = ip_index+1
            }
            else {
                is_local[i] = 0
            }
        }
    }

    device_index = 1
    for (i = 1; i <= object_name_index; i++ )  {
        if ( is_local[i] == 0) {
            known_devices[device_index, "name"] =  cluster_memebers_name[i]
            known_devices[device_index, "ip"] = cluster_memebers_ips[i]
            wrote_known_devices = 1
            device_index++
        }
    }


    if (wrote_known_devices == 1) {
        writeComplexMetricObjectArray("known-devices", null, known_devices)
    }

    if ( HAmode == "Primary Up") {
        preemption_enabled = 1
    }
    else {
        preemption_enabled = 0
    }
    writeDoubleMetric("cluster-preemption-enabled", null, "gauge", 300, preemption_enabled)
 }


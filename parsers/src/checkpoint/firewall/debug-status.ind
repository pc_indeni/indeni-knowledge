#! META
name: chkp-debug-status
description: Checks if any debugs are running
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: "checkpoint"
    role-firewall: "true"
    os.name:
        neq: "gaia-embedded"   # intentional per IKP-932

#! COMMENTS
debug-status:
    why: |
        When troubleshooting a system debug flags are often enabled. When enabled they use extra resources, and forgetting to turn them off after troubleshooting has finished can mean service interruptions or reduced throughput.
    how: |
        By checking for known debug flags using Check Point built-in "fw ctl debug" command, as well as checking if tcpdump is running, the most common debugs can be alerted on if running for too long.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing debug flags is only available from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 fw ctl debug && ${nice-path} -n 15 ps aux |grep "[t]cpdump"

#! PARSER::AWK

BEGIN {
    tcpdumpStatus = 0
}

#Kernel debugging buffer size: 50KB
/buffer size:/ {

    bufferSize = $NF
    gsub(/KB/, "", bufferSize)

    if ( bufferSize == "50") {
        debugStatus = 0
    } else {
        debugStatus = 1
    }
    tags["name"] = "firewall kernel debug - bufferSize"
    writeDoubleMetric("debug-status", tags, "gauge", 300, debugStatus)
}

#Module: fw
/Module:/ {
     moduleName = $NF
}

#Enabled Kernel debugging options: error warning
#Enabled Kernel debugging options: None
/debugging options:/ {
    # extract the module options
    moduleOptions = $0
    split(moduleOptions, moduleOptionsArr, ":")
    moduleOptions = trim(moduleOptionsArr[2])

    debugStatus = 0
    # Check if the options are different than the default ones
    if ( moduleName == "kiss" && moduleOptions != "error warning") {
        debugStatus = 1
    } else if ( moduleName == "kissflow" && moduleOptions != "error warning") {
        debugStatus = 1
    } else if ( moduleName == "fw" && (moduleOptions != "error warning" && moduleOptions != "None") ) {
        debugStatus = 1
    } else if ( moduleName == "h323" && moduleOptions != "error") {
        debugStatus = 1
    } else if ( moduleName == "CPAS" && moduleOptions != "error warning") {
        debugStatus = 1
    } else if ( moduleName == "VPN" && (moduleOptions != "err" && moduleOptions != "None") ) {
        debugStatus = 1
    } else if (moduleName != "None") {
        debugStatus = 0
    }

    # Write data
    tags["name"] = "firewall kernel debug - " moduleName "module"
    writeDoubleMetric("debug-status", tags, "gauge", 300, debugStatus)
}

#pcap     23315  2.0  0.0   4532  1152 pts/37   S+   23:12   0:00 tcpdump -nni any
/tcpdump/ {
    tcpdumpStatus = 1
}

END {
    tags["name"] = "tcpdump"
    writeDoubleMetric("debug-status", tags, "gauge", 300, tcpdumpStatus)
}
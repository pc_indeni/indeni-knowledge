#! META
name: chkp-cphaconf-cluster-id
description: determine check point cluster-id number (not "cluster-id" tag)
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: checkpoint
    high-availability: "true"
    clusterxl: "true"
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform

#! COMMENTS
cluster-id-number:
    why: |
        A Check Point cluster has a value used in cluster communication, called cluster ID. It can be the same for several clusters, as long as they do not share any layer 2 segment. If all the members of a cluster do not have the same setting, the cluster will not work correctly. More information can be found in Check Point KB article SK25977: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk25977
    how: |
        The cluster ID can be set in two ways, the old method of modifying the $FWDIR/boot/modules/fwkern.conf file, and the new method of using a new command, called "cphaconf cluster_id". If the cluster ID is configured in both way the old method takes precedence.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        The cluster ID can only be viewed from the command line interface.

chkp-cluster-id-conflict:
    why: |
        A Check Point cluster has a value used in cluster communication, called cluster ID. It can be the same for several clusters, as long as they do not share any layer 2 segment. If all the members of a cluster do not have the same setting, the cluster will not work correctly. The cluster ID can be configured in two different ways, but both methods must not be used at the same time. More information can be found in Check Point KB article SK25977: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk25977
    how: |
        Attempting to list the cluster ID using both the old and the new method, and if both method are used an alert can be issued.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        The cluster ID can only be viewed from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 cat $FWDIR/boot/modules/fwkern.conf ; ${nice-path} -n 15 cphaconf cluster_id get ; ${nice-path} -n 15 fw ctl get int fwha_mac_magic ; ${nice-path} -n 15 fw ctl get int fwha_mac_forward_magic ; ${nice-path} -n 15 cphaprob mmagic 

#! PARSER::AWK

# Check Point relies on a made up MAC address to communicate when two or more gateways are formed into a cluster.
# Since they have all had the default value, there are issues when placing more than one cluster on the same L2 segment.
# To avoid this the magic MAC needs to be changed in these scenarios.
# There are three different ways of doing this, depending on version
# Below R77.30 (method 1)
#	- Set the magic MAC manually with kernel parameters
#
# R77.30 - R80 (method 2)
#	- Set the magic MAC via a new command, called "cphaconf cluster_id"
#
# R80.10 - (method 3)
#	- The gateway listens which MAC addresses other clusters uses, and select another one. 
#
# The magic MAC should not be confused with the cluster-id tag
# This script will check several things
# 1. Is the magic mac set in the fwkern.conf file? If so then it should not be set with the "cphaconf cluster_id set" command.
# 2. Is the magic mac set in the fwkern.conf different from the one set with "cphaconf cluster_id set"?
# 3. Which magic mac is the one currently set?

BEGIN {
	fwhaMacBoot = ""
	fwhaForwardBoot = ""
}

###########################################################################
#
#		Read values if they are set in the fwkern.conf file
#
###########################################################################

#fwha_mac_magic=254
/^fwha_mac_magic=[0-9]/ {
	split($0, macMagicSplitArr, "=")
	fwhaMacBoot = macMagicSplitArr[2]
}

#fwha_mac_forward_magic=253
/^fwha_mac_forward_magic=[0-9]/ {
	split($0, macForwardSplitArr, "=")
	fwhaForwardBoot = macForwardSplitArr[2]
}

###########################################################################
#
#		Read kernel values. They are set in a pre R80.10 environment
#
###########################################################################

#fwha_mac_magic = 120
/^fwha_mac_magic/ {
	fwhaMac = $3
	
	# It is not recommended to set the value in fwkern.conf as well.
	if (fwhaMacBoot != "") {
		warning = 1
	}
}

#fwha_mac_forward_magic = 119
/^fwha_mac_forward_magic/ {
	
	# It is not recommended to set the value in fwkern.conf as well.
	if (fwhaForwardBoot != "") {
		warning = 1
	}
}


###########################################################################
#
#		Read values if R80.10 or higher.
#
###########################################################################


#MAC magic:         1
/^MAC magic: / {
	fwhaMac = $3
}

###########################################################################
#
#		If cluster ID is different in kernel from boot.conf
#
###########################################################################

# cphaconf cluster_id: WARNING: different values for cluster_id: kernel VALUE_1 ha_boot.conf: VALUE_2
/WARNING: different values for cluster_id/ {
	warning = 1
}

###########################################################################
#
#		END
#
###########################################################################

END {
	writeComplexMetricString("cluster-id-number", null, fwhaMac)

	# If both the new and olf method is configured at the same time, that is not good
	if (warning == 1) {
		conflict = "true"
	} else {
		conflict = "false"
	}
	writeComplexMetricString("chkp-cluster-id-conflict", null, conflict)
}
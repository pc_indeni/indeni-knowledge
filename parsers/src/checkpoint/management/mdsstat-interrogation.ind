#! META
name: cpmdm-mdsstag-interrogation
description: Checks if server is a MDS
type: interrogation
requires:
    vendor: "checkpoint"
    role-management: "true"
    and:
        -
            os.version:
                neq: "R80.10"
        -
            os.version:
                neq: "R80.20"

#! REMOTE::SSH
${nice-path} -n 15 mdsstat

#! PARSER::AWK

############
# Why: Check if the device is running MDS(Provider-1)
# How: Check if command "mdsstat" gives any relevant output.
###########

#| MDS |  -  | 10.10.6.13      | up 15549   | up 15547 | up 15544 | up 15638 |
/MDS/ {
    # MDS has virtual systems, like VSX
    writeTag("vsx", "true")
    writeTag("mds", "true")
}

#| CMA |lab-CP-MGMT-R7730_Management_Server                                                  | 10.10.6.10      | up 26197   | up 26156 | up 26088 | up 26345 |
/CMA.*[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/ {
    vsCount++
}

END {
    writeTag("vs-count", vsCount)
    writeTag("license-vs-ratio", "1")
}

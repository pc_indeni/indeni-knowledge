#! META
name: cpembedded-vpn_tu_tunnel_status
description: run "vpn tu" to find tunnel status
type: monitoring
monitoring_interval: 5 minutes
requires:
    os.name: gaia-embedded
    role-firewall: true
    this_tag_disables_this_script: this_is_intentional

#! COMMENTS
vpn-tunnel-state:
    skip-documentation: true

#! REMOTE::SSH
echo -e "2\nQ\nQ" | ${nice-path} -n 15 vpn tu

#! PARSER::AWK

BEGIN {
}

# Get the peer's IP address
# Peer  197.45.79.20 SAs:
/Peer.*SAs/ {
    peer = $2
    # For each peer we'll look to see that we found at least one SPI for inbound, and one for outbound
    foundinbound = "false"
    foundoutbound = "false"
    section=""
}
/INBOUND/ {
    section="INBOUND"
}
/OUTBOUND/ {
    section="OUTBOUND"
}
/0x[a-z0-9]+/ {
    if (peer != "") {
        if (section == "INBOUND") {
            foundinbound="true"
        }
        if (section == "OUTBOUND") {
            foundoutbound="true"
        }

        # If we found both inbound and outbound, write the metric
        if (foundinbound == "true" && foundoutbound == "true") {
            vpntags["peerip"] = peer
            up = 1.0
            writeDoubleMetricWithLiveConfig("vpn-tunnel-state", vpntags, "gauge", "60", up, "VPN Tunnels", "state", "peerip")

            # Reset the peer so we don't get back in here
            peer = ""
        }
    }
}

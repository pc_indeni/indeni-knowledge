#! META
name: chkp-asg-drop-monitor
description: Retrieve drop data
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: checkpoint
    asg: true

#! COMMENTS
packet-drop-counter:
    why: |
        A large increase in dropped packets could mean that a new rule is blocking legitimate traffic, or that some traffic need a firewall rule to be allowed out.
    how: |
        indeni uses the built-in Check Point "asg_drop_monitor" command to retreive the number of drops.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the drops is only available from the command line interface.

#! REMOTE::SSH
cat `which asg_drop_monitor` | sed 's/watch -d -t/bash -c/' | bash

#! PARSER::AWK

BEGIN {
	ppakDropsSection = 0
}



#IP Stack qdisc drops (Tx):
#general reason                      15    PXL decision                        307
/^[a-zA-Z]/ {
	if (ppakDropsSection != 1) {
		name = $0
	} else if (ppakDropsSection == 1) {
		split($0, splitArr, /[ ]{3,}/)

		tags["name"] = splitArr[1]
		writeDoubleMetric("packet-drop-counter", tags, "counter", 300, splitArr[2])

		tags["name"] = splitArr[3]
		writeDoubleMetric("packet-drop-counter", tags, "counter", 300, splitArr[4])
	}
}

#300
/^[0-9]+$/ {
	tags["name"] = name
	writeDoubleMetric("packet-drop-counter", tags, "counter", 300, $1)
}

#Reason                Value              Reason                Value
/^Reason/ {
	ppakDropsSection = 1
}
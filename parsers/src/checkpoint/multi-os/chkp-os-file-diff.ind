#! META
name: chkp-os-file-diff
description: Populates a complex metric array with all relevant config for a list of files
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: checkpoint
    high-availability: true
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform
    # os.name: gaia-embedded removed per   IKP-932

#! COMMENTS
lines-config-files:
    why: |
        Making sure members of a cluster have the same settings is critical. In this process comparing the actual contents of important files is needed.
    how: |
        Compare all configuration lines in important configuration files across cluster members. Some lines in the files are, however, member or time specific, so these lines are excluded. All comments (lines starting with #) and blank lines are ignored. For the local.arp file, MAC addresses are removed because these are device values that change on each device.
    without-indeni: |
        An administrator could login to all cluster members and manually check differences between config files in all cluster members, but would also need to know which lines in the files to exempt.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface.

#! REMOTE::SSH


file="$FWDIR/conf/*.def"
${nice-path}  -n 15 grep -v -e ^# -e "^[[:space:]]*$" $file | awk -v prefix="$file " '{print prefix $0}'

file="/etc/sysctl.conf"
${nice-path}  -n 15 grep -v -e ^# -e "^[[:space:]]*$" $file | awk -v prefix="$file " '{print prefix $0}'

file="/etc/modprobe.conf"
${nice-path}  -n 15 grep -v -e ^# -e "^[[:space:]]*$" $file | awk -v prefix="$file " '{print prefix $0}'

file="/etc/rc.local"
${nice-path}  -n 15 md5sum $file | awk -v prefix="$file " '{print prefix "md5sum = "$1}'

file="/etc/rc.sysinit"
${nice-path}  -n 15 md5sum $file | awk -v prefix="$file " '{print prefix "md5sum = "$1}'

file="/etc/scpusers"
${nice-path}  -n 15 grep -v -e ^# -e "^[[:space:]]*$" $file | awk -v prefix="$file " '{print prefix $0}'

file="/etc/grub.conf"
exception="password"
${nice-path}  -n 15 grep -v "$exception" $file | md5sum | awk -v prefix="$file " '{print prefix "md5sum = "$1}'

file="/etc/resolv.conf"
${nice-path}  -n 15 grep -v -e ^# -e "^[[:space:]]*$" $file | awk -v prefix="$file " '{print prefix $0}'

file="/etc/syslog.conf"
${nice-path}  -n 15 grep -v -e ^# -e "^[[:space:]]*$" $file | awk -v prefix="$file " '{print prefix $0}'

file="$FWDIR/boot/modules/fwkern.conf"
${nice-path}  -n 15 grep -v -e ^# -e "^[[:space:]]*$" $file -e fwx_bridge_reroute_ipv4 | awk -v prefix="$file " '{print prefix $0}'

file="$FWDIR/conf/local.arp"
${nice-path}  -n 15 grep -v -e ^# -e "^[[:space:]]*$" $file | awk '{print $1}' | awk -v prefix="$file " '{print prefix $0}'



#! PARSER::AWK

# Match example:
#/etc/resolv.conf nameserver 8.8.8.8
# Exclude lines that don't start with the directory name eg:
#grep: /opt/CPsuite-R77/fw1/conf/local.arp: No such file or directory
/^[\$\/]/{
    i_line++
    file_name = $1
    full_line = $0
    config_line = substr ( full_line, length ( file_name ) + 1)
    lines_config [i_line, file_name] = config_line
}

END {
    # Write complex metric - after we've collected all the files
    writeComplexMetricObjectArray("lines-config-files", null, lines_config)
}

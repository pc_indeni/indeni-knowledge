#! META
name: chkp-os-failed-logins
description: List how many failed logins per user for last hour
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform

#! COMMENTS
failed-logins:
    why: |
        Attackers often try to guess user passwords, in an attempt to get access to a device. Alerting to this behavior means that the administrator could take actions to limit or stop this.
    how: |
        Count the number of failed logins for the last hour, using the information in /var/log/secure log file.
    without-indeni: |
        An administrator could login and manually read the file to count attempts.
    can-with-snmp: false
    can-with-syslog: true
    vendor-provided-management: |
        This is only accessible from the command line interface.

#! REMOTE::SSH
echo -n "time: " && clock && ${nice-path} -n 15 grep "Failed password" /var/log/secure*

#! PARSER::AWK

#time: Fri Sep  1 05:15:20 2017  -0.016178 seconds
/^time: / {

    currentYear = $6
    currentMonth = parseMonthThreeLetter($3)
    currentDay = $4

    split($5, clockSplitArr, ":")
    currentHour = clockSplitArr[1]
    currentMinute = clockSplitArr[2]
    currentSecond = clockSplitArr[3]

    currentTime = datetime(currentYear, currentMonth, currentDay, currentHour, currentMinute, currentSecond)
}


#/var/log/secure:Feb  6 14:35:20 2018 lab-CP-GW4-1 sshd[23508]: Failed password for hawkeyetmp from 192.168.201.13 port 64407 ssh2
#/var/log/secure.1:Jul 27 17:42:37 Mario sshd[29849]: Failed password for indeni from 10.10.1.1 port 63913 ssh2
/ Failed password for / {

    user = $(NF-5)
    split($1, fileAndMonth, ":")
    failedMonth = parseMonthThreeLetter(fileAndMonth[2])
    failedDay = $2

    # Sometimes, the year is missing from the log line. (I don't know why this can happen.)
    # Usually, we expect that the year of the log line should be the same as the current year. However, if we just
    # passed the new year, the log line year might be from the previous year, not the current year. To handle this case,
    # I've created this heuristic:

    if ( $4 !~ /[0-9]{4}/) {  # if the year doesn't exist in the log line
        difference = currentMonth - failedMonth

        if (difference == 0 || difference == 1) { # log month and current month are the same, or log month is previous month; assume current year
            failedYear = currentYear
        } else if (difference > 1) {  # we're in the same year, but the log line is at least a month old: we don't care about it, so move to next record
            next
        } else if (difference == -11) {  # the curr month is Jan and log month is Dec; assume previous year
            failedYear = currentYear - 1
        } else {  # difference is negative and NOT -11. We are probably in the previous year, but definitely past the hour failure window. Ignore this record, move to the next.
            next
        }
    } else {
        failedYear = $4
    }


    #10:18:30
    split($3, failedTimeSplitArr, ":")
    failedHour = failedTimeSplitArr[1]
    failedMinute = failedTimeSplitArr[2]
    failedSecond = failedTimeSplitArr[3]

    failedTime = datetime(failedYear, failedMonth, failedDay, failedHour, failedMinute, failedSecond)
    diffTime = currentTime - failedTime

    if (diffTime < 3600) { # failed time is within 60min from current time
        failed[user]++
    }
}

END {
    for (user in failed) {
        tags["username"] = user
        writeDoubleMetric("failed-logins", tags, "gauge", 300, failed[user])
    }
}
#! META
name: chkp-os-cpinfo_y_all
description: run "cpinfo -y -all" to get hotfix information
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform
        -
            os.name: ipso
            
#! COMMENTS
hotfixes:
    why: |
        It is very important to make sure that devices are patched with the latest versions and hotfixes, to prevent downtime and security incidents.
    how: |
        Using the Check Point command "cpinfo" we retreive the currently installed hotfixes.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing installed hotfixes is only available from the command line interface, and in some cases also via the WebUI and SmartUpdate.

#! REMOTE::SSH
${nice-path} -n 15 cpinfo -y -all

#! PARSER::AWK

############
# Caveats: As cpuse is used more and more, we might want to get information that way in the future.
###########

/HOTFIX/ {
    # We store it in this associative array first because some hotfixes will appear
    # multiple times. We'll later convert it into the format the ComplexMetric function expects
    hotfixesbyname[trim($0)] = "true"
}

END {
    for (hotfix in hotfixesbyname) {
        ihotfix++
        hotfixes[ihotfix, "name"]=hotfix
    }
	writeComplexMetricObjectArrayWithLiveConfig("hotfixes", null, hotfixes, "Installed Hotfixes")
}
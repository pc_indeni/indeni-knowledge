#! META
name: junos-show-chassis-cluster-information-configuration-synchronization
description: Get chassis cluster configuration synchronization status
type: monitoring
monitoring_interval: 10 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall
    high-availability: true

#! COMMENTS
cluster-config-synced:
    why: |
        The failure of configuration synchronization will cause misbehaviors when the cluster failover occurs. For examples, an interfacer which should be enabled is still in disabled state, the latest configuration fails to apply to the new active node, and etc. 
    how: |
        The script runs the "show chassis cluster information configuration synchronization" command via SSH and retrieves the configuration synchronization status.
    without-indeni: |
        An administrator could log on to the device to manually run this command to get configuration synchronization status.
    can-with-snmp:
    can-with-syslog:
    vendor-provided-management: |
        The configuration synchronization status can be retrieved via the command line.

#! REMOTE::SSH
show chassis hardware node local | match node
show chassis cluster information configuration-synchronization

#! PARSER::AWK
BEGIN {
    node0 = 0
    node1 = 0
    feature_supported = 1
    node_sync_idx = 0
}

#error: syntax error, expecting <command>: informationconfiguration-synchronization
#the firmware is below 12.1X47
/^(errors:\s+syntax\s+error)/ {
    feature_supported = 0
}

#node0:
/^node0/ {
    node0++ 
}

#        Last sync result: Succeeded
#        Last sync result: Not needed 
/(Last sync result:)/ {
    split($0, get_status, ": ")
    if ( get_status[2] == "Succeeded" || get_status[2] == "Not needed" ){ 
        SyncStatus = 1
    } else {
        SyncStatus = 0
    }
    node_sync_status[node_sync_idx] = SyncStatus 
    node_sync_idx++
}

END {
    if ( feature_supported == 1 ) {
        if ( node0 == 2) {
            node_sync_idx = 0
            cluster_node["node"] = "node0"
        } else {
            node_sync_idx = 1
            cluster_node["node"] = "node1"
        }
        writeDoubleMetric("cluster-config-synced", cluster_node, "gauge", 60, node_sync_status[node_sync_idx]) 
    }
}

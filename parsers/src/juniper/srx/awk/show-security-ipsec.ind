#! META
name: junos-show-security-ipsec
description: Retrieve VPN/ipsec information
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall

#! COMMENTS
vpn-tunnel-state:
    why: |
        The IPSec VPN state can indicate whether the IPSec VPN has been correctly configured and whether the VPN is up or down.
    how: |
        The script runs "show configuration security ike, show configuration security ipsec, show security ipsec inactive-tunnels, show security ipsec security-associations brief" to retrieve IPSec VPN related information.
    without-indeni: |
        An administrator won't find the VPN being down until the users report issues. "show security ipsec inactive-tunnels, show security ipsec security-associations brief" will show the VPN status. 
    can-with-snmp: false 
    can-with-syslog: false 
    vendor-provided-management: |
        The command line can provide the same information

#! REMOTE::SSH
show configuration security ike
show configuration security ipsec
show security ipsec security-associations brief 

#! PARSER::AWK
#####
# We first get the IKE gateways - so we can translate gateway name to IP
#####

#gateway srx220 {
/^gateway .* \{$/ {
    ikeGatewayName = $2
    inIkeGateway = "true"
}

#    address 192.168.1.20;
/^\s+address [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3};/ {
    ipAddress = $2
    sub(/;/, "", ipAddress)

    if (inIkeGateway == "true") {
        gatewaysIP[ikeGatewayName] = ipAddress
        gatewaysMonitored[ikeGatewayName] = "false"
        gatewaysState[ikeGatewayName] = "0"
    }
}

#}
/^}$/ {
    inIkeGateway = "false"
}

######
# Parsing of ipsec, matching to IKE data.
######

#vpn ipsec-vpn-cfgr {
/vpn .* \{/ {
    inVpnIPsec = "true"
}

#vpn-monitor-options
/\s+vpn-monitor/ {
    vpnMonitorEnabled = "true"
}

#        gateway ike-gate-cfgr;
/\s+gateway \S+;/ {
    ikeGatewayRef = $2
    sub(/;/, "", ikeGatewayRef)
}

#}
/^}$/ {
    if (inVpnIPsec == "true" && vpnMonitorEnabled == "true") {
        gatewaysMonitored[ikeGatewayRef] = "true"
    }
    vpnMonitorEnabled = "false"
    inVpnIPsec = "false"
}

#####
# Parsing actual Phasee II tunnel status
#####
# <2    ESP:3des/md5    e12196b8 68188/unlim   -   root 500   128.208.90.2
# >2    ESP:3des/md5    9dfde7c2 68188/unlim   -   root 500   128.208.90.2
/(500\s+)[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/ {
    peer = $(NF)
    for (gw in gatewaysIP){
         if ( gatewaysIP[gw] == peer ) {
             gatewaysState[gw] = "1" 
         }
    }  
}

END { 
    for (vpnPeer in gatewaysIP) {
        state = gatewaysState[vpnPeer]
        vpntags["name"] = "gateway " vpnPeer 
        vpntags["peerip"] = gatewaysIP[vpnPeer] 
        vpntags["always-on"] = gatewaysMonitored[vpnPeer]

        writeDoubleMetricWithLiveConfig("vpn-tunnel-state", vpntags, "gauge", 300, state, "VPN Tunnels - State", "state", "name")
    }
}


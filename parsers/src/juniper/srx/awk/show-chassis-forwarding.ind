#! META
name: junos-show-chassis-forwarding
description: Retrieve the information for what is happening on the data plane. 
type: monitoring 
includes_resource_data: true
monitoring_interval: 5 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall

#! COMMENTS
cpu-usage:
    why: |
        If the CPU utilization is high, that means the device has some problems with the data plane.
    how: |
        The script runs the "show chassis forwarding" command via ssh to retrieve the status for CPU and memory utilization on the data plane.
    without-indeni: |
        The problem on the data plane might occur and cause the device to fail before the administrator realizes.
    can-with-snmp: false 
    can-wth-syslog: false 
    vendor-provided-management: |
        The information has to be retrieved via the command line.
memory-usage:
    why: |
        If the Heap and Buffer utilization is high, that means the device has some problems with the data plane.
    how: |
        The script runs the "show chassis forwarding" command via ssh to retrieve the status for CPU and memory utilization on the data plane.
    without-indeni: |
        The problem on the data plane might occur and cause the device to fail before the administrator realizes.
    can-with-snmp: false
    can-wth-syslog: false
    vendor-provided-management: |
        The information has to be retrieved via the command line.

#! REMOTE::SSH
show chassis hardware node local | match node
show chassis forwarding 

#! PARSER::AWK
BEGIN {
    node0 = 0
    cluster = 0
    node_ukern = 0
    node_urt = 0
    node_mem_heap = 0
    node_mem_buffer = 0
}

#node0:
/^node0/ {
    node0++ 
    cluster = 1
}

# Microkernel CPU utilization        15 percent
/(Microkernel CPU utilization)/ {
    cpu_kernel[node_ukern] = $(NF-1)
    node_ukern++
}

# Real-time threads CPU utilization   0 percent
/(Real-time threads CPU utilization)/ {
    cpu_real_time[node_urt] = $(NF-1)
    node_urt++
}

# Heap utilization                   62 percent
/(Heap utilization)/ {
    memory_heap[node_mem_heap] = $(NF-1) 
    node_mem_heap++
}

# Buffer utilization                  1 percent 
/(Buffer utilization)/ {
    memory_buffer[node_mem_buffer] = $(NF-1)
    node_mem_buffer++
}

END {
    if ( cluster == 0 ) {
        node_idx = 0 
    } else {
        if (node0 == 2) {
            node_idx = 0
        } else {
            node_idx = 1
        }  
    }
    cpu_total_usage = cpu_kernel[node_idx] + cpu_real_time[node_idx]
    cpu_tag["name"] = "FWDD"
    cpu_tag["cpu-is-avg"] = "false"
    cpu_tag["resource-metric"] = "true"
    cpu_tag["im.dstype.displayType"] = "percentage"
    writeDoubleMetric("cpu-usage", cpu_tag, "gauge", 60, cpu_kernel[node_idx]) 

    mem_heap_tag["name"] = "FWDD Heap"
    mem_heap_tag["resource-metric"] = "true"
    mem_heap_tag["im.dstype.displayType"] = "percentage"
    writeDoubleMetric("memory-usage", mem_heap_tag, "gauge", 60, memory_heap[node_idx]) 

    mem_buffer_tag["name"] = "FWDD Buffer"
    mem_buffer_tag["resource-metric"] = "true"
    mem_buffer_tag["im.dstype.displayType"] = "percentage"
    writeDoubleMetric("memory-usage", mem_buffer_tag, "gauge", 60, memory_buffer[node_idx]) 
}


#! META
name: junos-show-security-flow-session-summary-cluster
description: Retrieve security flow information for chassis cluster nodes 
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall
    high-availability: true

#! COMMENTS
concurrent-connections-limit:
    why: |
        Each device has a limit for concurrent sessions or connections based on the hardware capability. Exceeding this limit will cause traffic drops. 
    how: |
       This script runs the "show security flow session summary" command via ssh to retrieve the limit for concurrent sessions.
    without-indeni: |
        The administrator has to log on the device to retrieve the same information.
    can-with-snmp: true 
    can-with-syslog: false 
concurrent-connections:
    why: |
        Concurrent-connections tell the number of connections going through the device. The administrator has to monitor the concurrent-connections to avoid traffic drops because of the high number of concurrent-connection.
    how: |
       This script runs the "show security flow session summary" command via ssh to retrieve the number of concurrent sessions.
    without-indeni: |
        The administrator has to log on the device to retrieve the same information.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::SSH
show chassis hardware node local | display xml

#! PARSER::XML
_vars:
    root: /rpc-reply

_dynamic_vars:
    _temp:
        "node":
            _text: ${root}//re-name
    _transform:
        _dynamic:
            "node": |
                {
                    if ( temp("node") == "node0" ) {
                        print "0"
                    } else {
                        print "1"
                    }
                }

#! REMOTE::SSH
show security flow session summary node ${node} | display xml

#! PARSER::XML
_vars:
    root: /rpc-reply//flow-session-summary-information
_metrics:
    -
        _tags:
            "im.name":
                _constant: "concurrent-connections"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Sessions - Current"
            "im.dstype.displayType":
                _constant: "number"
        _temp:
            "activeSessions":
                _text: ${root}/active-sessions
        _transform:
            _tags:
                "vs.id": |
                    {
                        print dynamic("node")
                    }
            _value.double: |
                {
                     print temp("activeSessions")
                }
    -
        _tags:
            "im.name":
                _constant: "concurrent-connections-limit"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Sessions - Limit"
            "im.dstype.displayType":
                _constant: "number"
        _temp:
            "maxSessions":
                _text: ${root}/max-sessions
        _transform:
            _tags:
                "vs.id": |
                    {
                        print dynamic("node")
                    }
            _value.double: |
                {
                     print temp("maxSessions")
                }


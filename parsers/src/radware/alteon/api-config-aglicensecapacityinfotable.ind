#! META
name: radware-api-config-aglicensecapacityinfotable
description: Determine license usage
type: monitoring
monitoring_interval: 1 minute 
requires:
    os.name: "alteon-os"
    vendor: "radware"

#! COMMENTS
license-elements-limit:
    why: |
        Depending on platform and license a unit may have a limit to how much it will handle. This metric keeps track of the usage of licenses with capacity limits.
    how: |
        This alert uses the Alteon API to pull License Capacity Table and looks at the license capacity.
    without-indeni: |
        Login to the device through SSH and find license utilization, or run API command ip-address/config/AgLicenseCapacityInfoTable.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This can only be tested from the command line interface.
license-elements-used:
    why: |
        Reaching the limit on number of virtual systems allowed, means no new virtual systems can be created.
    how: |
        This alert uses the Alteon API "ip-address/config/AgLicenseCapacityInfoTable" to pull License Capacity Table and looks at the license utilization.
    without-indeni: |
        Login to the device through SSH and find license utilization, or run API command "ip-address/config/AgLicenseCapacityInfoTable".
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the current number of installed virtual systems and the limit is available from the command line interface.

#! REMOTE::HTTP
url: /config/AgLicenseCapacityInfoTable
protocol: HTTPS

#! PARSER::AWK
BEGIN {
    FS = ":"
    idx = ""
}

# "LicenseCapacityInfoIdx": 9,
/LicenseCapacityInfoIdx/ {
    idx = cleanJsonValue($NF)
}

# "LicenseCapacitySize": 10000,
/LicenseCapacitySize/ {
    size_string = cleanJsonValue($NF)  
    limit = size_string
}

# "LicenseCapacityCurrUsage": "880.65 Mbps",
/LicenseCapacityCurrUsage/ {
    current_string = cleanJsonValue($NF)
    # example: current_string = "880.65 Mbps"
    split(current_string, vals, " ")
    current = vals[1]
    # example: current = 880.65
    if (arraylen(vals) == 2) {
        currentunit = vals[2]
        # unify the throughput unit to Mbps
        if (currentunit == "Gbps") {
            current = current * 1000
            currentunit = "Mbps"
        } else if (currentunit == "bps") {
            current = current / 1000000
            currentunit = "Mbps"
        }
    } else {
        currentunit = ""
    }
}

# },
/\}/ {
    if ( idx == "8" ) {
        licensetags["name"] = "vADC"
    } else if (idx == "9") {
        licensetags["name"] = "Throughput (Mbps)"
    } else if (idx == "10") {
        licensetags["name"] = "SSL (CPS)"
    } else if (idx == "11") {
        licensetags["name"] = "Compression (Mbps)"
    } else if (idx == "12") {
        licensetags["name"] = "APM (PgPM)"
    } else if (idx == "101") {
        licensetags["name"] = "Fastview (PgPS)"
    } else if (idx == "16") {
        licensetags["name"] = "AppWall (Mbps)"
    } else if (idx == "17") {
        licensetags["name"] = "Authentication (users)"
    } else {
        # next line
        next
    }

    if (current != "N/A") {
        writeDoubleMetricWithLiveConfig("license-elements-used", licensetags, "gauge", "60", current, "Licenses Usage", "number", "name")
    }
    if (limit != "N/A") {
        writeDoubleMetricWithLiveConfig("license-elements-limit", licensetags, "gauge", "60", limit, "Licenses Limit", "number", "name")
    }
}


#! META
name: radware-api-config-portInfoTable
description: Fetch the current state of the ports.
type: monitoring
monitoring_interval: 1 minute
requires:
    os.name: "alteon-os"
    vendor: "radware"
    or:
        -
            vsx: "true"
        -
            standalone: "true"

#! COMMENTS
network-interface-admin-state:
    skip-documentation: true

network-interface-speed:
    skip-documentation: true

network-interface-duplex:
    skip-documentation: true

network-interface-type:
    skip-documentation: true

network-interface-mtu:
    skip-documentation: true

network-interface-mac:
    skip-documentation: true

#! REMOTE::HTTP
url: /config/PortInfoTable
protocol: HTTPS

#! PARSER::JSON

_metrics:
    ###################
    # 1=UP,2=DOWN,3=DISABLED,4=INOPERATIVE
    ###################
    -
        _groups:
            "$.['PortInfoTable'][0:]":
                _tags:
                    "im.name":
                        _constant: "network-interface-state"
                    "live-config":
                       _constant: "true"
                    "display-name":
                        _constant: "Network - Interfaces"
                    "im.dstype.displayType":
                        _constant: "state"
                    "im.identity-tags":
                        _constant: "name"
                _temp:
                    "status":
                        _value: "Link"
                    "index":
                        _value: "Indx"
        _transform:
            _tags:
                "name": |
                    {
                        print "Port Index " temp("index")
                    }
            _value.double: |
                {
                    if (temp("status") > 1) {
                        print 0.0
                    } else {
                        print 1.0
                    }
                }
    ###################
    # 2=MBS10, 3=MBS100, 4=MBS1000, 5=ANY, 6=MBS10000, 7=MBS40000, 8=AUTO
    # 5 and 8 indicate that the port is not being used, but it is available and waiting for negotiation.
    # Best to assume that the port speed is not low.
    ###################
    -
        _groups:
            "$.['PortInfoTable'][0:][?(@.Speed in ['2', '3', '4', '6', '7'])]":
                _tags:
                    "im.name":
                        _constant: "network-interface-speed"
                    "im.identity-tags":
                        _constant: "name"
                _temp:
                    "speed":
                        _value: "Speed"
                    "index":
                        _value: "Indx"
        _transform:
            _tags:
                "name": |
                    {
                        print "Port Index " temp("index")
                    }
                "alert-item-port-speed": |
                    {
                        if (temp("speed") == 2) {
                            speed = "10M"
                        } else if (temp("speed") == 3) {
                            speed = "100M"
                        } else if (temp("speed") == 4) {
                            speed = "1000M"
                        } else if (temp("speed") == 6) {
                            speed = "10000M"
                        } else if (temp("speed") == 7) {
                            speed = "40000M"
                        }

                        print temp("index") "-" speed
                    }
            _value.complex:
                value: |
                    {
                        if (temp("speed") == 2) {
                            print "10M"
                        } else if (temp("speed") == 3) {
                            print "100M"
                        } else if (temp("speed") == 4) {
                            print "1000M"
                        } else if (temp("speed") == 6) {
                            print "10000M"
                        } else if (temp("speed") == 7) {
                            print "40000M"
                        }
                    }
    ###################
    # 2=FULL_DUPLEX, 3=HALF_DUPLEX, 4=ANY
    ###################
    -
        _groups:
            "$.['PortInfoTable'][0:][?(@.Mode in ['2', '3'])]":
                _tags:
                    "im.name":
                        _constant: "network-interface-duplex"
                    "im.identity-tags":
                        _constant: "name"
                _temp:
                    "duplex":
                        _value: "Mode"
                    "index":
                        _value: "Indx"
        _transform:
            _tags:
                "name": |
                    {
                        print "Port Index " temp("index")
                    }
            _value.complex:
                value: |
                    {
                        if (temp("duplex") == 2) {
                            print "full"
                        } else if (temp("duplex") == 3) {
                            print "half"
                        }
                    }
    ###################
    # 1=FECOPPER, 2=GECOPPER, 3=GESFP, 4=UNKNOWN, 5=XGESFP, 6=XGEQSFP
    ###################
    -
        _groups:
            "$.['PortInfoTable'][0:][?(@.PhyConnType in ['1', '2', '3', '5', '6'])]":
                _tags:
                    "im.name":
                        _constant: "network-interface-type"
                    "im.identity-tags":
                        _constant: "name"
                _temp:
                    "type":
                        _value: "PhyConnType"
                    "index":
                        _value: "Indx"
        _transform:
            _tags:
                "name": |
                    {
                        print "Port Index " temp("index")
                    }
            _value.complex:
                value: |
                    {
                        if (temp("type") == 1 || temp("type") == 2) {
                            print "Ethernet"
                        } else if (temp("type") == 3 || temp("type") == 5 || temp("type") == 6 ) {
                            print "Fiber"
                        }
                    }
    -
        _groups:
            "$.['PortInfoTable'][0:]":
                _tags:
                    "im.name":
                        _constant: "network-interface-mtu"
                    "im.identity-tags":
                        _constant: "name"
                _temp:
                    "mtu":
                        _value: "PhyIfMtu"
                    "index":
                        _value: "Indx"
        _transform:
            _tags:
                "name": |
                    {
                        print "Port Index " temp("index")
                    }
            _value.complex:
                value: |
                    {
                        print (temp("mtu") - 18)
                    }
    -
        _groups:
            "$.['PortInfoTable'][0:]":
                _tags:
                    "im.name":
                        _constant: "network-interface-mac"
                    "im.identity-tags":
                        _constant: "name"
                _value.complex: 
                    value:
                        _value: "PhyIfPhysAddress"
                _temp:
                    "index":
                        _value: "Indx"
        _transform:
            _tags:
                "name": |
                    {
                        print "Port Index " temp("index")
                    }

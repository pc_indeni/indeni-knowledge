#! META
name: radware-stats-ntp
description: Pulls NTP status and checks if it has updated.
type: monitoring
monitoring_interval: 10 minutes
requires:
    os.name: "alteon-os"
    vendor: "radware"

#! COMMENTS
ntp-server-state:
    why: |
        While the NTP is configured, it does not guarantee that NTP works as configured. It is important to track the actual state of the NTP server. If no NTP servers can be reached, the clock could slowly drift, which makes log entries and other information harder to summarize between devices. If the clock drifts very far out, there could also be issues with validating certificates.
    how: |
        Use the built-in "/stats/ntp/" status command. This retrieves the current device time and the time that the device was last updated by the NTP server. If the current device time drifts too far from the last update time, the NTP server may be unreachable
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This can only be tested from the command line interface/API of the device.

#! REMOTE::SSH
/stats/ntp

#! PARSER::AWK


#Current system time:  14:07:06 Fri Jun  9, 2017 (DST)
/Current system time/ {
	split($4, clockArr, ":")
	hour = clockArr[1]
	minute = clockArr[2]
	second = clockArr[3]

	month = parseMonthThreeLetter($6)
	
	day = $7
	#9,
	gsub (/,/," ", day)

	year = $8
	currentSinceEpoch = datetime(year, month, day, hour, minute, second)
}


#Last update time:     11:15:18 Thu Jun  8, 2017 (DST)
/Last update time/ {
	split($4, clockArr, ":")
	hour = clockArr[1]
	minute = clockArr[2]
	second = clockArr[3]

	month = parseMonthThreeLetter($6)

	day = $7
	#8,
	gsub (/,/," ", day)

	year = $8
	lastUpdateSinceEpoch = datetime(year, month, day, hour, minute, second)
}



END {
	differencesInSeconds = currentSinceEpoch - lastUpdateSinceEpoch
	
	if (differencesInSeconds < 600) {
		serverState = 1
	} else {
		serverState = 0
	}
	
	writeDoubleMetric("ntp-server-state", null, "gauge", 0, serverState)
}

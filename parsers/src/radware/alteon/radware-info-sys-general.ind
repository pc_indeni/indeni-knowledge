#! META
name: radware-info-sys-general
description: Get cluster information 
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: "radware"
    os.name: "alteon-os"
    high-availability: "true"

#! COMMENTS
cluster-member-active:
    why: |
        Tracking the state of a cluster member is important. If a cluster member which used to be the active member of the cluster no longer is, it may be the result of an issue. In some cases, it is due to maintenance work (and so was anticipated), but in others it may be due to a failure in the firewall or another component in the network.
    how: |
        This alert logs into the Radware device through SSH and retrieves the local member's state.
    without-indeni: |
        An unplanned change of a cluster members state could be detected by traffic disruptions. An administrator could verify the cluster member state by logging into the web interface of the device looking in the upper left corner. An active device would show "ACTIVE".
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management |
        Can be done through Management GUI (Vision or Alteon VX) or through the CLI.

#! REMOTE::SSH
/stats/slb/pip/ / /

#! PARSER::AWK

#HA State:    NONE
/HA State:/ {
    sHaState = tolower($3)
    if (sHaState == "active") {
        dHaState = 1
    } else {
        dHaState = 0
    }
}

#vADC 1
/^vADC [0-9]+$/ {
    vADC = $0
}

END {
    if (length(vADC) > 0) {
        clusterTag["name"] = vADC
        writeDoubleMetric("cluster-member-active", clusterTag, "gauge", 300, dHaState)
    }
}

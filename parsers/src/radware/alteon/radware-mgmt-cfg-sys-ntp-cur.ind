#! META
name: radware-mgmt-cfg-sys-ntp-cur
description: Show current NTP server settings
type: monitoring
monitoring_interval: 10 minutes
requires:
    os.name: "alteon-os"
    vendor: "radware"
    or:
        -
            vadc: "true"
        -
            standalone: "true"

#! COMMENTS
ntp-servers:
    why: |
        Not having an NTP server configured could make the clock slowly drift, which makes log entries and other information harder to summarize between devices. If the clock drifts very far out, there could also be issues with validating certificates.
    how: |
        Using the "/cfg/sys/ntp/cur/" command in the CLI, Indeni will verify that ntp servers have been configured and that the current status of any existing servers is ok.
    without-indeni: |
        An administrator could login to the unit through SSH and issue the command "/cfg/sys/ntp/cur/" to see the configured ntp servers.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        Can be done through Management GUI (Vision or Alteon VX).

#! REMOTE::SSH
/cfg/sys/ntp/cur/ / /

#! PARSER::AWK

#Current primary NTP server: 173.71.73.207
#Current secondary NTP server: 2.2.2.0
/NTP server/ {
    iEntry++

    servers[iEntry, "ipaddress"] = $5
    servers[iEntry, "type"] = $2 
} 

END {
    writeComplexMetricObjectArrayWithLiveConfig("ntp-servers", null, servers, "NTP Servers")
}


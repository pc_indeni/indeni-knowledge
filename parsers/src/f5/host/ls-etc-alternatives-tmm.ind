#! META
name: f5-ls-etc-alternatives-tmm
description: See if the tmm processes is in debug mode
type: monitoring
monitoring_interval: 30 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
debug-status:
    why: |
        The tmm.debug daemon can be enabled in order to gather additional metrics when the F5 unit crashes. However, this state is not desireable in a normal set-up as the tmm debug daemon performs slower than the default version.
    how: |
        This alert logs into the F5 load balancer and verifies that the debug daemon is not activated by checking where the sym link /etc/alternatives/tmm is pointing to.
    without-indeni: |
        Login to the device with SSH and run "ls -l /etc/alternatives/tmm" and verify that the sym link is not pointing to "/usr/bin/tmm.debug"
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: Unknown  
    
#! REMOTE::SSH
${nice-path} -n 15 /bin/ls -l /etc/alternatives/tmm

#! PARSER::AWK

#Source article about enabling/disabling this debug process
#https://support.f5.com/kb/en-us/solutions/public/11000/400/sol11490.html

#lrwxrwxrwx. 1 root webusers 18 2016-11-06 12:52 /etc/alternatives/tmm -> /usr/bin/tmm.debug
/tmm/{
	
	debugTags["name"] = "tmm-process"
	
	if($NF == "/usr/bin/tmm.debug"){	
		status = 1
	} else {
		status = 0
	}
	writeDoubleMetricWithLiveConfig("debug-status", debugTags, "gauge", 300, status, "TMM Debug", "state", "")
}

#! META
name: f5-geo-ip-lookup
description: Determine last update of the geoip databases
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
geoip-database-version:
    why: |
        The geo-ip database provides meta data related to an IP address, such as city, region, country and ISP. Should this data be old or stale it could affect decisions taken based on geo-ip data in a negative way. For instance, a client might be refused access to critical services because his or her IP belongs to the wrong country.
    how: |
        This alert logs into the F5 load balancer and retrieves the current version of the geo-ip database.
    without-indeni: |
        To check the data base version, first log in to the unit with SSH. Then list the available files in "/shared/GeoIP". For each file, issue the following command: "geoip_lookup -f /shared/GeoIP/<filename> <IP>". Example: "geoip_lookup -f /shared/GeoIP/F5GeoIP.dat 8.8.8.8". Note the version informantion to determine the issue data of the database file. Example: "version = GEO-148 20170105" was issued 5th of January 2017.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: Unknown
geoip-database-state:
    why: |
        The geo-ip database provides meta data related to an IP address, such as city, region, country and ISP. Should the database be unavailable any attempts to retrieve geo-ip data could cause unpredictable behaviour.
    how: |
        This alert logs into the F5 load balancer and makes a lookup in the geo-ip database to verify it's functionality.
    without-indeni: |
        To check the data base version, first log in to the unit with SSH. Then list the available files in "/shared/GeoIP". For each file, issue the following command: "geoip_lookup -f /shared/GeoIP/<filename> <IP>". Example: "geoip_lookup -f /shared/GeoIP/F5GeoIP.dat 8.8.8.8". If a record was returned the database is intact.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: Unknown
    
#! REMOTE::SSH
for file in /shared/GeoIP/* ;do echo "---BEGINRECORD---";if [[ $file == *"v6"* ]] ; then ip="2001:4860:4860::8888"; else ip="8.8.8.8"; fi;echo "GeoIPDatabase: $file"; geoip_lookup -f $file $ip | egrep "(^country_name|^name =|version)"; echo "---ENDRECORD---"; done

#! PARSER::AWK

#The GeoIP database is used to take decisions based on client geographical location, ISP etc.
#This data is not automatically updated and requires manual updates using packages from download.f5.com.

#A failed database would cause problems when using lookups.
#An outdated database could place a customer in a country where they are not situated and cause problems in ie. irules.

#Remediation:
#Re-install/update the database
#https://support.f5.com/csp/#/article/K11176

#Updates are released, but not at a set interval so I would count a database being 3 months old too old.

#---BEGINRECORD---
/---BEGINRECORD---/{
    #Resetting tags and variables for geoip database version
    geoIPDatabaseVersionTags["database"] = ""
    geoIPDatabaseVersionTags["im.dstype.displayType"] = "date"
    versiondate = ""
    
    #Resetting tags and variables for geoip database state
    geoIPDatabaseStateTags["im.dstype.displayType"] = "state"
    result = 0
}

#GeoIPDatabase: /shared/GeoIP/F5GeoIP.dat
/^GeoIPDatabase:/{
    geoIPDatabaseVersionTags["database"] = $2
    geoIPDatabaseStateTags["database"] = $2
}

#country_name = United States
#name = google inc.
/(^country_name|^name =|version)/{
    #Got a result
    result = 1
}

#size of geoip database = 221754926, segments = 11319514, version = GEO-148 20161206 Build 1 Copyright (c) F5 Networks Inc All Rights Reserved
/version/{
    
    if(NF > 12){
    
        versiondate = $13
        
        if(match(versiondate,/^[\d]{8}$/)){
            #20161206
            year = versiondate
            month = versiondate
            day = versiondate
            
            #Remove the last 4 digits to get the year
            sub(/[\d]{4}$/,"",year)
            
            #Remove the first 4 digits to get month and day
            sub(/^[\d]{4}/,"",month)
            
            #Remove the last two to get the month
            #1206
            sub(/[\d]{2}$/,"",month)
            
            #Remove the first 6 digits to get the day
            sub(/^[\d]{6}/,"",day)
            
            versiondate = date(year,month,day)
        }
    }
}

#---ENDRECORD---
/---ENDRECORD---/{
    #Verifying that we have a date and writing metric if we do
    if(versiondate != ""){
        #Writing metric for when the database was alst updated
        writeDoubleMetric("geoip-database-version", geoIPDatabaseVersionTags, "gauge", 1440, versiondate)
    }
    
    #Verifying that a result was detected
    if(result == 1){
        writeDoubleMetric("geoip-database-state", geoIPDatabaseStateTags, "gauge", 1440, 1)
    } else {
        writeDoubleMetric("geoip-database-state", geoIPDatabaseStateTags, "gauge", 1440, 0)
    }
    
    
}

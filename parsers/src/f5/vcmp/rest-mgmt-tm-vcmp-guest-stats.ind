#! META
name: f5-rest-mgmt-tm-vcmp-guest-stats
description: Extract status of deployed vCMP guests
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    vsx: "true"

#! COMMENTS
vs-state:
    why: |
        Keeping track of vCMP guest states will enable alerting for when a VM has failed.
    how: |
        This script uses the F5 iControl REST API to retrieve the current status of vCMP guests.
    without-indeni: |
        This metric is available by logging into the device with SSH, entering TMSH and executing "show vcmp guest".
    can-with-snmp: false
    can-with-syslog: false
uptime-milliseconds:
    why: |
        Keeping track of vCMP guest uptimes can help trigger alerts if they are suddenly restarted due to a system failure.
    how: |
        This script uses the F5 iControl REST API to retrieve the current uptime of vCMP guests.
    without-indeni: |
        This metric is available by logging into the device with SSH, entering TMSH and executing "show vcmp guest".
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/sys/clock
protocol: HTTPS

#! PARSER::JSON

_dynamic_vars:
    _temp:
        "fullDate":
            _value: "$.entries.*.nestedStats.entries.fullDate.description"
    _transform:
        _dynamic:
            "currentTime": |
                {

                    strCurrentTime = temp("fullDate")

                    #2016-12-01T07:55:34Z
                    gsub(/[^\d-:]/, " ", strCurrentTime)

                    #2016-12-01 07:55:34
                    split(strCurrentTime, dateTimeArr, /\s/)

                    strDate = dateTimeArr[1]
                    strTime = dateTimeArr[2]

                    #2016-12-01
                    split(strDate, dateArr, /-/)

                    year = dateArr[1]
                    month = dateArr[2]
                    day = dateArr[3]

                    #07:55:34
                    split(strTime, timeArr, /:/)

                    hour = timeArr[1]
                    minute = timeArr[2]
                    second = timeArr[3]

                    secondsSinceEpoch = datetime(year, month, day, hour, minute, second)

                    print secondsSinceEpoch
                }

#! REMOTE::HTTP
url: /mgmt/tm/vcmp/guest/stats
protocol: HTTPS

#! PARSER::JSON

_metrics:
    - #vs-state OK
        _groups:
            "$.entries.*.nestedStats.entries.[?(@.requestedState.description == 'deployed' && @.requestComplete.description == 'true' && @.vmStatus.description == 'running')]":
                _tags:
                    "im.name":
                        _constant: "vs-state"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _constant: "1"
    - #vs-state not OK
        _groups:
            "$.entries.*.nestedStats.entries.[?(@.vmStatus.description == 'failed')]":
                _tags:
                    "im.name":
                        _constant: "vs-state"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _constant: "0"
    - #Guest uptime
        _groups:
            "$.entries.*.nestedStats.entries.[?(@.requestedState.description == 'deployed' && @.requestComplete.description == 'true')]":
                _tags:
                    "im.name":
                        _constant: "uptime-milliseconds"
                    "vs.name":
                        _value: "tmName.description"
                _temp:
                    "uptime":
                        _value: "uptime.description"
        _transform:
            _value.double: |
                {
                    strUptime = temp("uptime")

                    #2016-12-01T07:55:34Z
                    gsub(/[^\d-:]/, " ", strUptime)

                    #2016-12-01 07:55:34
                    split(strUptime, dateTimeArr, /\s/)

                    strDate = dateTimeArr[1]
                    strTime = dateTimeArr[2]

                    #2016-12-01
                    split(strDate, dateArr, /-/)

                    year = dateArr[1]
                    month = dateArr[2]
                    day = dateArr[3]

                    #07:55:34
                    split(strTime, timeArr, /:/)

                    hour = timeArr[1]
                    minute = timeArr[2]
                    second = timeArr[3]

                    secondsSinceEpoch = datetime(year, month, day, hour, minute, second)

                    uptime = (dynamic("currentTime") - secondsSinceEpoch) * 1000

                    print uptime
                }

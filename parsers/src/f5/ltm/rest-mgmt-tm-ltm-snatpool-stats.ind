#! META
name: f5-rest-mgmt-tm-ltm-snatpool-stats
description: Get snatpool server side connection statistics
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"

#! COMMENTS
lb-snatpool-usage:
    why: |
        SNAT port exhaustion could be a problem in environments that have large amounts of connections and too few source NAT IP's. If all available port combinations are exhausted it will lead to connections being dropped by the system.
    how: |
        This alert uses the iControl REST interface to extract the number of current server side connections per SNAT pool. Please note that this alert is not compatible with IP addresses belonging to different networks.
    without-indeni: |
        An adminstrator could login to the device through SSH, execute the command "tmsh -c 'cd /;show ltm snatpool recursive'". Then for each SNAT pool compare the connection count to the SNAT pool limit. More information can be found here: https://support.f5.com/csp/article/k7820.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/ltm/snatpool/stats?$select=tmName,serverside.totConns
protocol: HTTPS

#! PARSER::JSON

_metrics:
    - # Get the usage for each SNAT Pool
        _groups:
            $.entries.*.nestedStats.entries:
                _tags:
                    "im.name":
                        _constant: "lb-snatpool-usage"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _value: "['serverside.totConns'].value"

#! META
name: ios-dir-all-filesystems 
description: IOS dir all-filesystems
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: cisco
    os.name: ios

#! COMMENTS
disk-used-kbytes:
    skip-documentation: true

disk-total-kbytes:
    skip-documentation: true

disk-usage-percentage:
    why: |
       Check the utilization percentage of each of the different file systems on the device. If the free space gets too low an alert will be triggered.
       Filesystems that are over utilized, i.e. reaching their max capacity, can cause device or feature instabilities.
    how: |
       This script logs into the Cisco IOS device using SSH and uses the "dir all-filesystems" to collect information about the device's file systems.
    without-indeni: |
       Admins would have to log into the device and execute the 'dir all-filesystems' command manually. 
       Then they'd need to inspect the output and determine whether the respective filesystem has adequate capacity or is reaching its max value.
    can-with-snmp: false
    can-with-syslog: false
        
#! REMOTE::SSH
# Retrieve the filesystems first (mounted as directories) and then retrieve their allocations (free|space)
dir all-filesystems

#! PARSER::AWK

# Sample Input ###########################################
# Directory of flash:/
#   429  -rwx        5768  May 30 1993 23:59:52 +00:00  private-config.text
# 15998976 bytes total (963584 bytes free)
# 
# Directory of system:/
#     3  dr-x           0                    <no date>  vfiles
# No space information available
# 
# Directory of tmpsys:/
#     1  dr-x           0                    <no date>  lib
# No space information available
# 
# Directory of nvram:/
#     2  -rw-         595                    <no date>  IOS-Self-Sig#3232.cer
# 524288 bytes total (511485 bytes free)

BEGIN {
    newnic=0
    firstloop=1
    entry=0
    metric_tags["im.identity-tags"] = "file-system"
    metric_tags["live-config"] = "true"
    kbyte = 1024
}

/^Directory/ {
    dir++
    disk_name = $3
    flashtag[dir, "disk-name"] = disk_name
    writeDebug(" INDENI Add  ==> ... " disk_name " at location " dir)
}

/^No space information/ {
    space++
    writeDebug(" INDENI N/A  ==> " space)
}

/bytes/ {
    space++
    writeDebug(" INDENI Free ==> " space ", " flashtag[space, "disk-name"])
    
    if ($0 !~ /information/) { 
        disk_total_kbytes = $1
        disk_free_kbytes = $4
        sub(/\(/, "", disk_free_kbytes)
                
        # Total 
        metric_tags["display-name"] = "File System - Total"
        metric_tags["file-system"] = flashtag[space, "disk-name"]
        metric_tags["im.dstype.displayType"] = "kilobytes"
        writeDoubleMetric("disk-total-kbytes", metric_tags, "gauge", 1, disk_total_kbytes/kbyte)

        # Used 
        metric_tags["display-name"] = "File System - Used"
        metric_tags["file-system"] = flashtag[space, "disk-name"]
        metric_tags["im.dstype.displayType"] = "kilobytes"
        used_kbytes = disk_total_kbytes - disk_free_kbytes
        writeDoubleMetric("disk-used-kbytes", metric_tags, "gauge", 1, used_kbytes/kbyte)

        # Usage 
        metric_tags["display-name"] = "File System - Usage"
        metric_tags["file-system"] = flashtag[space, "disk-name"]
        metric_tags["im.dstype.displayType"] = "percentage"
        percentage = 100 - ((disk_free_kbytes*100)/disk_total_kbytes)
        #((disk_free_kbytes*100)/disk_total_kbytes)
        writeDoubleMetric("disk-usage-percentage", metric_tags, "gauge", 1, percentage)
    }
}

END {
}

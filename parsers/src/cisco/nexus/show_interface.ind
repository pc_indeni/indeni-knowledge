#! META
name: nexus-show-interface
description: Nexus Interface Information
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: "cisco"
    os.name: "nxos"

#! COMMENTS
network-interface-state:
    why: |
        Capture the interface state. If an interface transitions from up to down an alert would be raised.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP. Interface state transitions will generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

network-interface-admin-state:
    why: |
        Capture the interface administrative state.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP. Interface state transitions will generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

network-interface-description:
    why: |
        Capture the interface description.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-speed:
    why: |
        Capture the interface speed in human readable format such as 1G, 10G, etc.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-duplex:
    why: |
        Capture the interface duplex in human readable format such as full or half. In modern network environments it is very uncommon to see half-duplex interfaces, and that should be an indication for a potential exception.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP. If a duplex mismatch is detected on a port syslog messages will be generated.
    can-with-snmp: true
    can-with-syslog: true

network-interface-rx-frame:
    why: |
        Capture the interface Receive Errors (CRC) counter. If this counter increases an alarm will be raised.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-tx-overruns:
    why: |
        Capture the interface Transmit Overrun errors counter.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-mac:
    why: |
        Capture the interface MAC address.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-type:
    why: |
        Capture the interface type, for example "1000/10000 Ethernet".
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-rx-bytes:
    why: |
        Capture the interface Received Bytes counter. Knowing the amount of bytes and packets flowing through an interface can help estimate an interface's performance and utilization.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-rx-dropped:
    why: |
        Capture the interface Received Errors counter. Packet loss may impact traffic performance.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-rx-packets:
    why: |
        Capture the interface Received Packets counter. Knowing the amount of bytes and packets flowing through an interface can help estimate an interface's performance and utilization.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-ipv4-address:
    why: |
        Capture the interface IPv4 address. Only relevant for layer 3 interfaces, including Vlan interfaces (SVI).
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-ipv4-subnet:
    why: |
        Capture the interface IPv4 subnet mask. Only relevant for layer 3 interfaces, including Vlan interfaces (SVI).
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-tx-carrier:
    why: |
        Capture the interface carrier state change counter. It would increase every time the interface changes state from up to down.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-mtu:
    why: |
        Capture the interface MTU (Maximum Transmit Unit).
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-tx-bytes:
    why: |
        Capture the interface Transmitted Bytes counter. Knowing the amount of bytes and packets flowing through an interface can help estimate an interface's performance and utilization.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-tx-errors:
    why: |
        Capture the interface Transmit Errors counter. Transmission errors indicate an issue with duplex/speed matching.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-tx-packets:
    why: |
        Capture the interface Transmitted Packets counter. Knowing the amount of bytes and packets flowing through an interface can help estimate an interface's performance and utilization.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-rx-overruns:
    why: |
        Capture the interface Receive Overrun errors counter.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-invalid-sfp:
    why: |
        Capture interface SFP verification errors.
        Interface SFP verification can happen if the interface speed is configured incorrectly or the SFP module is an incompatible non-cisco hardware.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        Invalid detected SFPs would generate a Syslog message. The user would have to login to the device and use the "show interface" command to identify the reason for the invalid SFP.
    can-with-snmp: true
    can-with-syslog: true

network-interface-err-disable:
    why: |
        Capture the interface error-disable status.
        Interfaces can get automatically disabled (err-disable) if different error conditions are detected. For example link flaps, a connection loop or spanning tree BPDUs detected with BPDU guard.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        Error disabled interfaces would generate a syslog event. The user would have to login to the device and use the "show interface" command to identify the reason for the error disabled port.
    can-with-snmp: true
    can-with-syslog: true

network-interface-err-disable-description:
    why: |
        Capture additional data on the reason the interface is in error-disable status.
        Interfaces can get automatically disabled (err-disable) if different error conditions are detected. For example link flaps, a connection loop or spanning tree BPDUs detected with BPDU guard.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface" command. The output includes all the interface related information and statistics.
    without-indeni: |
        Error disabled interfaces would generate a syslog event. The user would have to login to the device and use the "show interface" command to identify the reason for the error disabled port.
    can-with-snmp: true
    can-with-syslog: true

#! REMOTE::SSH
show interface | xml | no-more

#! PARSER::XML
_omit:
    _bottom: 1
_vars:
    root: //__readonly__
_metrics:
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _temp:
                    state:
                        _text: "state"
                _tags:
                    "im.name":
                        _constant: "network-interface-state"
                    "name":
                        _text: "interface"
        _transform:
             _value.double: |
                 {
                     if (temp("state") == "up") {print "1.0"} else {print "0.0"}
                 }
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _temp:
                    state_rsn:
                        _text: "state_rsn_desc"
                _tags:
                    "im.name":
                        _constant: "network-interface-admin-state"
                    "name":
                        _text: "interface"
        _transform:
             _value.double: |
                 {
                     result = "0.0"
                     if (temp("state_rsn") == "Link not connected") {
                         result = "1.0"
                     }
                     if (temp("state_rsn") == "SFP validation failed") {
                         result = "1.0"
                     }
                     print result
                 }
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _temp:
                    state_rsn:
                        _text: "state_rsn_desc"
                _tags:
                    "im.name":
                        _constant: "network-interface-invalid-sfp"
                    "name":
                        _text: "interface"
        _transform:
             _value.double: |
                 {
                     result = "0.0"
                     if (temp("state_rsn") == "SFP validation failed") {
                         result = "1.0"
                     }
                     print result
                 }
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _temp:
                    state_rsn:
                        _text: "state_rsn_desc"
                _tags:
                    "im.name":
                        _constant: "network-interface-err-disable"
                    "name":
                        _text: "interface"
        _transform:
             _value.double: |
                 {
                     result = "0.0"
                     if (temp("state_rsn") ~ "ErrDisabled") {
                         result = "1.0"
                     }
                     print result
                 }
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _temp:
                    state_rsn:
                        _text: "state_rsn_desc"
                _tags:
                    "im.name":
                        _constant: "network-interface-err-disable-description"
                    "name":
                        _text: "interface"
        _transform:
             _value.complex:
                 value: |
                     {
                         result = ""
                         if (temp("state_rsn") ~ "ErrDisabled") {
                             result = temp("state_rsn")
                         }
                         print result
                     }
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.complex:
                    value:
                        _text: "desc"
                _tags:
                    "im.name":
                        _constant: "network-interface-description"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _temp:
                    name:
                        _text: "interface"
                    speed:
                       _text: "eth_media"
                _value.complex:
                    value:
                        _text: "eth_media"
                _tags:
                    "im.name":
                        _constant: "network-interface-speed"
                    "name":
                        _text: "interface"
        _transform:
            _tags:
                "alert-item-port-speed": |
                    {
                        print temp("name") "-" temp("speed")
                    }
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _temp:
                    duplex:
                        _text: "eth_duplex"
                _tags:
                    "im.name":
                        _constant: "network-interface-duplex"
                    "name":
                        _text: "interface"
        _transform:
            _value.complex:
                value: |
                    {
                        if (temp("duplex") == "half") {
                            print "half"
                        } else {
                            print "full"
                        }
                    }
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.double:
                    _text: "eth_crc"
                _tags:
                    "im.name":
                        _constant: "network-interface-rx-frame"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.double:
                    _text: "eth_deferred"
                _tags:
                    "im.name":
                        _constant: "network-interface-tx-overruns"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.complex:
                    value:
                        _text: "eth_hw_addr"
                _tags:
                    "im.name":
                        _constant: "network-interface-mac"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.complex:
                    value:
                        _text: "eth_hw_desc"
                _tags:
                    "im.name":
                        _constant: "network-interface-type"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.double:
                    _text: "eth_inbytes"
                _tags:
                    "im.name":
                        _constant: "network-interface-rx-bytes"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.double:
                    _text: "eth_inerr"
                _tags:
                    "im.name":
                        _constant: "network-interface-rx-dropped"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.double:
                    _text: "eth_inpkts"
                _tags:
                    "im.name":
                        _constant: "network-interface-rx-packets"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.complex:
                    value:
                        _text: "eth_ip_addr"
                _tags:
                    "im.name":
                        _constant: "network-interface-ipv4-address"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.complex:
                    value:
                        _text: "eth_ip_mask"
                _tags:
                    "im.name":
                        _constant: "network-interface-ipv4-subnet"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.double:
                    _text: "eth_lostcarrier"
                _tags:
                    "im.name":
                        _constant: "network-interface-tx-carrier"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.complex:
                    value:
                        _text: "eth_mtu"
                _tags:
                    "im.name":
                        _constant: "network-interface-mtu"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.double:
                    _text: "eth_outbytes"
                _tags:
                    "im.name":
                        _constant: "network-interface-tx-bytes"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.double:
                    _text: "eth_outerr"
                _tags:
                    "im.name":
                        _constant: "network-interface-tx-errors"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.double:
                    _text: "eth_outpkts"
                _tags:
                    "im.name":
                        _constant: "network-interface-tx-packets"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.double:
                    _text: "eth_overrun"
                _tags:
                    "im.name":
                        _constant: "network-interface-rx-overruns"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _temp:
                    admin_state:
                        _text: "svi_admin_state"
                _tags:
                    "im.name":
                        _constant: "network-interface-admin-state"
                    "name":
                        _text: "interface"
        _transform:
             _value.double: |
                 {
                     if (temp("admin_state") == "up") {print "1.0"} else {print "0.0"}
                 }
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _temp:
                    admin_state:
                        _text: "svi_admin_state"
                _tags:
                    "im.name":
                        _constant: "network-interface-type"
                    "name":
                        _text: "interface"
        _transform:
             _value.complex:
                 value: |
                    {
                        print "Vlan"
                    }
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.complex:
                    value:
                        _text: "svi_desc"
                _tags:
                    "im.name":
                        _constant: "network-interface-description"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "interface"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces"
                    "im.dstype.displayType":
                        _constant: "text"
                    "im.identity-tags":
                        _constant: "name"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.complex:
                    value:
                        _text: "svi_ip_addr"
                _tags:
                    "im.name":
                        _constant: "network-interface-ipv4-address"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.complex:
                    value:
                        _text: "svi_ip_mask"
                _tags:
                    "im.name":
                        _constant: "network-interface-ipv4-subnet"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _temp:
                    state:
                        _text: "svi_line_proto"
                _tags:
                    "im.name":
                        _constant: "network-interface-state"
                    "name":
                        _text: "interface"
        _transform:
             _value.double: |
                 {
                     if (temp("state") == "up") {print "1.0"} else {print "0.0"}
                 }
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.complex:
                    value:
                        _text: "svi_mac"
                _tags:
                    "im.name":
                        _constant: "network-interface-mac"
                    "name":
                        _text: "interface"
    -
        _groups:
            ${root}/TABLE_interface/ROW_interface:
                _value.double:
                    _text: "svi_mtu"
                _tags:
                    "im.name":
                        _constant: "network-interface-mtu"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "interface"
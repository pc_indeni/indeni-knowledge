#! META
name: nexus-snmp-protocol-status
description: Nexus snmp protocol status
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "cisco"
    os.name: "nxos"

#! COMMENTS
snmp-enabled:
    why: |
        Capture whether SNMP is enabled on the device.
    how: |
       This script logs in to the Cisco Nexus switch using SSH and retrieves the current state of the SNMP protocol by using the "show snmp" command.
    without-indeni: |
       The administrator would have to manually log in to the device and check if SNMP is enabled.
    can-with-snmp: false
    can-with-syslog: false

snmp-contact:
    why: |
        Capture the SNMP contact information. This field can be used to store real contact information for the device.
    how: |
       This script logs in to the Cisco Nexus switch using SSH and retrieves the current state of the SNMP protocol by using the "show snmp" command.
    without-indeni: |
       The administrator would have to manually log in to the device and check the SNMP contact.
    can-with-snmp: true
    can-with-syslog: false

snmp-location:
    why: |
        Capture the SNMP location information. This field can be used to store real location information for the device.
    how: |
       This script logs in to the Cisco Nexus switch using SSH and retrieves the current state of the SNMP protocol by using the "show snmp" command.
    without-indeni: |
       The administrator would have to manually log in to the device and check the SNMP location.
    can-with-snmp: true
    can-with-syslog: false

snmp-communities:
    why: |
        Capture the SNMP communities and permissions. SNMP communities are used by SNMP v1/v2c to identify the management system polling SNMP information from the device. Each community can be associated with a different security level (read-only or read/write) and a different view of the SNMP MIB tree.
        Note that SNMP communities are transimitted in clear text.
    how: |
       This script logs in to the Cisco Nexus switch using SSH and retrieves the current state of the SNMP protocol by using the "show snmp" command.
    without-indeni: |
       The administrator would have to manually log in to the device and check the SNMP communities configuration.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show snmp

#! PARSER::AWK

# initialize variables
BEGIN {
    isInCommunities = 0
    isSnmpEnabled = 0
    snmpLocation = ""
    snmpContact = ""
}

#---------
/^-----/ {
    #The 'next' is needed in order to not process the line as a 'community' structure
    next
}


#Community            Group / Access      context    acl_filter
/^Community.*acl_filter/ {

    # Initialize the variables in order to start processing the 'community' table/array
    isInCommunities = 1
    snmpCommunitiesCount = 0

    # Next is needed in order to not process the header as a 'community' structure
    next
}

(isInCommunities == 1) {

    #sys contact: who@where
    if ($0 ~ /^sys contact/) {

        # Found end of communities block,
        isInCommunities = 0

        # Set the 'snmp-contact'
        #sys contact: who@where
        split($0, array_contact, ":")
        snmpContact = trim(array_contact[2])
        writeComplexMetricString("snmp-contact", null, snmpContact)

    } else {

        # Process a community line. Store the community and permission in the array
        #really-secure         network-admin                  ACL1
        snmpCommunitiesCount++
        snmpCommunities[snmpCommunitiesCount, "community"] = $1
        snmpCommunities[snmpCommunitiesCount, "permissions"] = $2

    }
}

#SNMP protocol : Enabled
/SNMP protocol.*Enabled/ {
    isSnmpEnabled = 1
    next
}

#SNMP protocol : Disabled
/SNMP protocol.*Disabled/ {
    isSnmpEnabled = 0
    next
}

#sys location: snmplocation
/sys location:/ {
    split($0, arrayLocation, ":")
    snmpLocation = trim(arrayLocation[2])
    next
}


END {

    # Publish snmp-enabled
    snmpEnabledValue = "false"
    if (isSnmpEnabled == 1) {
        snmpEnabledValue = "true"
    }
    writeComplexMetricString("snmp-enabled", null, snmpEnabledValue)

    # Publish snmp-communities
    writeComplexMetricObjectArray("snmp-communities", null, snmpCommunities)

    # Publish snmp-location
    writeComplexMetricString("snmp-location", null, snmpLocation)

    # Publish snmp-contact
    writeComplexMetricString("snmp-contact", null, snmpContact)
}

#! META
name: nexus-show-ip-pim-neighbore
description: fetch the IP PIM (Multicast) neighbors
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: cisco
    os.name: nxos
    pim: true

#! COMMENTS
pim-state:
    why: |
       Check if IP PIM (Protocol Independent Multicast) neighbors are up. If an adjacency disappears an alert will be triggered.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show ip pim neighbors" command. The output includes a complete list of all PIM neighbors.
    without-indeni: |
       It is possible to poll this data through SNMP. State transitions generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

#! REMOTE::SSH
show ip pim neighbor | xml

#! PARSER::XML
_omit:
    _bottom: 1
_vars:
    root: //__readonly__ 
_metrics:
    -
        _groups:
            ${root}/TABLE_vrf/ROW_vrf/TABLE_neighbor/ROW_neighbor:
                _value.double:
                    _constant: "1.0"
                _tags:
                    "im.name":
                        _constant: "pim-state" 
                    "name":
                        _text: "nbr-addr"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "HSRP - This Member State"
                    "im.dstype.displayType":
                        _constant: "state"            
                    "im.identity-tags":
                        _constant: "name"             

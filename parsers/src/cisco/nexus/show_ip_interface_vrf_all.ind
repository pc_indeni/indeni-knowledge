#! META
name: nexus-show-ip-interface-vrf-all
description: Nexus show ip interface vrf all
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "cisco"
    os.name: "nxos"

#! COMMENTS
interface-proxy-arp-status:
    why: |
        Proxy arp increases the amount of ARP traffic on a network segment. In addtion, hosts need larger ARP tables in order to handle IP-to-MAC address mappings. Finally, security can be undermined since a machine can claim to be another in order to intercept packets, an act called "spoofing. So, it is recommended by the vendor in most cases to disable the disable ip proxy arp. For more info review the next link: https://www.cisco.com/c/en/us/about/security-center/securing-nx-os.html#_Toc303633231
    how: |
        This script login to the Cisco Nexus network device using SSH and retrieves the ip proxy arp status by using the "show ip interface vrf all" command. The output includes a complete report of the proxy arp status per interface.
    without-indeni: |
        The administrator would have to login to the device and use the "show ip interface vrf all" command to easily identify the interfaces where the ip proxy arp is enabled.
    can-with-snmp: false
    can-with-syslog: false

interface-local-proxy-arp-status:
    why: |
        This feature is used to enable an interface-local proxying of ARP requests. Activation will make the router answer all ARP requests on configured subnet, even for clients that shouldn't normally need routing. Local proxy ARP requires that proxy ARP is active. Proxy arp increases the amount of ARP traffic on a network segment. In addtion, hosts need larger ARP tables in order to handle IP-to-MAC address mappings. Finally, security can be undermined since a machine can claim to be another in order to intercept packets, an act called "spoofing. So, it is recommended by the vendor in most cases to disable the disable ip proxy arp. For more info review the next link: https://www.cisco.com/c/en/us/about/security-center/securing-nx-os.html#_Toc303633231
    how: |
        This script login to the Cisco Nexus switch using SSH and retrieves the ip local proxy arp status by using the "show ip interface vrf all" command. The output includes a complete report of the local proxy arp status per interface.
    without-indeni: |
        The administrator would have to login to the device and use the "show ip interface vrf all" command to easily identify the interfaces where the ip local proxy arp is enabled.
    can-with-snmp: false
    can-with-syslog: false

interface-directed-broadcasts-status:
    why: |
        Because directed broadcasts, and particularly Internet Control Message Protocol (ICMP) directed broadcasts, have been abused by malicious users, it is recommended by the vendor to disable the ip directed-broadcast command on any interface where directed broadcasts are not needed. It is also recommended to be used access lists to limit the number of broadcast packets. For more info review the next link: https://www.cisco.com/c/en/us/about/security-center/securing-nx-os.html#_Toc303633231
    how: |
        This script login to the Cisco Nexus switch using SSH and retrieves the ip directed broadcasts state using the "show ip interface vrf all" command. The output includes a complete report of the directed broadcast status per interface.
    without-indeni: |
        The administrator would have to login to the device and use the "show ip interface vrf all" command to easily identify the interfaces where the ip directed broadcasts is enabled.
    can-with-snmp: false
    can-with-syslog: false

interface-icmp-redirect-status:
    why: |
        An ICMP redirect message can be generated by a router when a packet is received and transmitted on the same interface. In this situation, the router forwards the packet and sends an ICMP redirect message back to the sender of the original packet. In a properly functioning IP network, a router sends redirect messages only to hosts on its own local subnets. In other words, ICMP redirect messages should never go beyond a Layer 3 boundary. A malicious user can exploit the capability of the router to send ICMP redirect messages by continually sending packets to the router, forcing the router to respond with ICMP redirect messages, resulting in adverse impact on the CPU and on the performance of the router.For more info review the next link: https://www.cisco.com/c/en/us/about/security-center/securing-nx-os.html#_Toc303633231
    how: |
        This script login to the Cisco Nexus switch using SSH and retrieves the ICMP redirect state by using the "show ip interface vrf all" command. The output includes a complete report of the icmp redirect status per interface.
    without-indeni: |
        The administrator would have to login to the device and use the "show ip interface vrf all" command to easily identify the interfaces where the ICMP redirect is enabled.
    can-with-snmp: false
    can-with-syslog: false

interface-icmp-unreachable-status:
    why: |
        Filtering with an interface access list elicits the transmission of ICMP unreachable messages back to the source of the filtered traffic. Generating these messages can increase CPU utilization on the device. It is possible to disable ICMP unreachable message generation. For more info review the next link: https://www.cisco.com/c/en/us/about/security-center/securing-nx-os.html#_Toc303633231
    how: |
        This script login to the Cisco Nexus switch using SSH and retrieves the ICMP unreachable state by using the "show ip interface vrf all" command. The output includes a complete report of the icmp unreachable status per interface.
    without-indeni: |
        The administrator would have to login to the device and use the "show ip interface vrf all" command to easily identify the interfaces where the ICMP unreachable is enabled.
    can-with-snmp: false
    can-with-syslog: false

interface-urpf-status:
    why: |
        Many attacks use source IP address spoofing to be effective or to conceal the true source of an attack and hinder accurate traceback. Cisco NX-OS provides uRPF and IP source guard to deter attacks that rely on source IP address spoofing. uRPF enables a device to verify that the source address of a forwarded packet can be reached through the interface that received the packet. For more info review the next link: https://www.cisco.com/c/en/us/about/security-center/securing-nx-os.html#_Toc303633231
    how: |
        This script logs in to the Cisco IOS network device using SSH and retrieves the urpf state by using the "show ip interface vrf all" command. The output includes a complete report of urpf status per interface.
    without-indeni: |
        The administrator would have to log in to the device and use the "show ip interface vrf all" command to easily identify the interfaces where the urpf is enabled.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show ip interface vrf all 

#! PARSER::AWK

# Evaluate the input text
# return 0 if the text is 'disabled' or 'none'
function getZeroIfDisabled(text) {
    if (tolower(trim(text)) == "disabled" || tolower(trim(text)) == "none")
        return 0;
    return 1;
}

BEGIN {
    # Store in table all the listed interfaces with the needed properties
    table_index = 0;

    # The VRF is not in table format
    value_vrf = "";
}

# Read the VRF (note that we don't increase the table index)
#IP Interface Status for VRF "default"
/IP Interface Status for VRF /{
    value_vrf = $NF;
}

# Read the interface name
#Vlan10, Interface status: protocol-up/link-up/admin-up, iod: 5,
/, Interface status:/ {
    table_index++;

    # Remove ',' character
    interface_name = $1;
    gsub(/,/, "", interface_name);

    table_interface[table_index, "name"] = interface_name;
    table_interface[table_index, "vrf"] = value_vrf;
}

#  IP proxy ARP : disabled
/\sIP proxy ARP :/{
    table_interface[table_index, "proxy"] = $NF;
}

#  IP Local Proxy ARP : disabled
/\sIP Local Proxy ARP :/{
    table_interface[table_index, "local-proxy"] = $NF;
}

#  IP directed-broadcast: disabled
/\sIP directed-broadcast:/{
    table_interface[table_index, "d-broadcast"] = $NF;
}

#  IP icmp redirects: disabled
/\sIP icmp redirects:/{
    table_interface[table_index, "icmp-r"] = $NF;
}

#  IP icmp unreachables (except port): disabled
/\sIP icmp unreachables \(except port\):/{
    table_interface[table_index, "icmp-un"] = $NF;
}

#  IP unicast reverse path forwarding: none
/\sIP unicast reverse path forwarding:/{
    table_interface[table_index, "ip-urpf"] = $NF;
}

END{
    # For each interface publish the stored metrics
    for (idx = 1; idx <= table_index ; idx++ ) {
        tags["name"] = table_interface[idx, "name"] " for VRF " table_interface[idx, "vrf"] ;

        writeDoubleMetric("interface-proxy-arp-status", tags, "gauge", 300, getZeroIfDisabled(table_interface[idx, "proxy"]))
        writeDoubleMetric("interface-local-proxy-arp-status", tags, "gauge", 300, getZeroIfDisabled(table_interface[idx, "local-proxy"]))
        writeDoubleMetric("interface-directed-broadcasts-status", tags, "gauge", 300, getZeroIfDisabled(table_interface[idx, "d-broadcast"]))
        writeDoubleMetric("interface-icmp-redirect-status", tags, "gauge", 300, getZeroIfDisabled(table_interface[idx, "icmp-r"]))
        writeDoubleMetric("interface-icmp-unreachable-status", tags, "gauge", 300, getZeroIfDisabled(table_interface[idx, "icmp-un"]))
        writeDoubleMetric("interface-urpf-status", tags, "gauge", 300, getZeroIfDisabled(table_interface[idx, "ip-urpf"]))
    }
}

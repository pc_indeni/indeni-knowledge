#! META
name: nexus-snmp-v3-best-practices
description: Nexus snmpv3 security policies applied
type: monitoring
monitoring_interval: 59 minutes
requires:
    vendor: cisco
    os.name: nxos

# --------------------------------------------------------------------------------------------------
# The script publish the following metrics
#
# [snmp-v3-enabled]         [true/false, true when snmp-v3 is enable (snmp-enable and users > 0)]
# [snmp-users]              [list with the snmp users]
# [snmp-v3-best-practice]   [true/false, true when snmp-v3 best practices are used. (priv enforce in all users)]
# --------------------------------------------------------------------------------------------------

#! COMMENTS
snmp-v3-enabled:
    why: |
        Capture whether SNMPv3 is configured and activated.
    how: |
        This script logs in to the Cisco Nexus switch using SSH and retrieves the current state of the SNMP
        configuration server by using the "show snmp" command.
    without-indeni: |
     The administrator would have to manually log in to the device and check if SNMPv3 is enabled.
    can-with-snmp: false
    can-with-syslog: false

snmp-users:
     why: |
        Capture the SNMP users and permissions. SNMPv3 is the recommended SNMP version because of the additional
        security authentication and encryption mechanisms.
     how: |
        This script logs in to the Cisco Nexus switch using SSH and retrieves the current state of the SNMP protocol by
        using the "show snmp" command.
     without-indeni: |
        The administrator would have to manually log in to the device and check the SNMP users configuration.
     can-with-snmp: false
     can-with-syslog: false

snmp-v3-best-practice:
     why: |
        Captures whether SNMPv3 is configured according to the best security practises by validating that SNMP requires
        authentication or encryption for incoming requests. By default, the SNMP agent accepts SNMPv3 messages without
        authentication and encryption.When you enforce privacy, Cisco NX-OS responds with an authorizationError for any
        SNMPv3 PDU request using securityLevel parameter of either noAuthNoPriv or authNoPriv.
     how: |
        This script logs in to the Cisco Nexus switch using SSH and retrieves the current state of the SNMP protocol by
        using the "show snmp" command.
     without-indeni: |
        The administrator would have to manually log in to the device and check the SNMP communities configuration.
     can-with-snmp: false
     can-with-syslog: false

#! REMOTE::SSH
show snmp

#! PARSER::AWK

# Initialize the variables
BEGIN {
    # user variables
    is_in_users = 0
    count_users = 0
    is_users_global_privacy_flag_enabled = 0

    is_snmp_enabled = 0
}

# Skip unneeded lines
/-----/ {
    next
}


# Read the columns of the user table
/^User.*Auth.*Priv.*Group/ {
    # init user variables and go to next line
    is_in_users = 1
    count_users = 0
    next
}

/SNMP USERS [global privacy flag enabled]/{
    is_users_global_privacy_flag_enabled = 1
}

is_in_users == 1 {

    if ($0 ~ /^______________________________/) {
        is_in_users = 0
        next
    }

    # There are empty lines between users. ignore them
    # Also ignore the first line After the column-names that contains '____'
    if (length($0) != 0 && ($0 !~ /^____  .*____ .*____/)) {
        count_users++
        list_snmp_users[count_users, "user"] = $1
        list_snmp_users[count_users, "auth"] = $2
        list_snmp_users[count_users, "priv_enforce"] = $3
        list_snmp_users[count_users, "groups"] = $4
    }
    next
}


#### snmp-enabled ####
#SNMP protocol : Enabled
/SNMP protocol.*Enabled/ {
    is_snmp_enabled = 1
    next
}

#SNMP protocol : Disabled
/SNMP protocol.*Disabled/ {
    is_snmp_enabled = 0
    next
}


END {

    #
    # Set if we have users with '(no)' text in the 'priv_enforce' column
    #
    is_at_least_one_user_with_no_enforce = 0
    for(i = 1; i <= count_users; i++){
        if (list_snmp_users[i, "priv_enforce"] ~ /\(no\)/) {
            is_at_least_one_user_with_no_enforce = 1
            break
        }
    }

    #
    # Publishing snmp-v3-enabled
    #
    is_snmpv3_enabled = (is_snmp_enabled == 1 && count_users > 0)
    snmpv3_enabled_value = "false"
    if (is_snmpv3_enabled == 1) {
        snmpv3_enabled_value = "true"
    }
    writeComplexMetricStringWithLiveConfig("snmp-v3-enabled", null, snmpv3_enabled_value, "SNMP-V3 Is Enabled")

    #
    # Publishing  snmp-v3-best-practice
    #
    snmp_v3_best_practice_value = "true"
    if (is_snmpv3_enabled && is_users_global_privacy_flag_enabled == 0 && is_at_least_one_user_with_no_enforce == 1) {
       snmp_v3_best_practice_value = "false"
    }
    writeComplexMetricStringWithLiveConfig("snmp-v3-best-practice", null, snmp_v3_best_practice_value, "SNMP-V3 Best Practice")

    writeComplexMetricObjectArray("snmp-users", null, list_snmp_users)


}
